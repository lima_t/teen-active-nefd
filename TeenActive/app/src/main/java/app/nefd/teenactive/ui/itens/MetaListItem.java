package app.nefd.teenactive.ui.itens;

import app.nefd.teenactive.R;

/**
 * Provê um objeto para um item de lista customizado em uma ListView própria para a tela de metas
 */
public class MetaListItem {

    //region Atributos
    private int codigoMeta = 0;
    private int imagemResourceId = R.mipmap.icone_nao_favorito;
    private String textoPrimario = "";
    private String textoSecundario = "";
    //endregion


    /**
     * Construtor vazio
     */
    public MetaListItem() { }

    /**
     * Construtor sem ícone
     * @param codigoMeta
     * @param textoPrimario
     * @param textoSecundario
     */
    public MetaListItem(int codigoMeta, String textoPrimario, String textoSecundario) {
        this.codigoMeta = codigoMeta;
        this.textoPrimario = textoPrimario;
        this.textoSecundario = textoSecundario;
    }

    /**
     * Construtor com ícone
     * @param codigoMeta
     * @param imagemResourceId
     * @param textoPrimario
     * @param textoSecundario
     */
    public MetaListItem(int codigoMeta, int imagemResourceId, String textoPrimario, String textoSecundario) {
        this.codigoMeta = codigoMeta;
        this.imagemResourceId = imagemResourceId;
        this.textoPrimario = textoPrimario;
        this.textoSecundario = textoSecundario;
    }

    public int getImagemResourceId() { return imagemResourceId; }

    public void setImagemResourceId(int imagemResourceId) { this.imagemResourceId = imagemResourceId; }

    public String getTextoPrimario() {
        return textoPrimario;
    }

    public void setTextoPrimario(String textoPrimario) {
        this.textoPrimario = textoPrimario;
    }

    public String getTextoSecundario() {
        return textoSecundario;
    }

    public void setTextoSecundario(String textoSecundario) { this.textoSecundario = textoSecundario; }

    public int getCodigoMeta() { return codigoMeta; }

    public void setCodigoMeta(int codigoMeta) { this.codigoMeta = codigoMeta; }
}
