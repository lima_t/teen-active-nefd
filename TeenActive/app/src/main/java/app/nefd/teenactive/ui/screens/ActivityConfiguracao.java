package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import app.nefd.teenactive.R;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityConfiguracao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        Toolbar barraAcaoCrudMeta = (Toolbar) findViewById(R.id.barraAcaoConfiguracao);

        try{

            setSupportActionBar(barraAcaoCrudMeta);
            getFragmentManager().beginTransaction().replace(R.id.contentConfiguracao, new FragmentConfiguracao()).commit();

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityConfiguracao.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    /**
     * Classe estática interna para o fragmento do arquivo da tela de configuração
     */
    public static class FragmentConfiguracao extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.fragment_configuracao);
        }
    }

}