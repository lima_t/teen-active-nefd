package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import app.nefd.teenactive.R;

/**
 * Classe conteém todas as funções necessárias para exibir dialogos de seleção numérica
 */
public class NumberPickerDialog extends AlertDialog{

    //region Variáveis Globais
    /**
     * Objeto NumberPicker global para recuperar as informações em outros contextos
     */
    public NumberPicker seletorNumerico;
    //endregion

    //region Construtores

    /**
     * Construtor para o Dialogo de Seletor Númerico
     * @param context Tela de contexto
     * @param titulo Título da caixa dialogo
     * @param valorInicial Valor inicial do seletor númerico
     * @param valorMin Valor mínimo do seletor númerico
     * @param valorMax Valor máximo do seletor númerico
     */
    public NumberPickerDialog(Context context, final String titulo, final int valorMin, final int valorMax){
        super(context);

        LayoutInflater objLayoutInflater = LayoutInflater.from(context);
        View objTela = objLayoutInflater.inflate(R.layout.numberpicker_dialog, null);

        ((TextView)objTela.findViewById(R.id.labTituloNumberPickerDialog)).setText(titulo);
        this.seletorNumerico = (NumberPicker) objTela.findViewById(R.id.seletorNumericoDialog);


        //this.seletorNumerico.setValue(valorInicial);
        this.seletorNumerico.setMinValue(valorMin);
        this.seletorNumerico.setMaxValue(valorMax);

        this.setCancelable(false);
        this.setView(objTela);

    }
    //endregion

}
