package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMeta;
import app.nefd.teenactive.database.entities.Meta;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.dialogs.MetaDialog;
import app.nefd.teenactive.ui.itens.AdapterMetaListView;
import app.nefd.teenactive.ui.itens.MetaListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityMeta extends AppCompatActivity {

    //region Variáveis globais
    private ListView listViewMetas;
    private TextView labNomeMeta,labDescricaoMeta;
    private FloatingActionButton fabAdicionarMeta;
    private ArrayList<Meta> listaMetas;

    private int codigoMetaFavorita = 0;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meta);

        Toolbar barraAcaoMeta = (Toolbar) findViewById(R.id.barraAcaoMeta);

        //region Vinculando componentes ao XML
        RelativeLayout layoutMetaVigente = (RelativeLayout) findViewById(R.id.layoutInfoIMC);
        listViewMetas = (ListView) findViewById(R.id.listViewMetas);
        labNomeMeta = (TextView) findViewById(R.id.labInfoIMC);
        labDescricaoMeta = (TextView) findViewById(R.id.labDescricaoMeta);
        fabAdicionarMeta = (FloatingActionButton) findViewById(R.id.fabAdicionarMeta);
        //endregion

        //region Setando valores dos componentes
        try{

            setSupportActionBar(barraAcaoMeta);//Seta a ActionBar
            initListView();//Inicia a ListView de metas
            setMetaPadraoInfo();//Seta os valores do painel da meta favorita

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityMeta.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        //endregion

        //region Eventos
        fabAdicionarMeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent objItenIntent = new Intent(ActivityMeta.this, ActivityCrudMeta.class);

                /*Não é necessário passar parâmetros para a activity quando a operação for um cadastro de meta, pois
                * o valor padrão do parametro codMeta = 0*/
                startActivity(objItenIntent);
            }
        });

        listViewMetas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chamarMetaDialog(position);
            }
        });

        layoutMetaVigente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent objItenIntent = new Intent(ActivityMeta.this, ActivityCrudMeta.class);

                //Passa como parametro o valor do código da meta na lista, isso indica que a operação será uma atualização
                objItenIntent.putExtra("codigoMeta", codigoMetaFavorita);
                startActivity(objItenIntent);
            }
        });
        //endregion
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            initListView();//Carrega a ListView Atualizada
            setMetaPadraoInfo();//Seta os valores do painel da meta favorita

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityMeta.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    /**
     * Metodo responsável por exibir o dialogo de contexto durante o clique de um item na lista de metas
     * @param position posição do item
     */
    private void chamarMetaDialog(final int position){
        final MetaDialog objMetaDialog = new MetaDialog(ActivityMeta.this);

        if(listaMetas.get(position).isMetaPadrao()){
            objMetaDialog.btFavoritarMeta.setBackgroundColor(Color.WHITE);
            objMetaDialog.btFavoritarMeta.setTextColor(Color.LTGRAY);
            objMetaDialog.btFavoritarMeta.setEnabled(false);
        }

        //region Click do Botão Editar Meta
        //Seta o click do botão no dialogo de contexto
        objMetaDialog.btEditarMeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objItenIntent = new Intent(ActivityMeta.this, ActivityCrudMeta.class);

                //Passa como parametro o valor do código da meta na lista, isso indica que a operação será uma atualização
                objItenIntent.putExtra("codigoMeta", listaMetas.get(position).getCodigo());
                startActivity(objItenIntent);
                objMetaDialog.dismiss();
            }
        });
        //endregion

        //region Click do botão Excluir Meta
        //Seta o click do botão no dialogo de contexto
        objMetaDialog.btExcluirMeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Mostra o dialogo de confirmação
                MessageDialog.exibirDialogoQuestao(ActivityMeta.this, "Info", "Você realmente deseja excluir a meta selecionada?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {//Seta o click positivo do dialogo de confirmação
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                try{

                                    new DAOMeta(ActivityMeta.this).excluirMeta(listaMetas.get(position).getCodigo());
                                    Toast.makeText(ActivityMeta.this, "A meta foi excluída com sucesso!", Toast.LENGTH_SHORT).show();
                                    onResume();//Atualiza a tela

                                }catch (final Exception erro){

                                    //Mostra o dialogo de erro
                                    MessageDialog.exibirDialogoErro(ActivityMeta.this, "Erro",
                                            "Ocorreu um erro e não foi possível excluir o item desejado.\n[Erro]:\n\n"+erro.getMessage())
                                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {//Seta o click do botão no dialogo de erro

                                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                            EmailType.RELATORIO_BUG,
                                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                            AppUtils.comporEmailErro(erro)),
                                                            "Enviar E-mail"));
                                                    finish();
                                                }
                                            })
                                            .setNegativeButton("Finalizar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            })
                                            .show();
                                }
                            }
                        })
                        .setNegativeButton("Não", null)
                        .show();

                objMetaDialog.dismiss();//Fecha o dialogo de contexto
            }
        });
        //endregion

        //region Click do Botão Favoritar Meta
        //Seta o click do botão no dialogo de contexto
        objMetaDialog.btFavoritarMeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{

                    new DAOMeta(ActivityMeta.this).favoritarMeta(listaMetas.get(position).getCodigo());
                    Toast.makeText(ActivityMeta.this,"A meta: \""+listaMetas.get(position).getDescricao()+"\" foi favoritada.",                            Toast.LENGTH_SHORT).show();
                    onResume();//Atualiza a visualização

                }catch(final Exception erro){
                    //Mostra o dialogo de erro
                    MessageDialog.exibirDialogoErro(ActivityMeta.this, "Erro",
                            "Ocorreu um erro ao tentar favoritar a meta \""+listaMetas.get(position).getDescricao()+"\"\n[Erro]:\n\n"+erro.getMessage())
                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {//Seta o click do botão no dialogo de erro

                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                            EmailType.RELATORIO_BUG,
                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                            AppUtils.comporEmailErro(erro)),
                                            "Enviar E-mail"));
                                    finish();
                                }
                            })
                            .setNegativeButton("Finalizar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
                objMetaDialog.dismiss();//Fecha o dialogo de contexto
            }
        });
        //endregion

        objMetaDialog.show();//Exibe o dialogo de contexto da lista de metas cadastradas
    }

    /**
     * Carrega os dados da listView de metas
     */
    private void initListView() throws Exception{

        listaMetas = new DAOMeta(ActivityMeta.this).getListaMetas();

        if(!listaMetas.isEmpty()){

            ArrayList<MetaListItem> listaItens = new ArrayList<>();
            String textoSecundario = "";

            for(Meta meta: listaMetas){

                //region Switch
                switch (meta.getTipo()){
                    case 0:
                        textoSecundario = "Tempo diário: "+
                                (
                                        meta.getValorSimbolico()>=60?//Se for maior que 60 minutos
                                                (meta.getValorSimbolico()/60)+" hora(s) e "+
                                                (meta.getValorSimbolico()%60)+"min(s)"://Exibe em horas
                                                meta.getValorSimbolico()+" min(s)"//Senão exiba em minutos
                                );
                        break;
                    case 1:
                        textoSecundario = "Calorias diárias: "+meta.getValorSimbolico()+" kcal";
                        break;
                    case 2:
                        textoSecundario = "Passos diários: "+meta.getValorSimbolico()+" passos";
                        break;
                    case 3:
                        textoSecundario = "Distância diária: "+
                                (
                                        meta.getValorSimbolico()>=1_000?//se o valor simbólico for maior que 1000
                                                (meta.getValorSimbolico()/1_000f)+" km"://Então divida o valor e mostre em km
                                                meta.getValorSimbolico()+" metros"//Senão apenas mostre o valor em metros
                                );
                        break;
                }
                //endregion

                listaItens.add(new MetaListItem(
                        meta.getCodigo(),
                        (meta.isMetaPadrao()?R.mipmap.icone_favorito:R.mipmap.icone_nao_favorito),
                        meta.getDescricao(),
                        textoSecundario
                        ));
            }

            listViewMetas.setAdapter(new AdapterMetaListView(ActivityMeta.this, listaItens));

        }else{
            Toast.makeText(ActivityMeta.this, "Não existe metas cadastradas ainda", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Metodo responsável por percorrer toda a lista de metas e verificar qual é a meta padrão a partir
     * o ícone de meta favorita, por fim, setar os valores no painel da meta favorita.
     */
    private void setMetaPadraoInfo(){
        for(Meta meta : listaMetas){
            if(meta.isMetaPadrao()){
                String textoSecundario = "";
                //region Switch
                switch (meta.getTipo()){
                    case 0:
                        textoSecundario = "Tempo diário: "+(
                                meta.getValorSimbolico()>=60?//Se for maior que 60 minutos
                                        (meta.getValorSimbolico()/60)+" hora(s) e "+
                                                (meta.getValorSimbolico()%60)+"min(s)"://Exibe em horas
                                        meta.getValorSimbolico()+" min"//Senão exiba em minutos
                        );
                        break;
                    case 1:
                        textoSecundario = "Calorias diárias: "+meta.getValorSimbolico()+" kcal";
                        break;
                    case 2:
                        textoSecundario = "Passos diários: "+meta.getValorSimbolico()+" passos";
                        break;
                    case 3:
                        textoSecundario = "Distância diária: "+(
                                meta.getValorSimbolico()>=1_000?//se o valor simbólico for maior que 1000
                                        (meta.getValorSimbolico()/1_000f)+" km"://Então divida o valor e mostre em km
                                        meta.getValorSimbolico()+" metros"//Senão apenas mostre o valor em metros
                        );
                        break;
                }
                //endregion
                labNomeMeta.setText(meta.getDescricao());
                labDescricaoMeta.setText(textoSecundario);
                codigoMetaFavorita = meta.getCodigo();
            }
        }
    }
}
