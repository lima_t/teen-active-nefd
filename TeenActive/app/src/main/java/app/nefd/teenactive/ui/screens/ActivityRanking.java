package app.nefd.teenactive.ui.screens;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMeta;
import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Meta;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.AdapterMetaListView;
import app.nefd.teenactive.ui.itens.MetaListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityRanking extends AppCompatActivity {

    //region Variáveis globais
    TabHost painelTabulado;
    private RadioButton radMetasConcluidas;
    private TextView labGeneric;
    private ListView listViewMetas;
    private ArrayList<MetaListItem> listaMetaListItens;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        try {

            //region Vinculando XML dos componentes
            Toolbar toolbar = (Toolbar) findViewById(R.id.barraAcaoRanking);

            painelTabulado = (TabHost) findViewById(R.id.painelTabuladoRanking);
            radMetasConcluidas = (RadioButton) findViewById(R.id.radMetasConcluidas);
            listViewMetas = (ListView) findViewById(R.id.listViewMetas);
            //endregion

            //region Setando valores dos componentes
            setSupportActionBar(toolbar);
            painelTabulado.setup();//Setando Painel Tabulado

            //Setando as aba Pendentes
            TabHost.TabSpec abaRankingLista = painelTabulado.newTabSpec("Metas");
            abaRankingLista.setContent(R.id.tabRankingLista);
            abaRankingLista.setIndicator("Metas");
            painelTabulado.addTab(abaRankingLista);

            //Setando as aba Concluídas
            TabHost.TabSpec abaRankingDetalhes = painelTabulado.newTabSpec("Detalhes");
            abaRankingDetalhes.setContent(R.id.tabRankingDetalhes);
            abaRankingDetalhes.setIndicator("Detalhes");
            painelTabulado.addTab(abaRankingDetalhes);

            //Iniciando as listas
            loadListView();
            //endregion

        } catch (final Exception erro) {
            MessageDialog.exibirDialogoErro(ActivityRanking.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n" + erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }

        //region Eventos dos componentes
        radMetasConcluidas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try{

                    loadListView();

                }catch(final Exception erro){
                    //region Dialogo para enviar BUG
                    MessageDialog.exibirDialogoErro(ActivityRanking.this, "Erro",
                            "Ocorreu um erro ao tentar filtrar a lista.\n[Erro]:\n\n" + erro.getMessage())
                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                            EmailType.RELATORIO_BUG,
                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                            AppUtils.comporEmailErro(erro)),
                                            "Enviar E-mail"));
                                    finish();
                                }
                            })
                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                    //endregion
                }
            }
        });

        listViewMetas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{

                    loadRankingMeta(listaMetaListItens.get(position).getCodigoMeta());
                    painelTabulado.setCurrentTab(1);

                }catch(final Exception erro){
                    //region Dialogo para enviar BUG
                    MessageDialog.exibirDialogoErro(ActivityRanking.this, "Erro",
                            "Ocorreu um erro ao tentar filtrar a lista.\n[Erro]:\n\n" + erro.getMessage())
                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                            EmailType.RELATORIO_BUG,
                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                            AppUtils.comporEmailErro(erro)),
                                            "Enviar E-mail"));
                                    finish();
                                }
                            })
                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                    //endregion
                }
            }
        });
        //endregion
    }

    /**
     * Carrega os dados das listas de metas pendentes e concluídas
     *
     * @throws Exception
     */
    private void loadListView() throws Exception {
        ArrayList<Meta> listaMetas = new DAOMeta(ActivityRanking.this).getListaMetas();

        if (!listaMetas.isEmpty()) {

            listaMetaListItens = new ArrayList<>();
            String textoSecundario = "";

            for (Meta meta : listaMetas) {

                //region Switch
                switch (meta.getTipo()) {
                    case 0:
                        textoSecundario = "Tempo diário: " + (
                                meta.getValorSimbolico() >= 60 ?//Se for maior que 60 minutos
                                        (meta.getValorSimbolico() / 60) + " hora(s) e " +
                                                (meta.getValorSimbolico() % 60) + "min(s)" ://Exibe em horas
                                        meta.getValorSimbolico() + " min"//Senão exiba em minutos
                        );
                        break;
                    case 1:
                        textoSecundario = "Calorias diárias: " + meta.getValorSimbolico() + " kcal";
                        break;
                    case 2:
                        textoSecundario = "Passos diários: " + meta.getValorSimbolico() + " passos";
                        break;
                    case 3:
                        textoSecundario = "Distância diária: " + (
                                meta.getValorSimbolico() >= 1_000 ?//se o valor simbólico for maior que 1000
                                        (meta.getValorSimbolico() / 1_000f) + " km" ://Então divida o valor e mostre em km
                                        meta.getValorSimbolico() + " metros"//Senão apenas mostre o valor em metros
                        );
                        break;
                }
                //endregion

                //Distribui o itens de acordo com o status de concluída ou não para as listas
                if (radMetasConcluidas.isChecked()) {

                    if (meta.isMetaAtingida()) {
                        listaMetaListItens.add(new MetaListItem(meta.getCodigo(), (meta.isMetaPadrao()?R.mipmap.icone_favorito:R.mipmap.icone_nao_favorito), meta.getDescricao(), textoSecundario));
                    }

                } else {

                    if (!meta.isMetaAtingida()) {
                        listaMetaListItens.add(new MetaListItem(meta.getCodigo(), (meta.isMetaPadrao()?R.mipmap.icone_favorito:R.mipmap.icone_nao_favorito), meta.getDescricao(), textoSecundario));
                    }
                }

                //Seta a meta padrão inicialmente no painel
                if (meta.isMetaPadrao()) {
                    //Colocar lógica da meta padrão inicialmente selecionada aqui
                    loadRankingMeta(meta.getCodigo());
                }
            }

            listViewMetas.setAdapter(new AdapterMetaListView(ActivityRanking.this, listaMetaListItens));

        } else {
            Toast.makeText(ActivityRanking.this, "Não existe metas cadastradas ainda", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Carrega as informações do ranking da meta selecionada no painel de detalhes
     * @param codigoMeta
     * @throws Exception
     */
    @SuppressLint("SetTextI18n")
    private void loadRankingMeta(int codigoMeta) throws Exception{
        Meta meta = new DAOMeta(ActivityRanking.this).consultarMeta(codigoMeta);

        if (meta.isMetaAtingida()) {

            //Preenchendo informações sobre a meta
            labGeneric = (TextView) findViewById(R.id.labNomeMeta);
            labGeneric.setText("Ranking: "+meta.getDescricao());

            //Objeto para formatar a data
            SimpleDateFormat objDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            ArrayList<Treino> listaTreinosRankeados = new DAOTreino(ActivityRanking.this).getRankedAtividades(meta.getTipo(), meta.getValorSimbolico());

            for(int index=0; index < listaTreinosRankeados.size(); index++){

                //region Switch dos treinos rankeados
                switch (index){
                    case 0:
                        //1º Lugar
                        labGeneric = (TextView) findViewById(R.id.labDataTreino1);
                        labGeneric.setText(objDateFormat.format(listaTreinosRankeados.get(index).getDataTreino()));
                        labGeneric = (TextView) findViewById(R.id.labAtividadeTreino1);
                        labGeneric.setText(listaTreinosRankeados.get(index).getAtividadeDescricao());
                        labGeneric = (TextView) findViewById(R.id.labTempoTreino1);
                        labGeneric.setText(AppUtils.formatarMillisToHoras(listaTreinosRankeados.get(index).getTempoTotal()));
                        labGeneric = (TextView) findViewById(R.id.labDistanciaTreino1);
                        labGeneric.setText(
                                (
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()>=1_000?
                                        (listaTreinosRankeados.get(index).getDistanciaPercorrida()/1_000f)+" km":
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()+" metros"
                                )
                        );
                        labGeneric = (TextView) findViewById(R.id.labKcalTreino1);
                        labGeneric.setText(String.format(AppUtils.pt_BR, "%.2f kcal",listaTreinosRankeados.get(index).getKcalConsumida()));
                        labGeneric = (TextView) findViewById(R.id.labPassosTreino1);
                        labGeneric.setText(listaTreinosRankeados.get(index).getQuantidadePassos()+" passos");
                        labGeneric = (TextView) findViewById(R.id.labRecordTreino1);
                        break;
                    case 1:
                        //2º Lugar
                        labGeneric = (TextView) findViewById(R.id.labDataTreino2);
                        labGeneric.setText(objDateFormat.format(listaTreinosRankeados.get(index).getDataTreino()));
                        labGeneric = (TextView) findViewById(R.id.labAtividadeTreino2);
                        labGeneric.setText(listaTreinosRankeados.get(index).getAtividadeDescricao());
                        labGeneric = (TextView) findViewById(R.id.labTempoTreino2);
                        labGeneric.setText(AppUtils.formatarMillisToHoras(listaTreinosRankeados.get(index).getTempoTotal()));
                        labGeneric = (TextView) findViewById(R.id.labDistanciaTreino2);
                        labGeneric.setText(
                                (
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()>=1_000?
                                        (listaTreinosRankeados.get(index).getDistanciaPercorrida()/1_000f)+" km":
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()+" metros"
                                )
                        );
                        labGeneric = (TextView) findViewById(R.id.labKcalTreino2);
                        labGeneric.setText(String.format(AppUtils.pt_BR, "%.2f kcal",listaTreinosRankeados.get(index).getKcalConsumida()));
                        labGeneric = (TextView) findViewById(R.id.labPassosTreino2);
                        labGeneric.setText(listaTreinosRankeados.get(index).getQuantidadePassos()+" passos");
                        labGeneric = (TextView) findViewById(R.id.labRecordTreino2);
                        break;
                    case 2:
                        //3º Lugar
                        labGeneric = (TextView) findViewById(R.id.labDataTreino3);
                        labGeneric.setText(objDateFormat.format(listaTreinosRankeados.get(index).getDataTreino()));
                        labGeneric = (TextView) findViewById(R.id.labAtividadeTreino3);
                        labGeneric.setText(listaTreinosRankeados.get(index).getAtividadeDescricao());
                        labGeneric = (TextView) findViewById(R.id.labTempoTreino3);
                        labGeneric.setText(AppUtils.formatarMillisToHoras(listaTreinosRankeados.get(index).getTempoTotal()));
                        labGeneric = (TextView) findViewById(R.id.labDistanciaTreino3);
                        labGeneric.setText(
                                (
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()>=1_000?
                                        (listaTreinosRankeados.get(index).getDistanciaPercorrida()/1_000f)+" km":
                                        listaTreinosRankeados.get(index).getDistanciaPercorrida()+" metros"
                                )
                        );
                        labGeneric = (TextView) findViewById(R.id.labKcalTreino3);
                        labGeneric.setText(String.format(AppUtils.pt_BR, "%.2f kcal",listaTreinosRankeados.get(index).getKcalConsumida()));
                        labGeneric = (TextView) findViewById(R.id.labPassosTreino3);
                        labGeneric.setText(listaTreinosRankeados.get(index).getQuantidadePassos()+" passos");
                        labGeneric = (TextView) findViewById(R.id.labRecordTreino3);
                        break;
                    default:
                        //Não faz nada
                        break;
                }
                //endregion

                //region Switch do calculo do Record dos treinos Rankeados
                switch (meta.getTipo()){
                    case 0:
                        //Tempo
                        labGeneric.setText(String.format(AppUtils.pt_BR,"↑ %d min(s) acima da meta",
                                meta.getValorSimbolico()-AppUtils.converterMillisToMinutos(listaTreinosRankeados.get(index).getTempoTotal())));
                        break;
                    case 1:
                        //Kcal
                        labGeneric.setText(String.format(AppUtils.pt_BR,"↑ %.2f kcal acima da meta",
                                meta.getValorSimbolico()-listaTreinosRankeados.get(index).getKcalConsumida()
                                ));
                        break;
                    case 2:
                        //Passos
                        labGeneric.setText(String.format(AppUtils.pt_BR,"↑ %d passos acima da meta",
                                meta.getValorSimbolico()-listaTreinosRankeados.get(index).getQuantidadePassos()
                        ));
                        break;
                    case 3:
                        //Distância
                        labGeneric.setText(String.format(AppUtils.pt_BR,"↑ %.2f metros acima da meta",
                                meta.getValorSimbolico()-listaTreinosRankeados.get(index).getDistanciaPercorrida()
                        ));
                        break;
                    default:
                        //Nenhuma das opções válidas
                        break;
                }
                //endregion
            }
        }else{

            //region Valores padrões vazio
            //Preenchendo informações sobre a meta
            labGeneric = (TextView) findViewById(R.id.labNomeMeta);
            labGeneric.setText("Ranking: "+meta.getDescricao());

            //1º Lugar
            labGeneric = (TextView) findViewById(R.id.labDataTreino1);
            labGeneric.setText("Não Registrada");
            labGeneric = (TextView) findViewById(R.id.labAtividadeTreino1);
            labGeneric.setText("Pendente [Não Atingida]");
            labGeneric = (TextView) findViewById(R.id.labTempoTreino1);
            labGeneric.setText("0 min(s)");
            labGeneric = (TextView) findViewById(R.id.labDistanciaTreino1);
            labGeneric.setText("0.00 km");
            labGeneric = (TextView) findViewById(R.id.labKcalTreino1);
            labGeneric.setText("0.00 kcal");
            labGeneric = (TextView) findViewById(R.id.labPassosTreino1);
            labGeneric.setText("0 passos");
            labGeneric = (TextView) findViewById(R.id.labRecordTreino1);
            labGeneric.setText("Diferença Desconhecida");

            //2º Lugar
            labGeneric = (TextView) findViewById(R.id.labDataTreino2);
            labGeneric.setText("Não Registrada");
            labGeneric = (TextView) findViewById(R.id.labAtividadeTreino2);
            labGeneric.setText("Pendente [Não Atingida]");
            labGeneric = (TextView) findViewById(R.id.labTempoTreino2);
            labGeneric.setText("0 min(s)");
            labGeneric = (TextView) findViewById(R.id.labDistanciaTreino2);
            labGeneric.setText("0.00 km");
            labGeneric = (TextView) findViewById(R.id.labKcalTreino2);
            labGeneric.setText("0.00 kcal");
            labGeneric = (TextView) findViewById(R.id.labPassosTreino2);
            labGeneric.setText("0 passos");
            labGeneric = (TextView) findViewById(R.id.labRecordTreino2);
            labGeneric.setText("Diferença Desconhecida");

            //3º Lugar
            labGeneric = (TextView) findViewById(R.id.labDataTreino3);
            labGeneric.setText("Não Registrada");
            labGeneric = (TextView) findViewById(R.id.labAtividadeTreino3);
            labGeneric.setText("Pendente [Não Atingida]");
            labGeneric = (TextView) findViewById(R.id.labTempoTreino3);
            labGeneric.setText("0 min(s)");
            labGeneric = (TextView) findViewById(R.id.labDistanciaTreino3);
            labGeneric.setText("0.00 km");
            labGeneric = (TextView) findViewById(R.id.labKcalTreino3);
            labGeneric.setText("0.00 kcal");
            labGeneric = (TextView) findViewById(R.id.labPassosTreino3);
            labGeneric.setText("0 passos");
            labGeneric = (TextView) findViewById(R.id.labRecordTreino3);
            labGeneric.setText("Diferença Desconhecida");
            //endregion
        }

    }
}
