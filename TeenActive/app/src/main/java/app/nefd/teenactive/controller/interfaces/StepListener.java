package app.nefd.teenactive.controller.interfaces;

/**
 * Interface para os metodos de detectação de passos
 */
public interface StepListener {

    void onStep(long eventMsecTime);
}
