package app.nefd.teenactive.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import app.nefd.teenactive.R;

/**
 *Classe de fragmento responsável por montar a 2ª tela do Bem vindo!
 */
public class FragmentBemVindo2 extends Fragment {

    private Button btDarPermissoes;
    private TextView labPermissaoGps, labPermissaoSensores, labPermissaoArmazenamento;
    public Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bem_vindo2, container, false);

        btDarPermissoes = (Button) view.findViewById(R.id.btDarPermissoes);
        labPermissaoGps = (TextView) view.findViewById(R.id.labPermissaoGps);
        labPermissaoSensores = (TextView) view.findViewById(R.id.labPermissaoSensores);
        labPermissaoArmazenamento = (TextView) view.findViewById(R.id.labPermissaoArmazenamento);

        //Verifica se as permissões já foram dadas ao iniciar ao criar o fragmento para garantir que o botão seja desabilitado
        if ((ContextCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) |
                (ContextCompat.checkSelfPermission(
                        context, Manifest.permission.BODY_SENSORS) == PackageManager.PERMISSION_GRANTED) |
                (ContextCompat.checkSelfPermission(
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED))
        {
            setComponentsGranted();
        }

        btDarPermissoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Antes de ir para a próxima tela ele verifica se todas as permissões foram concedidas
                if ((ContextCompat.checkSelfPermission(
                        context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) |
                        (ContextCompat.checkSelfPermission(
                                context, Manifest.permission.BODY_SENSORS) == PackageManager.PERMISSION_DENIED) |
                        (ContextCompat.checkSelfPermission(
                                context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED))
                {

                    ActivityCompat.requestPermissions((Activity) context, new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.BODY_SENSORS,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }else{
                    setComponentsGranted();

                }
            }
        });

        return view;
    }

    /**
     * Altera o estado dos componentes quando todas as permissões foram garantidas
     */
    public void setComponentsGranted(){

        btDarPermissoes.setEnabled(false);

        //Seta a cor do texto para laranja Teen Active
        labPermissaoGps.setTextColor(Color.rgb(255, 102, 0));
        labPermissaoSensores.setTextColor(Color.rgb(255, 102, 0));
        labPermissaoArmazenamento.setTextColor(Color.rgb(255, 102, 0));

        labPermissaoGps.setText("[OK!] Acessar a lozalização GPS");
        labPermissaoSensores.setText("[OK!] Usar os sensores corporais");
        labPermissaoArmazenamento.setText("[OK!] Acessar o armazenamento");

        FragmentBemVindo3.isGrantedPermissions = true;
    }
}
