package app.nefd.teenactive.ui.itens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.nefd.teenactive.R;


public class AdapterMetaListView extends BaseAdapter {

    //region Atributos
    private LayoutInflater mInflater;
    private ArrayList<MetaListItem> itens;
    //endregion

    //region Construtores
    public AdapterMetaListView(Context context, ArrayList<MetaListItem> itens)
    {
        this.itens = itens;//Itens que preencheram o listview
        mInflater = LayoutInflater.from(context);//Responsável por pegar o Layout do item.
    }

    //endregion

    //region Metodos abstratos que foram implementados devido a herança
    /**
     * Retorna a quantidade de itens da lista
     * @return
     */
    @Override
    public int getCount() {
        return itens.size();
    }

    /**
     * Retorna a posição (index) do item selecionado
     * @param posicao
     * @return
     */
    @Override
    public Object getItem(int posicao) {
        return itens.get(posicao);
    }

    /**
     * retorna o ID do item na posição (Metodo sem necessidade de implementação)
     * @param posicao
     * @return
     */
    @Override
    public long getItemId(int posicao) {
        return posicao;
    }

    /**
     * Adapta o XML do layout do item da lista customizada para o adapter
     * @param posicao
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {
        //Pega o item de acordo com a posição.
        MetaListItem item = itens.get(posicao);
        //infla o layout para podermos preencher os dados
        convertView = mInflater.inflate(R.layout.meta_list_item, null);

        //atravez do layout pego pelo LayoutInflater, pegamos cada id relacionado
        //ao item e definimos as informações.
        ((ImageView) convertView.findViewById(R.id.imgFavoriteMetaListItem)).setImageResource(item.getImagemResourceId());
        ((TextView) convertView.findViewById(R.id.labMetaListItem1)).setText(item.getTextoPrimario());
        ((TextView) convertView.findViewById(R.id.labMetaListItem2)).setText(item.getTextoSecundario());

        return convertView;
    }
}
