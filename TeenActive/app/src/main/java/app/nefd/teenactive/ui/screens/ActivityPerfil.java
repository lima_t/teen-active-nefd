package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.vdo.VDOPerfil;
import app.nefd.teenactive.ui.dialogs.DatePickerDialog;
import app.nefd.teenactive.ui.dialogs.DecimalPickerDialog;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.dialogs.NumberPickerDialog;
import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.CientificUtils;

public class ActivityPerfil extends AppCompatActivity {

    //region Declarando variaveis dos componentes
    private CircularImageView imgFotoPerfil;
    private EditText txtNome, txtDataNascimento, txtAltura, txtPeso;
    private Spinner spinnerSexo;

    private VDOPerfil objVDOPerfil;
    private Perfil objPerfil;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        Toolbar barraAcaoPerfil = (Toolbar) findViewById(R.id.barraAcaoPerfil);
        DAOPerfil objDAOPerfil = new DAOPerfil(ActivityPerfil.this);

        //region Vinculando os componentes ao XML
        objVDOPerfil = new VDOPerfil(ActivityPerfil.this);

        imgFotoPerfil = (CircularImageView) findViewById(R.id.imgFotoPerfil);
        txtNome = (EditText) findViewById(R.id.txtNome);
        txtDataNascimento = (EditText) findViewById(R.id.txtDataNascimento);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        spinnerSexo = (Spinner) findViewById(R.id.spinnerSexo);
        //endregion

        //region Iniciando os componentes
        try{

            objPerfil = objDAOPerfil.carregarPerfil();//Retorna o perfil salvo no banco de dados
            SimpleDateFormat objDateFormat = new SimpleDateFormat("dd/MM/yyyy");//SimpleDateFormat para converter para o padrão pt-BR
            setSupportActionBar(barraAcaoPerfil);

            if(objPerfil.getFotoPerfil() != null && !objPerfil.getFotoPerfil().isEmpty()){

                if(new File(AppUtils.APP_PROFILE_DIRECTORY+"foto_perfil.jpg").exists()){
                    imgFotoPerfil.setImageBitmap(BitmapFactory.decodeFile(objPerfil.getFotoPerfil()));//Seta a imagem no CircularImageView
                }else{
                    imgFotoPerfil.setImageResource(R.mipmap.icone_imagem_perfil);
                    Toast.makeText(ActivityPerfil.this, "Não foi possível carregar a foto do perfil (arquivo inexistente).", Toast.LENGTH_LONG).show();
                }

            }

            txtNome.setText(objPerfil.getNome());
            txtAltura.setText(String.valueOf(objPerfil.getAltura()));
            txtPeso.setText(String.valueOf(objPerfil.getPeso()));
            if(objPerfil.getDataNascimento() != null){

                txtDataNascimento.setText(objDateFormat.format(objPerfil.getDataNascimento()));

            }else{
                Calendar objCalendario = Calendar.getInstance();//Cria a instância de um calendário
                objCalendario.set((objCalendario.get(Calendar.YEAR)-6), objCalendario.get(Calendar.MONTH), objCalendario.get(Calendar.DAY_OF_MONTH));
                txtDataNascimento.setText(objDateFormat.format(objCalendario.getTime()));
            }

            spinnerSexo.setSelection(objPerfil.getSexo());

        }catch(final Exception erro){

            MessageDialog.exibirDialogoErro(ActivityPerfil.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();

        }
        //endregion

        //region Eventos dos componentes
        //Chamando o NumberPickerDialog e criando os eventos para os botões
        txtDataNascimento.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //Abre o datapicker se o foco tiver entrado no campo de texto
                if(hasFocus){
                    chamarDataPickerDialog();
                }
            }
        });

        txtDataNascimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarDataPickerDialog();
            }
        });

        txtAltura.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //Abre o NumberPicker se o foco tiver entrado no campo de texto
                if(hasFocus){
                    chamarNumberPickerDialog();
                }
            }
        });

        txtAltura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarNumberPickerDialog();
            }
        });

        txtPeso.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    chamarDecimalPickerDialog();
                }
            }
        });

        txtPeso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarDecimalPickerDialog();
            }
        });

        imgFotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Abre a galeria para selecionar uma foto
                Intent objIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(objIntent, 1);

            }
        });
        //endregion
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de acação
        getMenuInflater().inflate(R.menu.menu_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {

            if(item.getItemId() == R.id.itemSalvarPerfil){

                //Converte a string do padrão brasileiro para uma data
                SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
                Date objDataFormatada = objFormatoData.parse(txtDataNascimento.getText().toString());

                //Não precisa setar o valor do objPerfil.setFotoPerfil() pois ela já foi alimentada na troca da imagem
                objPerfil.setNome(txtNome.getText().toString());
                objPerfil.setDataNascimento(objDataFormatada);
                objPerfil.setSexo((short) spinnerSexo.getSelectedItemPosition());
                objPerfil.setAltura(Short.parseShort(txtAltura.getText().toString()));
                objPerfil.setPeso(Float.parseFloat(txtPeso.getText().toString()));

                Configuracao configs = new Configuracao(ActivityPerfil.this);

                if (configs.isPrimeiroAcesso()) {

                    //Efetua essa ação se for o primeiro acesso
                    objVDOPerfil.inserirPerfil(objPerfil);

                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ActivityPerfil.this).edit();
                    editor.putBoolean("primeiro_acesso", false);//Define o primeiro acesso como falso
                    editor.apply();//Confirma as alterações

                    if(CientificUtils.calcularIdadeAnos(objPerfil.getDataNascimento()) > 19){

                        MessageDialog.exibirDialogoInformacao(ActivityPerfil.this, "Sucesso",
                                "[AVISO]: Aparentemente você tem mais de 19 anos, o Teen Active é um aplicativo voltado para monitoramento de atividades em crianças e adolescentes portanto algumas funcionalidades poderão não estar disponíveis e a precisão dos resultados poderá ser significativamente divergente.\n\nSeu perfil foi cadastrado com sucesso e a partir de agora podemos começar as atividades!")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent objIntent = new Intent(ActivityPerfil.this, ActivityPrincipal.class);
                                        startActivity(objIntent);//Abre a tela principal
                                        finish();//Fecha a tela de perfil

                                    }
                                }).show();

                    }else{
                        MessageDialog.exibirDialogoInformacao(ActivityPerfil.this, "Sucesso",
                                "Seu perfil foi cadastrado com sucesso e a partir de agora podemos começar as atividades!")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent objIntent = new Intent(ActivityPerfil.this, ActivityPrincipal.class);
                                        startActivity(objIntent);//Abre a tela principal
                                        finish();//Fecha a tela de perfil

                                    }
                                }).show();
                    }

                } else {

                    //Efetua a atualização se não for o primeiro acesso
                    objVDOPerfil.alterarPerfil(objPerfil);

                    if(CientificUtils.calcularIdadeAnos(objPerfil.getDataNascimento()) > 19){

                        MessageDialog.exibirDialogoInformacao(ActivityPerfil.this, "Sucesso",
                                "[AVISO]: Aparentemente você tem mais de 19 anos, o Teen Active é um aplicativo voltado para monitoramento de atividades em crianças e adolescentes portanto algumas funcionalidades poderão não estar disponíveis e a precisão dos resultados poderá ser significativamente divergente.\n\nSeu perfil foi alterado com sucesso!")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent objIntent = new Intent(ActivityPerfil.this, ActivityPrincipal.class);
                                        startActivity(objIntent);//Abre a tela principal
                                        finish();//Fecha a tela de perfil

                                    }
                                }).show();

                    }else{
                        MessageDialog.exibirDialogoInformacao(ActivityPerfil.this, "Sucesso",
                                "Seu perfil foi alterado com sucesso")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent objIntent = new Intent(ActivityPerfil.this, ActivityPrincipal.class);
                                        startActivity(objIntent);//Abre a tela principal
                                        finish();//Fecha a tela de perfil

                                    }
                                }).show();
                    }
                }
            }

        } catch (Exception erro) {
            MessageDialog.exibirDialogoErro(ActivityPerfil.this, "Erro", erro.toString()).setNeutralButton("Ok", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{

            //Se os parametros retornarem positivos para uma imagem válida selecionada ele exibi a imagem no ImageView
            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                objPerfil.setFotoPerfil(cursor.getString(cursor.getColumnIndex(filePathColumn[0])));
                AppUtils.copiarArquivo(objPerfil.getFotoPerfil(), AppUtils.APP_PROFILE_DIRECTORY+"foto_perfil.jpg");
                objPerfil.setFotoPerfil(AppUtils.APP_PROFILE_DIRECTORY+"foto_perfil.jpg");

                imgFotoPerfil.setImageBitmap(BitmapFactory.decodeFile(objPerfil.getFotoPerfil()));//Seta a imagem no CircularImageView
                cursor.close();

                Toast.makeText(ActivityPerfil.this, "Imagem do perfil alterada com sucesso.", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(ActivityPerfil.this, "Seleção da imagem cancelada.", Toast.LENGTH_LONG).show();
            }

        }catch(final Exception erro){

            MessageDialog.exibirDialogoErro(ActivityPerfil.this, "Erro",
                    "Ocorreu um erro ao tentar alterar a imagem.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();

        }
    }

    //region Metodos personalizados
    /**
     * Exibe um dialogo para seleção de datas para data de nascimento
     */
    private void chamarDataPickerDialog(){
        Calendar objCalendario = Calendar.getInstance();//Cria a instância de um calendário
        objCalendario.set((objCalendario.get(Calendar.YEAR)-6), objCalendario.get(Calendar.MONTH), objCalendario.get(Calendar.DAY_OF_MONTH));
        final SimpleDateFormat objDateFormat = new SimpleDateFormat("dd/MM/yyyy");//SimpleDateFormat para converter para o padrão pt-BR
        final DatePickerDialog objDatePickerDialog = new DatePickerDialog(ActivityPerfil.this, "Data de Nascimento");
        objDatePickerDialog.seletorData.setMaxDate(objCalendario.getTimeInMillis());

        //Altera a data do datapicker para a salva no cadastro quando ele abrir caso o atributo dataNascimento não for nulo
        if(objPerfil.getDataNascimento() != null){

            objCalendario.setTime(objPerfil.getDataNascimento());
            objDatePickerDialog.seletorData.init(objCalendario.get(Calendar.YEAR), objCalendario.get(Calendar.MONTH), objCalendario.get(Calendar.DAY_OF_MONTH), null);

        }

        objDatePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDatePickerDialog.seletorData.clearFocus();//Limpa o focus para commmitar as alterações
                txtDataNascimento.setText(objDateFormat.format(objDatePickerDialog.seletorData.getCalendarView().getDate()));
                objDatePickerDialog.dismiss();
            }
        });
        objDatePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDatePickerDialog.cancel();
            }
        });

        objDatePickerDialog.show();
    }

    /**
     * Exibe um dialogo para seleção de números decimais para o peso
     */
    private void chamarDecimalPickerDialog(){
        final DecimalPickerDialog objDecimalPickerDialog = new DecimalPickerDialog(ActivityPerfil.this, "Medidas (Kg)", 300);

        objDecimalPickerDialog.seletorNumericoMajor.setValue(objPerfil.getPeso()>0?(int)objPerfil.getPeso():1);

        objDecimalPickerDialog.setButton(DecimalPickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDecimalPickerDialog.seletorNumericoMajor.clearFocus();//Limpa o focus para commmitar as alterações
                objDecimalPickerDialog.seletorNumericoMinor.clearFocus();//Limpa o focus para commmitar as alterações
                txtPeso.setText(
                        objDecimalPickerDialog.seletorNumericoMajor.getValue()+"."+objDecimalPickerDialog.seletorNumericoMinor.getValue());
                objDecimalPickerDialog.dismiss();
            }
        });

        objDecimalPickerDialog.setButton(DecimalPickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDecimalPickerDialog.cancel();
            }
        });

        objDecimalPickerDialog.show();
    }

    /**
     * Exibe um dialogo para seleção de números para a altura
     */
    private void chamarNumberPickerDialog(){
        final NumberPickerDialog objNumberPickerDialog = new NumberPickerDialog(ActivityPerfil.this, "Altura (Cm)", 1, 300);
        objNumberPickerDialog.seletorNumerico.setValue(objPerfil.getAltura() > 0?objPerfil.getAltura():100);//Setando valor inicial

        objNumberPickerDialog.setButton(NumberPickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.seletorNumerico.clearFocus();//Limpa o focus para commmitar as alterações
                txtAltura.setText(String.valueOf(objNumberPickerDialog.seletorNumerico.getValue()));
                objNumberPickerDialog.dismiss();
            }
        });
        objNumberPickerDialog.setButton(NumberPickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.cancel();
            }
        });

        objNumberPickerDialog.show();
    }
    //endregion
}
