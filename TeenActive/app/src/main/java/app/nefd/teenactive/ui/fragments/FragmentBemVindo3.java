package app.nefd.teenactive.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import app.nefd.teenactive.R;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.screens.ActivityPerfil;

/**
 *Classe de fragmento responsável por montar a 3ª tela do Bem vindo!
 */
public class FragmentBemVindo3 extends Fragment {

    public Context context;
    protected static boolean isAceptedLicense = false, isGrantedPermissions = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bem_vindo3, container, false);
        Button btCriarPerfil = (Button) view.findViewById(R.id.btCriarPerfil);

        btCriarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                    isGrantedPermissions = true;
                }

                if(FragmentBemVindo3.isAceptedLicense && FragmentBemVindo3.isGrantedPermissions){
                    Intent objIntent = new Intent(context, ActivityPerfil.class);
                    startActivity(objIntent);
                    Activity.class.cast(context).finish();

                }else{
                    MessageDialog.exibirDialogoInformacao(context, "Aviso",
                            "Aparentemente algumas configurações foram negadas e sem elas o aplicativo não pode iniciar o cadastro do perfil. Volte e altere corretamente as devidas configurações!")
                            .setNeutralButton("OK ENTENDI", null)
                            .show();
                }
            }
        });
        return view;
    }
}
