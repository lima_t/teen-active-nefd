package app.nefd.teenactive.database.entities;

import android.support.annotation.IdRes;

/**
 * Classe que representa a entidade ATIVIDADE do banco de dados
 */
public class Atividade {

    //region Atributos
    private int codigo = 0;
    private short categoria = 0;
    private int resourceId = 0;
    private String descricao = null;
    private float metsLeve = 0.0f;
    private float metsModerado = 0.0f;
    private float metsIntenso = 0.0f;
    private boolean passosAtivo = true;
    private boolean distanciaAtiva = true;
    private boolean atividadeFavorita = false;
    //endregion

    //region Construtores

    /**
     * Construtor padrão
     */
    public Atividade() {}

    /**
     * Construtor com todos os atributos
     * @param codigo
     * @param categoria
     * @param resourceId
     * @param descricao
     * @param metsLeve
     * @param metsModerado
     * @param metsIntenso
     * @param atividadeFavorita
     */
    public Atividade(int codigo, int resourceId, short categoria, String descricao, float metsLeve, float metsModerado, float metsIntenso, boolean passosAtivo, boolean distanciaAtiva, boolean atividadeFavorita) {
        this.codigo = codigo;
        this.categoria = categoria;
        this.resourceId = resourceId;
        this.descricao = descricao;
        this.metsLeve = metsLeve;
        this.metsModerado = metsModerado;
        this.metsIntenso = metsIntenso;
        this.passosAtivo = passosAtivo;
        this.distanciaAtiva = distanciaAtiva;
        this.atividadeFavorita = atividadeFavorita;
    }
    //endregion

    //region Getters e Setters

    /**
     * Pega o código da DAOAtividade
     * @return Código da atividade (int)
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Define o valor do código da DAOAtividade
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Pega a categoria da atividade
     * @return categoria da atividade (short)
     */
    public short getCategoria() { return categoria; }

    /**
     * Pega o Resource ID do ícone da atividade
     * @return Resource ID da atividade (Int)
     */
    public int getResourceId() { return resourceId; }

    /**
     * Pega a descrição da DAOAtividade
     * @return Descrição da atividade (String)
     */
    public String getDescricao() {
        return descricao;
    }

    public float getMetsLeve() {
        return metsLeve;
    }

    public float getMetsModerado() {
        return metsModerado;
    }

    public float getMetsIntenso() {
        return metsIntenso;
    }

    public boolean isPassosAtivo() {
        return passosAtivo;
    }

    public boolean isAtividadeFavorita() {
        return atividadeFavorita;
    }

    public boolean isDistanciaAtiva() {
        return distanciaAtiva;
    }

    //endregion
}
