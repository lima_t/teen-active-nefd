package app.nefd.teenactive.controller.interfaces;

/**
 * Classe responsável por definir a interface para o evento da classe TimeCounter
 */
public interface OnTimeCounterResumed {

    /**
     * Ocorre quando o cronometro é resumnido
     */
    void onTimeCounterResumed();
}
