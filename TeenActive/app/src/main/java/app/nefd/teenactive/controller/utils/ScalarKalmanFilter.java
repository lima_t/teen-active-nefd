package app.nefd.teenactive.controller.utils;

/**
 * Filtro de Kalman para suavizar o ruído de baixa intensidade
 */
public final class ScalarKalmanFilter {

    private double mF;
    private double mH;
    private double mQ;
    private double mR;
    private double mState = 0;
    private double mCovariance = 0.1f;

    /**
     * Construtor padrão da classe
     * @param f Fator do valor real para o valor real anterior
     * @param h Fator do valor mensurado para o valor real
     * @param q Ruído mensurado
     * @param r Ruído ambiente
     */
    public ScalarKalmanFilter(float f, float h, float q, float r){
        mF = f;
        mH = h;
        mQ = q;
        mR = r;
    }

    /**
     * Inicia os valores do filtro
     * @param estadoInicial Estado corrente
     * @param covarianciaInicial Covariância corrente
     */
    public void iniciar(float estadoInicial, float covarianciaInicial){
        mState = estadoInicial;
        mCovariance = covarianciaInicial;
    }

    /**
     * Correção do valor mensurado pelo filtro
     * @param measuredValue - valor menssurado
     * @return
     */
    public double corrigir(double measuredValue){

        // Tempo de atualização - predição
        double mX0 = mF * mState;//Predição do estado
        double mP0 = mF * mCovariance * mF + mQ;//Predição da covariância

        // Medida de atualização - correção
        double k = mH * mP0 /(mH * mP0 * mH + mR);
        mCovariance = (1 - k * mH) * mP0;
        return mState = mX0 + k * (measuredValue - mH * mX0);
    }
}
