package app.nefd.teenactive.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Classe contém todos os atributos, construtores e metodos necessários para efetuar as operações básicas de contexto
 * com o banco de dados do aplicativo.
 */
public class ConexaoBanco extends SQLiteOpenHelper{

    /**
     * Construtor necessário para que o banco de dados possa receber uma conexão válida e utilizar as
     * funções
     *
     * @param context tela de contexto do aplicativo
     */
    public ConexaoBanco(Context context) {
        super(context, "teen_active.db", null, 1);
    }

    /**
     * Chamado quando o banco de dados é criado pela primeira vez, executa o script para criar a
     * extrutura do banco de dados
     *
     * @param db objeto tipo tipo SQLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        //Instância da classe ScriptSQL para criar o banco
        ArrayList<String> arrayListScript = ScriptSql.getSqlScriptArray();

        //Cria o banco de dados
        for (String comandoSQL:arrayListScript) {
            db.execSQL(comandoSQL);
        }
    }

    /**
     * Chamado quando o banco de dados tem sua versão atualizada
     * <p>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         Bando de dados
     * @param versaoAntiga Versão antiga
     * @param versaoNova Versão nova
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int versaoAntiga, int versaoNova) {
        //Por enquanto não faz nada
    }
}
