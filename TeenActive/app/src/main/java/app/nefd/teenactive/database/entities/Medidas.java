package app.nefd.teenactive.database.entities;

import java.util.Date;

/**
 * Classe que representa a entidade REGISTRO DE PESO do banco de dados
 */
public class Medidas {

    //region Atributos
    private int codigo = 0;
    private short altura = 0;
    private float peso = 0.000f;
    private Date dataRegistro = null;
    //endregion

    //region Construtores de Classe
    public Medidas() {
    }

    public Medidas(short altura, float peso, Date dataRegistro) {
        this.altura = altura;
        this.peso = peso;
        this.dataRegistro = dataRegistro;
    }

    public Medidas(int codigo, short altura, float peso, Date dataRegistro) {
        this.codigo = codigo;
        this.altura = altura;
        this.peso = peso;
        this.dataRegistro = dataRegistro;
    }

    //endregion

    //region Getters e Setters
    /**
     * Pega o código do objeto Medidas
     * @return
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Define o código do objeto Medidas
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Pega a altura em Cm
     * @return
     */
    public short getAltura() {
        return altura;
    }

    /**
     * Define a altura em Cm
     * @param altura
     */
    public void setAltura(short altura) {
        this.altura = altura;
    }

    /**
     * Pega o peso em Kg
     * @return peso em Kg
     */
    public float getPeso() {
        return peso;
    }

    /**
     * Define o peso em kg
     * @param peso
     */
    public void setPeso(float peso) {
        this.peso = peso;
    }

    /**
     * Pega a data de registro
     * @return data de registro
     */
    public Date getDataRegistro() {
        return dataRegistro;
    }

    /**
     * Define a data de registro
     * @param dataRegistro
     */
    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }
    //endregion
}
