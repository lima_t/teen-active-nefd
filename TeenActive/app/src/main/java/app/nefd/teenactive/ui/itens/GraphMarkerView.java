package app.nefd.teenactive.ui.itens;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import app.nefd.teenactive.R;

/**
 * Classe contém váriaveis e metodos necessários para criar Marcadores de Popup para gráficos
 */
public class GraphMarkerView extends MarkerView {

    //region Variáveis globais
    public TextView labMarkerViewText1, labMarkerViewText2;
    private MPPointF mpPointF;
    //enregion

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     */
    public GraphMarkerView(Context context) {
        super(context, R.layout.graph_markerview);

        //iniciando os componentes
        labMarkerViewText1 = (TextView) findViewById(R.id.labMarkerViewText1);
        labMarkerViewText2 = (TextView) findViewById(R.id.labMarkerViewText2);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {

        if(mpPointF == null) {
            // Centraliza o popup horizontalmente para não sair da tela
            mpPointF = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mpPointF;
    }
}
