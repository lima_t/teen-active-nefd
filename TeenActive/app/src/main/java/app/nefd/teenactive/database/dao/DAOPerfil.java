package app.nefd.teenactive.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Perfil;

/**
 * * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto DAOPerfil do Usuário
 */
public class DAOPerfil {

    private SQLiteDatabase objBanco = null;

    /**
     * Construtor default da classe, automaticamente ela já retorna uma conexão para a classe para que possa efetuar
     * as operações DML durante o NEW
     * @param telaContexto
     */
    public DAOPerfil(Context telaContexto){
        ConexaoBanco objConexaoBanco = new ConexaoBanco(telaContexto);
        objBanco = objConexaoBanco.getWritableDatabase();
    }

    /**
     * Método para inserir um cadastro no perfil (este método só será chamado quando o perfil estiver em branco
     * no banco de dados para que o mesmo possa inserir um registro).
     *
     * @param perfil objeto do tipo Perfil para ser inserido.
     */
    public void inserirPerfil(Perfil perfil) throws SQLiteException, ParseException{
        //Criando objetos
        ContentValues objValoresParametros = new ContentValues();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
        objFormatoData.applyPattern("yyyy-MM-dd");//Altera a data para o padrão americano (o SQLite só entende esse formato)

        //Passando valores do perfil
        objValoresParametros.put("FOTO", perfil.getFotoPerfil());
        objValoresParametros.put("NOME", perfil.getNome());
        objValoresParametros.put("DATA_NASCIMENTO", objFormatoData.format(perfil.getDataNascimento()));
        objValoresParametros.put("SEXO", perfil.getSexo());
        objValoresParametros.put("ALTURA", perfil.getAltura());
        objValoresParametros.put("PESO", perfil.getPeso());

        objBanco.insertOrThrow("TB_PERFIL", null, objValoresParametros);//Tenta inserir
        objBanco.close();

    }

    /**
     * Método para alterar um cadastro no perfil.
     *
     * @param perfil objeto do tipo Perfil para ser alterado.
     */
    public void alterarPerfil(Perfil perfil)throws  SQLiteException, ParseException{
        //Criando objetos
        ContentValues objValoresParametros = new ContentValues();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
        objFormatoData.applyPattern("yyyy-MM-dd");//Altera a data para o padrão americano (o SQLite só entende esse formato)

        //Passando valores do perfil
        objValoresParametros.put("FOTO", perfil.getFotoPerfil());
        objValoresParametros.put("NOME", perfil.getNome());
        objValoresParametros.put("SEXO", perfil.getSexo());
        objValoresParametros.put("DATA_NASCIMENTO", objFormatoData.format(perfil.getDataNascimento()));
        objValoresParametros.put("ALTURA", perfil.getAltura());
        objValoresParametros.put("PESO", perfil.getPeso());

        objBanco.update("TB_PERFIL", objValoresParametros, "CODIGO = 1", null);
        objBanco.close();
    }

    /**
     * Método retorna o perfil cadastrado
     * @return perfil
     */
    public Perfil carregarPerfil() throws SQLiteException, ParseException{

        Perfil objPerfil = new Perfil();
        Cursor cursor = null;

        try{

            cursor = objBanco.query(true, "TB_PERFIL", null, "CODIGO = 1", null, null, null, null, null);

            if(cursor.getCount() > 0 & cursor.moveToFirst()){

                //Convertendo a String da data recuperada no banco
                SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd");

                //Preenche os atributos do objeto perfil com os valores retornados do banco de dados
                objPerfil.setCodigo(cursor.getInt(cursor.getColumnIndex("CODIGO")));
                objPerfil.setFotoPerfil(cursor.getString(cursor.getColumnIndex("FOTO")));
                objPerfil.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
                objPerfil.setDataNascimento(objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA_NASCIMENTO"))));
                objPerfil.setSexo(cursor.getShort(cursor.getColumnIndex("SEXO")));
                objPerfil.setAltura(cursor.getShort(cursor.getColumnIndex("ALTURA")));
                objPerfil.setPeso(cursor.getFloat(cursor.getColumnIndex("PESO")));

            }else{

                throw new NullPointerException("A consulta na tabela do perfil retornou nenhum registro");

            }

        }catch(Exception erro){

            throw erro;

        }finally {

            assert cursor != null;
            cursor.close();
            objBanco.close();
            return objPerfil;

        }
    }
}