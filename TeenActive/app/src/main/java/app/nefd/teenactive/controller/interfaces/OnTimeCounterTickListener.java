package app.nefd.teenactive.controller.interfaces;

/**
 * Classe responsável por definir a interface para o evento da classe TimeCounter
 */
public interface OnTimeCounterTickListener {

    /**
     * Ocorre quando o cronometro troca os segundos.
     */
    void onTimeCounterTickListener(long milliseconds, int minutes, short seconds);
}
