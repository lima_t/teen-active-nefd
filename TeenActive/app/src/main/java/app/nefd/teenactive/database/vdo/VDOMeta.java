package app.nefd.teenactive.database.vdo;

import android.content.Context;

import app.nefd.teenactive.database.dao.DAOMeta;
import app.nefd.teenactive.database.entities.Meta;

/**
 * Classe responsável por armazenar os atributos, construtores e metodos responsáveis por manipular as
 * pré-operações de validação do cadastro de metas no banco de dados
 */
public class VDOMeta {

    //region Variáveis globais
    private Context telaContexto;
    //endregion

    //region Construtores da classe
    public VDOMeta(Context telaContexto) {
        this.telaContexto = telaContexto;
    }

    public void inserirMeta(Meta meta) throws Exception{

        DAOMeta objDAOMeta = new DAOMeta(telaContexto);

        if(meta.getDescricao().equals(null) || meta.getDescricao().isEmpty()){
            throw new Exception("O nome da meta é obrigatório");
        }

        if(meta.getValorSimbolico() <= 0){
            throw new Exception("O valor simbólico da meta deve ser maior que 1");
        }

        objDAOMeta.inserirMeta(meta);//Tenta inserir a meta
    }

    public void alterarMeta(Meta meta) throws Exception{

        if(meta.getCodigo() <= 0){
            throw new Exception("O valor do código da meta à ser atualizada é inválido. Código = "+meta.getCodigo());
        }

        if(meta.getDescricao().equals(null) || meta.getDescricao().isEmpty()){
            throw new Exception("O nome da meta é obrigatório");
        }

        if(meta.getValorSimbolico() <= 0){
            throw new Exception("O valor simbólico da meta deve ser maior que 1");
        }

        new DAOMeta(telaContexto).alterarMeta(meta);
        new DAOMeta(telaContexto).favoritarMeta(meta.getCodigo());
    }
    //endregion

}
