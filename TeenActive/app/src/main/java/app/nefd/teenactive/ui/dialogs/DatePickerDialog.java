package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import app.nefd.teenactive.R;

/**
 * Classe contém todas as funções necessárias para exibir dialogos de seletor de datas
 */
public class DatePickerDialog extends AlertDialog{

    //region Variáveis Globais
    /**
     * Objeto DatePicker global para recuperar as informações em outros contextos
     */
    public DatePicker seletorData;
    //endregion

    //region Construtores

    /**
     * Construtor para o Dialogo de Seletor de Datas
     * @param context Tela de contexto
     * @param titulo Título da caixa dialogo
     */
    public DatePickerDialog(Context context, final String titulo){
        super(context);

        LayoutInflater objLayoutInflater = LayoutInflater.from(context);
        View objTela = objLayoutInflater.inflate(R.layout.datepicker_dialog, null);

        ((TextView)objTela.findViewById(R.id.labTituloDatePickerDialog)).setText(titulo);
        seletorData = (DatePicker) objTela.findViewById(R.id.seletorDataDialog);

        this.setCancelable(false);
        this.setView(objTela);

    }
    //endregion

}
