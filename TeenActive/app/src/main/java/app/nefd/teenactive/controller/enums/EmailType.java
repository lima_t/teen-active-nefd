package app.nefd.teenactive.controller.enums;

import app.nefd.teenactive.controller.utils.AppUtils;

/**
 * Enumeração para definir o tipo da mensagem de e-mail que será enviada através do metodo
 * @see AppUtils
 */
public enum EmailType {
    ENVIAR_OPINIAO,
    RELATORIO_BUG,
    FALAR_DESENVOLVEDOR
}
