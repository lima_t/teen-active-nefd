package app.nefd.teenactive.database.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Classe que representa a entidade Treino do banco de dados
 */
public class Treino implements Parcelable {

    //region Atributos
    private int codigo = 0;
    private int atividadeFk = 0;
    private String atividadeDescricao = "";
    private Date dataTreino = null;
    private long tempoTotal = 0;
    private long tempoOcioso = 0;
    private long tempoIntensidadeLeve = 0;
    private long tempoIntensidadeModerada = 0;
    private long tempoIntensidadeVigorosa = 0;
    private float distanciaPercorrida = 0.000f;
    private float kcalConsumida = 0.00f;
    private int quantidadePassos = 0;
    private boolean treinoAutonomo = false;
    private boolean treinoPassivo = false;
    //endregion

    //region Construtores
    /**
     * Construtor padrão vazio
     */
    public Treino() {}

    /**
     * Construtor com todos os parametros
     * @param atividadeFk
     * @param dataTreino
     * @param tempoTotal
     * @param distanciaPercorrida
     * @param kcalConsumida
     * @param quantidadePassos
     */
    public Treino(int codigo, int atividadeFk, String atividadeDescricao, Date dataTreino, long tempoTotal, long tempoOcioso, long tempoIntensidadeLeve, long tempoIntensidadeModerada, long tempoIntensidadeVigorosa, float distanciaPercorrida, float kcalConsumida, int quantidadePassos, boolean treinoAutonomo, boolean treinoPassivo) {
        this.codigo = codigo;
        this.atividadeFk = atividadeFk;
        this.atividadeDescricao = atividadeDescricao;
        this.dataTreino = dataTreino;
        this.tempoTotal = tempoTotal;
        this.tempoOcioso = tempoOcioso;
        this.tempoIntensidadeLeve = tempoIntensidadeLeve;
        this.tempoIntensidadeModerada = tempoIntensidadeModerada;
        this.tempoIntensidadeVigorosa = tempoIntensidadeVigorosa;
        this.distanciaPercorrida = distanciaPercorrida;
        this.kcalConsumida = kcalConsumida;
        this.quantidadePassos = quantidadePassos;
        this.treinoAutonomo = treinoAutonomo;
        this.treinoPassivo = treinoPassivo;
    }

    /**
     * Construtor com a atividade realizada no formato de String
     * @param atividadeDescricao
     * @param dataTreino
     * @param tempoTotal
     * @param distanciaPercorrida
     * @param kcalConsumida
     * @param quantidadePassos
     */
    public Treino(String atividadeDescricao, Date dataTreino, long tempoTotal, long tempoOcioso, long tempoIntensidadeLeve, long tempoIntensidadeModerada, long tempoIntensidadeVigorosa, float distanciaPercorrida, float kcalConsumida, int quantidadePassos, boolean treinoAutonomo, boolean treinoPassivo) {
        this.atividadeDescricao = atividadeDescricao;
        this.dataTreino = dataTreino;
        this.tempoTotal = tempoTotal;
        this.tempoOcioso = tempoOcioso;
        this.tempoIntensidadeLeve = tempoIntensidadeLeve;
        this.tempoIntensidadeModerada = tempoIntensidadeModerada;
        this.tempoIntensidadeVigorosa = tempoIntensidadeVigorosa;
        this.distanciaPercorrida = distanciaPercorrida;
        this.kcalConsumida = kcalConsumida;
        this.quantidadePassos = quantidadePassos;
        this.treinoAutonomo = treinoAutonomo;
        this.treinoPassivo = treinoPassivo;
    }
    //endregion

    //region Métodos implementados pelo Parcelable
    protected Treino(Parcel in) {
        codigo = in.readInt();
        atividadeFk = in.readInt();
        atividadeDescricao = in.readString();
        dataTreino = (Date) in.readValue(ClassLoader.getSystemClassLoader());
        tempoTotal = in.readLong();
        tempoOcioso = in.readLong();
        tempoIntensidadeLeve = in.readLong();
        tempoIntensidadeModerada = in.readLong();
        tempoIntensidadeVigorosa = in.readLong();
        distanciaPercorrida = in.readFloat();
        kcalConsumida = in.readFloat();
        quantidadePassos = in.readInt();
        treinoAutonomo = (in.readByte()==1);
        treinoPassivo = (in.readByte()==1);
    }

    public static final Creator<Treino> CREATOR = new Creator<Treino>() {
        @Override
        public Treino createFromParcel(Parcel in) {
            return new Treino(in);
        }

        @Override
        public Treino[] newArray(int size) {
            return new Treino[size];
        }
    };

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(codigo);
        dest.writeInt(atividadeFk);
        dest.writeString(atividadeDescricao);
        dest.writeValue(dataTreino);
        dest.writeLong(tempoTotal);
        dest.writeLong(tempoOcioso);
        dest.writeLong(tempoIntensidadeLeve);
        dest.writeLong(tempoIntensidadeModerada);
        dest.writeLong(tempoIntensidadeVigorosa);
        dest.writeFloat(distanciaPercorrida);
        dest.writeFloat(kcalConsumida);
        dest.writeInt(quantidadePassos);
        dest.writeByte((byte)(isTreinoAutonomo()?1:0));
        dest.writeByte((byte)(isTreinoPassivo()?1:0));
    }
    //endregion

    //region Getters e Setters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getAtividadeDescricao() { return atividadeDescricao; }

    public void setAtividadeDescricao(String atividadeDescricao) { this.atividadeDescricao = atividadeDescricao; }

    public int getAtividadeFk() {
        return atividadeFk;
    }

    public void setAtividadeFk(int atividadeFk) {
        this.atividadeFk = atividadeFk;
    }

    public Date getDataTreino() {
        return dataTreino;
    }

    public void setDataTreino(Date dataTreino) {
        this.dataTreino = dataTreino;
    }

    public long getTempoTotal() {
        return tempoTotal;
    }

    public void setTempoTotal(long tempoTotal) {
        this.tempoTotal = tempoTotal;
    }

    public float getDistanciaPercorrida() {
        return distanciaPercorrida;
    }

    public void setDistanciaPercorrida(float distanciaPercorrida) {
        this.distanciaPercorrida = distanciaPercorrida;
    }

    public float getKcalConsumida() {
        return kcalConsumida;
    }

    public void setKcalConsumida(float kcalConsumida) {
        this.kcalConsumida = kcalConsumida;
    }

    public int getQuantidadePassos() {
        return quantidadePassos;
    }

    public void setQuantidadePassos(int quantidadePassos) {
        this.quantidadePassos = quantidadePassos;
    }

    public long getTempoIntensidadeLeve() {
        return tempoIntensidadeLeve;
    }

    public void setTempoIntensidadeLeve(long tempoIntensidadeLeve) {
        this.tempoIntensidadeLeve = tempoIntensidadeLeve;
    }

    public long getTempoIntensidadeModerada() {
        return tempoIntensidadeModerada;
    }

    public void setTempoIntensidadeModerada(long tempoIntensidadeModerada) {
        this.tempoIntensidadeModerada = tempoIntensidadeModerada;
    }

    public long getTempoIntensidadeVigorosa() {
        return tempoIntensidadeVigorosa;
    }

    public void setTempoIntensidadeVigorosa(long tempoIntensidadeVigorosa) {
        this.tempoIntensidadeVigorosa = tempoIntensidadeVigorosa;
    }

    public boolean isTreinoAutonomo() {
        return treinoAutonomo;
    }

    public void setTreinoAutonomo(boolean treinoAutonomo) {
        this.treinoAutonomo = treinoAutonomo;
    }

    public boolean isTreinoPassivo() {
        return treinoPassivo;
    }

    public void setTreinoPassivo(boolean treinoPassivo) {
        this.treinoPassivo = treinoPassivo;
    }

    public long getTempoOcioso() {
        return tempoOcioso;
    }

    public void setTempoOcioso(long tempoOcioso) {
        this.tempoOcioso = tempoOcioso;
    }
    //endregion
}
