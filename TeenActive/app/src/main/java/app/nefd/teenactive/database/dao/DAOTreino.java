package app.nefd.teenactive.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.controller.utils.AppUtils;

/**
 * * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto Registro de Atividades
 */
public class DAOTreino {

    //Variáveis globais
    private SQLiteDatabase objBanco = null;

    //Construtor
    public DAOTreino(Context context) {
        ConexaoBanco objConexaoBanco = new ConexaoBanco(context);
        this.objBanco = objConexaoBanco.getWritableDatabase();
    }

    /**
     * Insere um novo registro de sessão de atividade no banco de dados
     * @param treino Objeto do tipo Sessão com os parâmetros
     * @throws SQLiteException Ocorre quando a instrução SQL está mal-formatada
     * @throws ParseException Ocorre quando não é possível converter as datas para o padrão americano
     */
    public void inserirTreino(Treino treino) throws SQLiteException, ParseException{
        //Criando objetos
        ContentValues objValoresParametros = new ContentValues();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        objFormatoData.applyPattern("yyyy-MM-dd HH:mm:ss");//Altera a data para o padrão americano (o SQLite só entende esse formato)

        //Passando valores do perfil
        objValoresParametros.put("ATIVIDADE_FK", treino.getAtividadeFk());
        objValoresParametros.put("DATA_TREINO", objFormatoData.format(treino.getDataTreino()));
        objValoresParametros.put("TEMPO_TOTAL", treino.getTempoTotal());
        objValoresParametros.put("TEMPO_OCIOSO", treino.getTempoOcioso());
        objValoresParametros.put("TEMPO_LEVE", treino.getTempoIntensidadeLeve());
        objValoresParametros.put("TEMPO_MODERADO", treino.getTempoIntensidadeModerada());
        objValoresParametros.put("TEMPO_VIGOROSO", treino.getTempoIntensidadeVigorosa());
        objValoresParametros.put("DISTANCIA_PERCORRIDA", treino.getDistanciaPercorrida());
        objValoresParametros.put("KCAL_CONSUMIDA", treino.getKcalConsumida());
        objValoresParametros.put("QUANTIDADE_PASSOS", treino.getQuantidadePassos());
        objValoresParametros.put("TREINO_AUTONOMO", treino.isTreinoAutonomo()?1:0);
        objValoresParametros.put("TREINO_PASSIVO", treino.isTreinoPassivo()?1:0);

        objBanco.insertOrThrow("TB_TREINO", null, objValoresParametros);
        objBanco.close();
    }

    /**
     * Retorna uma lista de Sessões de atividades a partir de uma consulta SQL
     * @param filterType filtro de data à ser aplicado na consulta
     * @return Lista de Sessões de atividades
     */
    public ArrayList<Treino> getAtividadesHoje(byte filterType) throws SQLiteException, ParseException{
        //Criando objetos utilizaveis
        ArrayList<Treino> listaTreinos = new ArrayList<>();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //Crianco consulta e cursor
        Cursor cursor = null;

        //Corrigir a consulta aqui o total de tempo e o CURRENT_DATE não funciona
        //region Consultas para o Resumo do dia
        switch (filterType){
            case 0:
                //Tempo
                cursor = objBanco.query("VIEW_SESSOES",
                        new String[]{
                                "CODIGO",
                                "ATIVIDADE_FK",
                                "DESCRICAO",
                                "DATA_TREINO",
                                "sum(TEMPO_TOTAL) AS TEMPO_TOTAL",//Soma o tempo
                                "TEMPO_OCIOSO",
                                "TEMPO_LEVE",
                                "TEMPO_MODERADO",
                                "TEMPO_VIGOROSO",
                                "DISTANCIA_PERCORRIDA",
                                "KCAL_CONSUMIDA",
                                "QUANTIDADE_PASSOS",
                                "TREINO_AUTONOMO",
                                "TREINO_PASSIVO"
                        },
                        "date(DATA_TREINO) = CURRENT_DATE", null, "ATIVIDADE_FK", null, "sum(TEMPO_TOTAL) DESC");
                break;
            case 1:
                //Kcal
                cursor = objBanco.query("VIEW_SESSOES",
                        new String[]{
                                "CODIGO",
                                "ATIVIDADE_FK",
                                "DESCRICAO",
                                "DATA_TREINO",
                                "TEMPO_TOTAL",
                                "TEMPO_OCIOSO",
                                "TEMPO_LEVE",
                                "TEMPO_MODERADO",
                                "TEMPO_VIGOROSO",
                                "DISTANCIA_PERCORRIDA",
                                "sum(KCAL_CONSUMIDA)  AS KCAL_CONSUMIDA",//Soma as kcal
                                "QUANTIDADE_PASSOS",
                                "TREINO_AUTONOMO",
                                "TREINO_PASSIVO"
                        },
                        "date(DATA_TREINO) = CURRENT_DATE", null, "ATIVIDADE_FK", null, "sum(KCAL_CONSUMIDA) DESC");
                break;
            case 2:
                //Passos
                cursor = objBanco.query("VIEW_SESSOES",
                        new String[]{
                                "CODIGO",
                                "ATIVIDADE_FK",
                                "DESCRICAO",
                                "DATA_TREINO",
                                "TEMPO_TOTAL",
                                "TEMPO_OCIOSO",
                                "TEMPO_LEVE",
                                "TEMPO_MODERADO",
                                "TEMPO_VIGOROSO",
                                "DISTANCIA_PERCORRIDA",
                                "KCAL_CONSUMIDA",
                                "sum(QUANTIDADE_PASSOS) AS QUANTIDADE_PASSOS",//Soma a quantidade de passos
                                "TREINO_AUTONOMO",
                                "TREINO_PASSIVO"
                        },
                        "date(DATA_TREINO) = CURRENT_DATE", null, "ATIVIDADE_FK", null, "sum(QUANTIDADE_PASSOS) DESC");
                break;
            case 3:
                //Distância
                cursor = objBanco.query("VIEW_SESSOES",
                        new String[]{
                                "CODIGO",
                                "ATIVIDADE_FK",
                                "DESCRICAO",
                                "DATA_TREINO",
                                "TEMPO_TOTAL",
                                "TEMPO_OCIOSO",
                                "TEMPO_LEVE",
                                "TEMPO_MODERADO",
                                "TEMPO_VIGOROSO",
                                "sum(DISTANCIA_PERCORRIDA) AS DISTANCIA_PERCORRIDA",//Soma a distância percorrida
                                "KCAL_CONSUMIDA",
                                "QUANTIDADE_PASSOS",
                                "TREINO_AUTONOMO",
                                "TREINO_PASSIVO"
                        },
                        "date(DATA_TREINO) = CURRENT_DATE", null, "ATIVIDADE_FK", null, "sum(DISTANCIA_PERCORRIDA) DESC");
                break;
        }
        //endregion

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            do{

                listaTreinos.add(new Treino(
                        cursor.getInt(cursor.getColumnIndex("CODIGO")),
                        cursor.getInt(cursor.getColumnIndex("ATIVIDADE_FK")),
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA_TREINO"))),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_TOTAL")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_OCIOSO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_LEVE")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_MODERADO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_VIGOROSO")),
                        cursor.getFloat(cursor.getColumnIndex("DISTANCIA_PERCORRIDA")),
                        cursor.getFloat(cursor.getColumnIndex("KCAL_CONSUMIDA")),
                        cursor.getInt(cursor.getColumnIndex("QUANTIDADE_PASSOS")),
                        cursor.getInt(cursor.getColumnIndex("TREINO_AUTONOMO"))==1,
                        cursor.getInt(cursor.getColumnIndex("TREINO_PASSIVO"))==1
                ));

            }while(cursor.moveToNext());

        }

        cursor.close();
        objBanco.close();
        return listaTreinos;
    }

    /**
     * Retorna uma lista de treinos de atividades a partir de uma consulta SQL durante um intervalor de datas
     * @param dataInicial Data inicial da consulta
     * @param dataFinal Data final da consulta
     * @return
     * @throws SQLiteException
     * @throws ParseException
     */
    public ArrayList<Treino> getAtividadesPorIntervalo(Date dataInicial, Date dataFinal, byte filterType, boolean isGroupBy) throws SQLiteException, ParseException{
        //Criando objetos utilizaveis
        ArrayList<Treino> listaTreinos = new ArrayList<>();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd");

        Cursor cursor = null;

        //Se a consulta for groupBy é referente ao preenchimento do gráfico, se não é referente
        //ao preenchimento da lista de atividades
        if(isGroupBy){
            //region Consultas para o Histórico
            switch (filterType){
                case 0:
                    //Tempo
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "sum(TEMPO_TOTAL) AS TEMPO_TOTAL",//Soma o tempo
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, "ATIVIDADE_FK", null, "sum(TEMPO_TOTAL) DESC");
                    break;
                case 1:
                    //Kcal
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "sum(KCAL_CONSUMIDA)  AS KCAL_CONSUMIDA",//Soma as kcal
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, "ATIVIDADE_FK", null, "sum(KCAL_CONSUMIDA) DESC");
                    break;
                case 2:
                    //Passos
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "sum(QUANTIDADE_PASSOS) AS QUANTIDADE_PASSOS",//Soma a quantidade de passos
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, "ATIVIDADE_FK", null, "sum(QUANTIDADE_PASSOS) DESC");
                    break;
                case 3:
                    //Distância
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "sum(DISTANCIA_PERCORRIDA) AS DISTANCIA_PERCORRIDA",//Soma a distância percorrida
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, "ATIVIDADE_FK", null, "sum(DISTANCIA_PERCORRIDA) DESC");
                    break;
            }
            //endregion
        }else{
            //region Consultas para o Histórico
            switch (filterType){
                case 0:
                    //Tempo
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, null, null, "CODIGO DESC");
                    break;
                case 1:
                    //Kcal
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, null, null, "CODIGO DESC");
                    break;
                case 2:
                    //Passos
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, null, null, "CODIGO DESC");
                    break;
                case 3:
                    //Distância
                    cursor = objBanco.query("VIEW_SESSOES",
                            new String[]{
                                    "CODIGO",
                                    "ATIVIDADE_FK",
                                    "DESCRICAO",
                                    "DATA_TREINO",
                                    "TEMPO_TOTAL",
                                    "TEMPO_OCIOSO",
                                    "TEMPO_LEVE",
                                    "TEMPO_MODERADO",
                                    "TEMPO_VIGOROSO",
                                    "DISTANCIA_PERCORRIDA",
                                    "KCAL_CONSUMIDA",
                                    "QUANTIDADE_PASSOS",
                                    "TREINO_AUTONOMO",
                                    "TREINO_PASSIVO"
                            },
                            "date(DATA_TREINO) BETWEEN date('"+objFormatoData.format(dataInicial)+"') AND date('"+objFormatoData.format(dataFinal)+"')", null, null, null, "CODIGO DESC");
                    break;
            }
            //endregion
        }

        objFormatoData.applyPattern("yyyy-MM-dd HH:mm:ss");

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            do{

                listaTreinos.add(new Treino(
                        cursor.getInt(cursor.getColumnIndex("CODIGO")),
                        cursor.getInt(cursor.getColumnIndex("ATIVIDADE_FK")),
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA_TREINO"))),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_TOTAL")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_OCIOSO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_LEVE")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_MODERADO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_VIGOROSO")),
                        cursor.getFloat(cursor.getColumnIndex("DISTANCIA_PERCORRIDA")),
                        cursor.getFloat(cursor.getColumnIndex("KCAL_CONSUMIDA")),
                        cursor.getInt(cursor.getColumnIndex("QUANTIDADE_PASSOS")),
                        cursor.getInt(cursor.getColumnIndex("TREINO_AUTONOMO"))==1,
                        cursor.getInt(cursor.getColumnIndex("TREINO_PASSIVO"))==1
                ));

            }while(cursor.moveToNext());

        }

        cursor.close();
        objBanco.close();
        return listaTreinos;
    }

    /**
     * Retorna uma lista de <b>3 posições</b> para as atividades rankeadas de um determinado tipo de meta
     *
     * @param tipoMeta
     * @param valorSimbolico
     * @return
     * @throws SQLiteException
     * @throws ParseException
     * @throws ArrayIndexOutOfBoundsException
     */
    public ArrayList<Treino> getRankedAtividades(short tipoMeta, int valorSimbolico) throws NullPointerException, SQLiteException, ParseException, ArrayIndexOutOfBoundsException{

        ArrayList<Treino> listaTreinos = new ArrayList<>();

        //Convertendo a String da data recuperada no banco
        SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //Crianco consulta e cursor
        Cursor cursor = null;
        long millis = AppUtils.converterMinutosToMillis(valorSimbolico);

        //region Consultando a VIEW_TREINOS
        switch (tipoMeta){
            case 0:
                //Consulta pelo Tempo
                cursor = objBanco.query( "VIEW_SESSOES", null, "TEMPO_TOTAL >= "+AppUtils.converterMinutosToMillis(valorSimbolico), null, null, null, "TEMPO_TOTAL DESC", null);
                break;
            case 1:
                //Consulta pelas Calorias
                cursor = objBanco.query( "VIEW_SESSOES", null, "KCAL_CONSUMIDA >= "+valorSimbolico, null, null, null, "KCAL_CONSUMIDA DESC", null);
                break;
            case 2:
                //Consulta pelos Passos
                cursor = objBanco.query( "VIEW_SESSOES", null, "QUANTIDADE_PASSOS >= "+valorSimbolico, null, null, null, "QUANTIDADE_PASSOS DESC", null);
                break;
            case 3:
                //Consulta pela Distância Percorrida
                cursor = objBanco.query( "VIEW_SESSOES", null, "DISTANCIA_PERCORRIDA >= "+valorSimbolico, null, null, null, "DISTANCIA_PERCORRIDA DESC", null);
                break;
        }
        //endregion

        if(cursor.getCount() > 0 && cursor.moveToFirst()){
            do{

                listaTreinos.add(new Treino(
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA_TREINO"))),
                        cursor.getShort(cursor.getColumnIndex("TEMPO_TOTAL")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_OCIOSO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_LEVE")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_MODERADO")),
                        cursor.getLong(cursor.getColumnIndex("TEMPO_VIGOROSO")),
                        cursor.getFloat(cursor.getColumnIndex("DISTANCIA_PERCORRIDA")),
                        cursor.getFloat(cursor.getColumnIndex("KCAL_CONSUMIDA")),
                        cursor.getInt(cursor.getColumnIndex("QUANTIDADE_PASSOS")),
                        cursor.getInt(cursor.getColumnIndex("TREINO_AUTONOMO"))==1,
                        cursor.getInt(cursor.getColumnIndex("TREINO_PASSIVO"))==1
                ));

            }while(cursor.moveToNext());
        }

        cursor.close();
        objBanco.close();
        return listaTreinos;

    }
}
