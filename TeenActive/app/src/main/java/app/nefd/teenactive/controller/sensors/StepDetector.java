package app.nefd.teenactive.controller.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import app.nefd.teenactive.controller.enums.AccelerometerSignals;
import app.nefd.teenactive.controller.interfaces.StepListener;

/**
 * Classe para a detectação de passos
 */
public class StepDetector implements SensorEventListener {

    //region Variáveis globais
    private double[] vResultadoAceleracao = new double[AccelerometerSignals.count];
    private AccelDataProcessor accelProcessing = AccelDataProcessor.getInstance();
    private SensorManager mSensorManager;
    private Sensor acelerometro;
    private StepListener stepListener;
    //endregion

    //region Construtor
    /**
     * Construtor padrão da classe
     * @param sensorManager Passar um sensor do tipo acelerometro como parâmetro
     */
    public StepDetector(SensorManager sensorManager) {
        this.stepListener = null;
        this.mSensorManager = sensorManager;

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            acelerometro = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            Log.d("StepDetector", "Sucesso! Acelerometro com Resolução:" + acelerometro.getResolution()
                    + " Escala máxima: " + acelerometro.getMaximumRange()
                    + "\n Tempo de intervalo: " + acelerometro.getMinDelay() / 1000 + "ms.");
        } else {
            Log.d("StepDetector", "Falha! O Sensor passado por parâmetro não é do tipo Acelerometro");
        }
    }
    //endregion

    /**
     * Listener para escutar o evento do passo
     * @param listener a listener.
     */
    public void setOnStepListener(StepListener listener) {
        this.stepListener = listener;
    }

    /**
     * Inicia o sensor de acelerometria
     * @throws Exception
     */
    public void iniciarDetector() throws Exception {
        if (!mSensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_GAME)) {
            throw new Exception("O acelerometro não é suportado por este aparelho.");
        }
    }

    /**
     * Para o sensor de acelerometria
     */
    public void pararDetector() {
        mSensorManager.unregisterListener(this, acelerometro);
    }

    /**
     * Called when sensor values have changed.
     * <p>See {@link SensorManager SensorManager}
     * for details on possible sensor types.
     * <p>See also {@link SensorEvent SensorEvent}.
     * <p>
     * <p><b>NOTE:</b> The application doesn't own the
     * {@link SensorEvent event}
     * object passed as a parameter and therefore cannot hold on to it.
     * The object may be part of an internal pool and may be reused by
     * the framework.
     *
     * @param event the {@link SensorEvent SensorEvent}.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        // handle accelerometer data
        accelProcessing.setEvent(event);
        final long eventMsecTime = accelProcessing.timestampToMilliseconds();

        vResultadoAceleracao[0] = accelProcessing.calcMagnitudeVector(0);
        vResultadoAceleracao[0] = accelProcessing.calcExpMovAvg(0);
        vResultadoAceleracao[1] = accelProcessing.calcMagnitudeVector(1);


        //Detecção do passo
        if (accelProcessing.stepDetected(1)) {
            //Caso retorne verdadeiro então um passo foi detectado
            //Notifica o listener
            if (stepListener != null)
                stepListener.onStep(eventMsecTime);
        }
    }

    /**
     * Called when the accuracy of the registered sensor has changed.
     * <p>
     * <p>See the SENSOR_STATUS_* constants in
     * {@link SensorManager SensorManager} for details.
     *
     * @param sensor
     * @param accuracy The new accuracy of this sensor, one of
     *                 {@code SensorManager.SENSOR_STATUS_*}
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Não faz nada por enquanto
    }
}
