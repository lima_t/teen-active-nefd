package app.nefd.teenactive.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import app.nefd.teenactive.controller.utils.CientificUtils;
import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Referencia;

/**
 * * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto Referência
 */
public class DAOReferencia {

    private SQLiteDatabase objBanco = null;

    /**
     * Construtor default da classe, automaticamente ela já retorna uma conexão para a classe para que possa efetuar
     * as operações DML durante o NEW
     * @param telaContexto
     */
    public DAOReferencia(Context telaContexto){
        ConexaoBanco objConexaoBanco = new ConexaoBanco(telaContexto);
        objBanco = objConexaoBanco.getReadableDatabase();
    }

    /**
     * Retorna um objeto <b>Referencia</b> com os valores filtrados pela busca
     * @param idadeMeses Idade em meses
     * @param varReferencia 0 = IMC; 1 = Peso; 2 = Altura
     * @throws SQLiteException
     */
    public Referencia consultarReferencia(short idadeMeses, byte varReferencia, short sexo) throws SQLiteException{

        Cursor cursor = objBanco.query("TB_REFERENCIA", null, "VAR_REFERENCIA = "+varReferencia+" AND SEXO = "+sexo+" AND IDADE_MESES = "+idadeMeses, null, null, null, null, "1");
        Referencia objReferencia = new Referencia();

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            objReferencia.setCodigo(cursor.getInt(cursor.getColumnIndex("CODIGO")));
            objReferencia.setVariavelReferencia((byte)cursor.getShort(cursor.getColumnIndex("VAR_REFERENCIA")));
            objReferencia.setSexo(cursor.getShort(cursor.getColumnIndex("SEXO")));
            objReferencia.setIdadeMeses(cursor.getShort(cursor.getColumnIndex("IDADE_MESES")));
            objReferencia.setPercentil3(cursor.getFloat(cursor.getColumnIndex("PERCENTIL_3")));
            objReferencia.setPercentil15(cursor.getFloat(cursor.getColumnIndex("PERCENTIL_15")));
            objReferencia.setPercentil50(cursor.getFloat(cursor.getColumnIndex("PERCENTIL_50")));
            objReferencia.setPercentil85(cursor.getFloat(cursor.getColumnIndex("PERCENTIL_85")));
            objReferencia.setPercentil97(cursor.getFloat(cursor.getColumnIndex("PERCENTIL_97")));

        }

        cursor.close();
        objBanco.close();

        return objReferencia;
    }

    /**
     * Pega a mediana do IMC a partir dos dados dos usuários
     * @param perfil Perfil do usuário com todos os dados
     * @return percentil 50 do IMC pela idade
     * @throws Exception Retornada caso o cursor não retorne nenhum registro
     */
    public float getMedianaImc(Perfil perfil) throws Exception{
        if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) > 19){
            return 25.0f;//25,0 é a mediana do do IMC para adultos

        }else{
            Cursor cursor = objBanco.query("TB_REFERENCIA", null, "VAR_REFERENCIA = "+Referencia.VAR_IMC+" AND SEXO = "+perfil.getSexo()+" AND IDADE_MESES = "+ CientificUtils.calcularIdadeMeses(perfil.getDataNascimento()), null, null, null, null, "1");

            if(cursor.getCount() > 0 && cursor.moveToFirst()){
                float percentil50 = cursor.getFloat(cursor.getColumnIndex("PERCENTIL_50"));

                objBanco.close();
                cursor.close();

                return percentil50;

            }else{
                objBanco.close();
                cursor.close();
                throw new Exception("A consulta não retornou referências para a mediana do IMC.");
            }

        }
    }

    public float getAlturaIdeal(Perfil perfil) throws Exception{
        if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) > 19){
            return 0.0f;//É adulto e retorna nada

        }else{
            Cursor cursor = objBanco.query("TB_REFERENCIA", null, "VAR_REFERENCIA = "+Referencia.VAR_ALTURA+" AND SEXO = "+perfil.getSexo()+" AND IDADE_MESES = "+ CientificUtils.calcularIdadeMeses(perfil.getDataNascimento()), null, null, null, null, "1");

            if(cursor.getCount() > 0 && cursor.moveToFirst()){
                float percentil50 = cursor.getFloat(cursor.getColumnIndex("PERCENTIL_50"));

                objBanco.close();
                cursor.close();

                return percentil50;

            }else{
                objBanco.close();
                cursor.close();
                throw new Exception("A consulta não retornou referências para a altura ideal.");
            }

        }
    }
}
