package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import app.nefd.teenactive.R;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.fragments.FragmentBemVindo1;
import app.nefd.teenactive.ui.fragments.FragmentBemVindo2;
import app.nefd.teenactive.ui.fragments.FragmentBemVindo3;
import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityBemVindo extends AppCompatActivity {

    //region Variáveis Globais
    private SectionsPagerAdapter objSectionsPagerAdapter;
    private ViewPager objViewPager;

    //Cria os fragmentos que serão exibidos
    private FragmentBemVindo1 fragment1;
    private FragmentBemVindo2 fragment2;
    private FragmentBemVindo3 fragment3;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bem_vindo);

        try{

            //Verifica se é o primeiro acesso
            if(new Configuracao(ActivityBemVindo.this).isPrimeiroAcesso()){

                    //region Iniciando os componentes
                    fragment1 = new FragmentBemVindo1();
                    fragment2 = new FragmentBemVindo2();
                    fragment3 = new FragmentBemVindo3();

                    //Define a tela de contexto para eles
                    fragment1.context = ActivityBemVindo.this;
                    fragment2.context = ActivityBemVindo.this;
                    fragment3.context = ActivityBemVindo.this;

                    objSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                    objViewPager = (ViewPager) findViewById(R.id.container);
                    objViewPager.setAdapter(objSectionsPagerAdapter);

            }else{

                Intent objIntent = new Intent(ActivityBemVindo.this, ActivityPrincipal.class);
                startActivity(objIntent);
                finish();
            }

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityBemVindo.this, "Erro Fatal",
                    "Ocorreu um erro enquanto o aplicativo iniciava.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Finalizar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                grantResults[2] == PackageManager.PERMISSION_GRANTED){

            //Se o usuário deu todas as permissões desabilita o botão de dar permissões
            fragment2.setComponentsGranted();

        }else{

            //Se o usuário negou a permissão ao GPS então mostra uma mensagem de que não pode continuar
            Toast.makeText(ActivityBemVindo.this,
                    "Uma ou mais permissões foram negadas, não é possível monitorar as atividades sem essas permissões",
                    Toast.LENGTH_LONG)
                    .show();
        }

    }


    //region Classe SectionsAdapter
    /**
     * Classe interna para controlar a navegação das paginas tabuladas deslizantes
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return fragment1;
                case 1:
                    return fragment2;
                case 2:
                    return fragment3;
                default:
                    return fragment1;
            }
        }

        /**
         * Retorna o número de sessões da activity tabulada
         * @return
         */
        @Override
        public int getCount() {
            return 3;
        }

    }
    //endregion
}
