package app.nefd.teenactive.ui.screens;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Calendar;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.dialogs.TaskDialog;
import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.enums.Intensity;
import app.nefd.teenactive.controller.interfaces.StepListener;
import app.nefd.teenactive.controller.sensors.AccelDataProcessor;
import app.nefd.teenactive.controller.sensors.StepDetector;
import app.nefd.teenactive.controller.utils.AnimationUtils;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.CientificUtils;
import app.nefd.teenactive.controller.utils.ConnectivityUtils;
import app.nefd.teenactive.controller.utils.IntensityUtils;

public class ActivityNovaAtividade extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    //region Variáveis globais
    private Chronometer cronTempoDecorrido;
    private GoogleMap googleMap;
    private LocationManager locationManager;
    private Polyline rota;
    private ArrayList<LatLng> listaCoordenadas;
    private TextView labDistanciaPercorrida, labKcalConsumida, labPassosEfetuados, labIntensidade;
    private FloatingActionButton fabIniciarAtividade;
    private SeekBar barraDesbloqueio;
    private Button btSelecionarAtividade;

    //Auxiliares do treino
    private Treino treino;
    private int passos = 0;
    private float kcal = 0.0f;
    private float distancia = 0.0f;
    private long tempo = 0;

    private Atividade atividade;
    private Perfil perfil;

    private short limitSeconds = -1;
    private short passosLast15seg = 0;
    private Intensity intensidadeAtividade = Intensity.SEDENTARIA;
    private StepDetector stepDetector;
    private boolean isStarted = false;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_atividade);

        try {

            Toolbar barraAcaoNovaAtividade = (Toolbar) findViewById(R.id.barraAcaoNovaAtividade);
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
            final IntensityUtils intensityUtils;
            final Configuracao config = new Configuracao(ActivityNovaAtividade.this);
            final float tmbMin;

            //region Vinculando os componentes ao XML
            //Componentes na classe do serviço
            treino = new Treino();
            atividade = new Atividade();
            perfil = new DAOPerfil(ActivityNovaAtividade.this).carregarPerfil();
            tmbMin = CientificUtils.calcularTMBmin(perfil);
            intensityUtils = new IntensityUtils(atividade, CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()));

            listaCoordenadas = new ArrayList<>();

            btSelecionarAtividade = (Button) findViewById(R.id.btSelecionarAtividade);
            barraDesbloqueio = (SeekBar) findViewById(R.id.unlockBar);
            fabIniciarAtividade = (FloatingActionButton) findViewById(R.id.fabIniciarAtividade);
            cronTempoDecorrido = (Chronometer) findViewById(R.id.cronTempoDecorrido);
            labDistanciaPercorrida = (TextView) findViewById(R.id.labDistanciaPercorrida);
            labKcalConsumida = (TextView) findViewById(R.id.labKcalConsumida);
            labPassosEfetuados = (TextView) findViewById(R.id.labPassosEfetuados);
            labIntensidade = (TextView) findViewById(R.id.labIntensidade);
            mapFragment.getMapAsync(ActivityNovaAtividade.this);
            //endregion

            //region Iniciando o detector de passos
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            stepDetector = new StepDetector((SensorManager) getSystemService(Context.SENSOR_SERVICE));
            //endregion

            //region Setando os valores dos componentes
            setSupportActionBar(barraAcaoNovaAtividade);

            if(config.isTreinoPassivo()){
                this.setTitle("Novo Treino (Modo Passivo)");
                treino.setTreinoPassivo(true);
            }

            if(!atividade.isPassosAtivo()){
                intensidadeAtividade = Intensity.MODERADA;
                labIntensidade.setText("Esforço Constante");
            }

            //É obrigatório haver a checagem da permissão em tempo real, caso não haja acesso a localização
            //É lançada uma exceção.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //Define critérios de localização e o melhor provedor (Internet ou GPS)
                Criteria bestProvider = new Criteria();
                bestProvider.setAccuracy(Criteria.ACCURACY_FINE);

                //Registra o listener para ficar capturando a localização do usuário
                locationManager.requestLocationUpdates(locationManager.getBestProvider(bestProvider, true), 0, 0, ActivityNovaAtividade.this);


            } else {
                MessageDialog.exibirDialogoErro(ActivityNovaAtividade.this, "Erro", "A permissão para acessar a localização através do GPS foi desabilitada, verifique as configurações do privacidade do aplicativo!")
                        .setPositiveButton("Verificar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent objIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:app.nefd.teenactive"));
                                startActivity(objIntent);
                            }
                        })
                        .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }
            //endregion

            //region Evento dos componentes
            fabIniciarAtividade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Colocar ação de iniciar a atividade aqui
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(AppUtils.CLICK_VIBRATION);

                    if (isStarted) {
                        finalizarAtividade();
                        isStarted = false;

                    } else {
                        isStarted = iniciarAtividade();
                    }

                }
            });

            btSelecionarAtividade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(AppUtils.CLICK_VIBRATION);

                    chamarDialogoSeletorAtividade();
                }
            });

            barraDesbloqueio.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    //Nâo faz nada
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    //Nâo implementado
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if(seekBar.getProgress() > 90){

                        bloquearTela(false);

                    }else{
                        seekBar.setProgress(5);
                    }
                }
            });

            cronTempoDecorrido.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {

                    tempo = SystemClock.elapsedRealtime() - chronometer.getBase();
                    chronometer.setText(AppUtils.formatarMillisToHoras(tempo));

                    //region vai verificar a intensidade da atividade
                    if(limitSeconds == 15){

                        if(!barraDesbloqueio.isShown()){
                            bloquearTela(true);
                            barraDesbloqueio.setProgress(5);
                        }

                        if(!config.isTreinoPassivo() || !atividade.isPassosAtivo()){
                            //Se a função de treino passivo não estiver habilitada ele faz a verificação por passos da intensidades
                            intensidadeAtividade = intensityUtils.getActivityIntensityBySteps(passosLast15seg);
                            passosLast15seg = 0;

                            //region Seta o texto da intensidade e incrementa o tempo da mesma
                            switch (intensidadeAtividade){
                                case SEDENTARIA:
                                    labIntensidade.setText("Ocioso");
                                    treino.setTempoOcioso(treino.getTempoOcioso()+15000);
                                    break;
                                case LEVE:
                                    labIntensidade.setText("Esforço Leve");
                                    treino.setTempoIntensidadeLeve(treino.getTempoIntensidadeLeve()+15000);
                                    break;
                                case MODERADA:
                                    labIntensidade.setText("Esforço Moderado");
                                    treino.setTempoIntensidadeModerada(treino.getTempoIntensidadeModerada()+15000);
                                    break;
                                case VIGOROSA:
                                    labIntensidade.setText("Esforço Intenso");
                                    treino.setTempoIntensidadeVigorosa(treino.getTempoIntensidadeVigorosa()+15000);
                                    break;
                                default:
                                    //Não faz nada.
                                    break;
                            }
                            //endregion
                        }

                        limitSeconds = 1;

                    }else{
                        limitSeconds++;
                    }
                    //endregion

                    //region Calcula o gasto calórico a partir da intensidade
                    switch (intensidadeAtividade){
                        case SEDENTARIA:
                            kcal = CientificUtils.calcularGastoCalorico(1.2f, tmbMin, tempo);
                            break;
                        case LEVE:
                            kcal = CientificUtils.calcularGastoCalorico(atividade.getMetsLeve(), tmbMin, tempo);
                            break;
                        case MODERADA:
                            kcal = CientificUtils.calcularGastoCalorico(atividade.getMetsModerado(), tmbMin, tempo);
                            break;
                        case VIGOROSA:
                            kcal = CientificUtils.calcularGastoCalorico(atividade.getMetsIntenso(), tmbMin, tempo);
                            break;
                        default:
                            //Não faz nada.
                            break;
                    }
                    //endregion

                    labKcalConsumida.setText(String.format(AppUtils.pt_BR, "%.2f kcal", kcal));
                }
            });
            //endregion

        } catch (final Exception erro) {
            MessageDialog.exibirDialogoErro(ActivityNovaAtividade.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n" + erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Configuracao config = new Configuracao(ActivityNovaAtividade.this);

        //Se o modo de treino passivo estiver ativado ele habilita o menu da seleção da intensidade
        if(config.isTreinoPassivo()){
            //Cria o menu na barra de ação
            getMenuInflater().inflate(R.menu.menu_nova_atividade, menu);
            menu.setGroupCheckable(R.id.groupMenuIntensidade, true, true);
            intensidadeAtividade = Intensity.MODERADA;
            return true;

        }else{
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(!isStarted){
            switch (item.getItemId()){
                case R.id.itemAtividadeLeve:
                    intensidadeAtividade = Intensity.LEVE;
                    labIntensidade.setText("Esforço Leve");
                    break;
                case R.id.itemAtividadeModerada:
                    intensidadeAtividade = Intensity.MODERADA;
                    labIntensidade.setText("Esforço Moderado");
                    break;
                case R.id.itemAtividadeIntensa:
                    intensidadeAtividade = Intensity.VIGOROSA;
                    labIntensidade.setText("Esforço Intenso");
                    break;
                default:
                    //Não faz nada
                    break;
            }

            item.setChecked(true);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {

        if(isStarted){
            exibirNotificacao(false);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if(isStarted){
            exibirNotificacao(false);
        }
        super.onResume();
    }

    @Override
    protected void onUserLeaveHint() {

        if(isStarted){
            exibirNotificacao(true);
        }

        super.onUserLeaveHint();
    }

    @Override
    public void onBackPressed() {
        if (isStarted) {
            Toast.makeText(ActivityNovaAtividade.this, "Não é possível retornar enquanto você estiver em um atividade.", Toast.LENGTH_SHORT).show();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Chama o dialogo seletor da atividade
     */
    private void chamarDialogoSeletorAtividade() {
        final TaskDialog objTaskDialog = new TaskDialog(ActivityNovaAtividade.this);
        objTaskDialog.listViewAtividades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                atividade = objTaskDialog.listaAtividades.get(position);
                btSelecionarAtividade.setText(atividade.getDescricao());
                objTaskDialog.dismiss();

            }
        });
        objTaskDialog.show();
    }

    /**
     * Efetua os procedimentos para iniciar o monitoramente da atividade
     * @return TRUE = Sucesso; FALSE = não pode inicia a atividade
     */
    private boolean iniciarAtividade() {

        try {

            //Verifica se está no modo avião
            if (ConnectivityUtils.isModoAviaoAtivo(ActivityNovaAtividade.this)) {
                return false;//Sai do metodo e retorna falso porque está no modo avião
            }

            //Verifica se o GPS está habilitado
            if(!ConnectivityUtils.isGpsAtivo(ActivityNovaAtividade.this)){
                return false;//Sai do metodo e retorna falso porque o GPS está desativado
            }

            //Verifica se o GPS está no modo de alta precisão
            if(!ConnectivityUtils.isGPSModoPrecisao(ActivityNovaAtividade.this)){
                return false;//Sai do metodo e retorna falso porque o GPS não está no modo de alta prescisão
            }

            //Verifica se algum atividade foi selecionada
            if (atividade.getCodigo() <= 0) {
                MessageDialog.exibirDialogoInformacao(ActivityNovaAtividade.this, "Aviso", "Nenhuma atividade foi selecionada ainda, utilize o botão das modalidades para selecionar a tarefa que será praticada")
                        .setNeutralButton("OK", null)
                        .show();
                return false;//Sai do metodo e retorna falso porque nenhuma atividade foi selecionada
            }

            //Iniciando o cronômetro e setando os componentes
            cronTempoDecorrido.setBase(SystemClock.elapsedRealtime());

            //Se for colocar um atraso de 3 segundos para iniciar atividade incluir aqui

            cronTempoDecorrido.start();
            fabIniciarAtividade.setImageResource(R.mipmap.icone_parar_atividade);
            bloquearTela(true);
            btSelecionarAtividade.setEnabled(false);


            //Setando valores escolhidos para o treino
            treino.setDataTreino(Calendar.getInstance().getTime());
            treino.setAtividadeDescricao(atividade.getDescricao());
            treino.setAtividadeFk(atividade.getCodigo());
            treino.setTreinoAutonomo(false);

            //Inicia o registro dos movimentos detectados pelo acelerometro
            configureLimiarPedometro();

            //Se a pedometria estiver permitida ele registra o listener para contar os passos
            if(atividade.isPassosAtivo()){
                stepDetector.iniciarDetector();
                stepDetector.setOnStepListener(new StepListener() {
                    @Override
                    public void onStep(long eventMsecTime) {
                        passos++;
                        passosLast15seg++;
                        labPassosEfetuados.setText(String.format(AppUtils.pt_BR, "%d pas", passos));
                    }
                });
            }

            this.setTitle("Treino em Andamento...");

            return true;

        } catch (Exception erro) {
            Log.e("Teen Active", erro.toString());
            return false;
        }

    }

    /**
     * Efetua os procedimentos para parar o monitoramente da atividade
     */
    private void finalizarAtividade() {

        //Para os contadores
        stepDetector.pararDetector();
        cronTempoDecorrido.stop();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //Para o monitoramento via GPS e desregistra o listener
            locationManager.removeUpdates(ActivityNovaAtividade.this);

        } else {

            MessageDialog.exibirDialogoAlerta(ActivityNovaAtividade.this, "Aviso", "A permissão para acessar a localização através do GPS foi negada")
                    .setNeutralButton("OK", null)
                    .show();
        }

        treino.setTempoTotal(tempo);
        treino.setKcalConsumida(kcal);
        treino.setQuantidadePassos(passos);
        treino.setDistanciaPercorrida(distancia);

        //region Acrescenta o tempo adicional restante à respectiva intensidade
        switch (intensidadeAtividade){
            case SEDENTARIA:
                treino.setTempoOcioso(treino.getTempoOcioso()+(treino.getTempoTotal() - somaIntensidades()));
                break;
            case LEVE:
                treino.setTempoIntensidadeLeve(treino.getTempoIntensidadeLeve()+(treino.getTempoTotal() - somaIntensidades()));
                break;
            case MODERADA:
                treino.setTempoIntensidadeModerada(treino.getTempoIntensidadeModerada()+(treino.getTempoTotal() - somaIntensidades()));
                break;
            case VIGOROSA:
                treino.setTempoIntensidadeVigorosa(treino.getTempoIntensidadeVigorosa()+(treino.getTempoTotal() - somaIntensidades()));
                break;
            default:
                //Não faz nada.
                break;
        }
        //endregion

        //Quando o ponteiro chegar aqui, o objeto treino já estará preenchido com os devidos valores
        Intent objIntent = new Intent(ActivityNovaAtividade.this, ActivityDetalheTreino.class);
        objIntent.putExtra("treino", treino);
        objIntent.putExtra("atividade_icone", atividade.getResourceId());//Alterar essa linha para passar o resourceID da atividade
        startActivity(objIntent);

        this.finish();
    }

    /**
     * Configura o limiar de passos inicial do acelerometro
     */
    private void configureLimiarPedometro() {
        AccelDataProcessor accelDataProcessor = AccelDataProcessor.getInstance();
        Configuracao config = new Configuracao(ActivityNovaAtividade.this);
        accelDataProcessor.setLimiar(config.getSensibilidade());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(googleMap != null){

            this.googleMap = googleMap;
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                this.googleMap.setMyLocationEnabled(true);
                this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                Location location = this.googleMap.getMyLocation();

                    if (location != null) {
                        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16f));
                    }

            }else{

                MessageDialog.exibirDialogoAlerta(ActivityNovaAtividade.this, "Aviso", "A permissão para acessar a localização através do GPS foi desabilitada, verifique as permissões do aplicativo!")
                        .setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent objIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:app.nefd.teenactive"));
                                startActivity(objIntent);
                            }
                        })
                        .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();//finaliza a activity
                            }
                        })
                        .show();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if(location != null){

            LatLng coordenadas = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate updatePoint = CameraUpdateFactory.newLatLngZoom(coordenadas, 16f);
            googleMap.animateCamera(updatePoint);

            if(isStarted && atividade.isDistanciaAtiva()){
                listaCoordenadas.add(coordenadas);
                desenharRota();
                distancia = getDistance();//Pega a distância percorrida
                labDistanciaPercorrida.setText(
                        distancia >= 1000f?
                                String.format(AppUtils.pt_BR, "%.1f km", distancia/1000):
                                String.format(AppUtils.pt_BR, "%d m", (int)distancia)
                );
            }
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //Metodo não implementado por enquanto
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Não faz nada :(
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Não faz nada :(
    }

    /**
     * Traça uma polilinha baseada nos valores da lista de coordenadas capturadas pelo GPS
     */
    private void desenharRota(){
        PolylineOptions rotaOptions;

        if(rota == null){
            rotaOptions = new PolylineOptions();

            for(int index = 0; index < listaCoordenadas.size(); index++){
                rotaOptions.add(listaCoordenadas.get(index));
            }

            rotaOptions.color(Color.RED);
            rotaOptions.visible(true);
            rotaOptions.width(8f);

            rota = googleMap.addPolyline(rotaOptions);

        }else{
            rota.setPoints(listaCoordenadas);
        }
    }

    /**
     * Bloqueia ou desbloqueia a tela do treino durante a atividade
     * @param isLockScreen TRUE  = bloqueia os componentes e exibe o FlipButton<br>
     *                     FALSE = desbloqueia os componentes
     */
    private void bloquearTela(boolean isLockScreen){
        if(isLockScreen){
            //Bloqueia a tela
            fabIniciarAtividade.setEnabled(false);
            AnimationUtils.fadding(fabIniciarAtividade, 1000, (short)100, (short)50);
            barraDesbloqueio.setVisibility(View.VISIBLE);

        }else{
            //Desbloqueia a tela
            fabIniciarAtividade.setEnabled(true);
            AnimationUtils.fadding(fabIniciarAtividade, 1000, (short)50, (short)100);
            barraDesbloqueio.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Calcula a distância total percorrida pelo usuário, baseado no calculo do percurso da polilinha
     * @return Distância em metros
     */
    private float getDistance(){
        float distancia = 0.0f;

        for(int index = 0; index < listaCoordenadas.size(); index++){
            if(index < (listaCoordenadas.size() - 1)){
                distancia += CientificUtils.calcularDistanciaByGoogle(listaCoordenadas.get(index), listaCoordenadas.get(index+1));
            }
        }

        return distancia;
    }

    /**
     * Cria um notificação quando o usuário sai da tela da atividade para iniciar o serviço de monitoramento
     * @param show TRUE = Exibi a notificação; FALSE = Cancela uma notificação ativa se houver
     */
    private void exibirNotificacao(boolean show){

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.content_notificacao);
        contentView.setImageViewResource(R.id.imgIconeNotificacao, atividade.getResourceId());
        contentView.setTextViewText(R.id.labTituloNotificacao, atividade.getDescricao()+" em Andamento...");
        contentView.setTextViewText(R.id.labDetalheNotificacao, "Clique aqui para restaurar a janela da atividade!");

        NotificationManager objNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent objItentNotification = new Intent(getBaseContext(), ActivityNovaAtividade.class);

        NotificationCompat.Builder notificacao = new NotificationCompat.Builder(getBaseContext());
        notificacao.setContent(contentView);
        notificacao.setSmallIcon(R.drawable.icone_lembrete);
        notificacao.setSound(null);
        notificacao.setAutoCancel(true);
        notificacao.setOngoing(true);
        notificacao.setCategory(Notification.CATEGORY_SERVICE);
        notificacao.setDefaults(Notification.PRIORITY_HIGH);
        notificacao.setContentIntent(
                PendingIntent.getActivity(getBaseContext(), 0, objItentNotification, PendingIntent.FLAG_UPDATE_CURRENT)
        );

        if(show){

            objNotificationManager.notify(getApplicationContext().hashCode(), notificacao.build());

        }else{
            objNotificationManager.cancel(getApplicationContext().hashCode());
        }
    }

    /**
     * Calcula a soma dos valores acumulados nas 4 intensidades monitoradas durante a atividade. Esse metodo é usado para calcular
     * a diferença de tempo restante em segundos ao finalizar a atividade
     * @return Total em millisegundos
     */
    private long somaIntensidades(){
        return treino.getTempoOcioso() + treino.getTempoIntensidadeLeve() + treino.getTempoIntensidadeModerada() + treino.getTempoIntensidadeVigorosa();
    }
}