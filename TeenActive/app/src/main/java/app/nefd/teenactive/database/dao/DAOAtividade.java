package app.nefd.teenactive.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Atividade;

/**
 * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto DAOAtividade
 */
public class DAOAtividade {

    //region Variáveis Globais
    private SQLiteDatabase objBanco = null;
    //endregion

    //region Construtores

    /**
     * Construtor padrão da classe
     * @param context
     */
    public DAOAtividade(Context context){
        ConexaoBanco objConexaoBanco = new ConexaoBanco(context);
        objBanco = objConexaoBanco.getWritableDatabase();
    }
    //endregion

    /**
     * Retorna um objeto do tipo Atividade com todos os atributos preenchidos a partir de uma consulta
     * @param codigo Código da atividade para a consulta
     * @return Atividade preenchida caso a consulta retorne registros ou new Atividade() em branco
     * @throws SQLiteException
     */
    public Atividade getAtividade(int codigo) throws SQLiteException{

        //Criando objetos
        Atividade atividade = new Atividade();
        Cursor cursor = objBanco.query("TB_ATIVIDADE", null, "CODIGO = "+codigo, null, null, null, null, "1");

        if(cursor.getCount() > 0 && cursor.moveToFirst()){
            atividade = new Atividade(
                    cursor.getInt(cursor.getColumnIndex("CODIGO")),
                    cursor.getInt(cursor.getColumnIndex("RESOURCE_ID")),
                    cursor.getShort(cursor.getColumnIndex("CATEGORIA")),
                    cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                    cursor.getFloat(cursor.getColumnIndex("METS_LEVE")),
                    cursor.getFloat(cursor.getColumnIndex("METS_MODERADO")),
                    cursor.getFloat(cursor.getColumnIndex("METS_INTENSO")),
                    cursor.getInt(cursor.getColumnIndex("PASSOS_ATIVO")) == 1,
                    cursor.getInt(cursor.getColumnIndex("DISTANCIA_ATIVA")) == 1,
                    cursor.getInt(cursor.getColumnIndex("MODALIDADE_FAVORITA")) == 1
            );
        }

        objBanco.close();
        cursor.close();
        return atividade;
    }

    /**
     * Marca uma atividade como favorita a partir do seu respectivo código
     * @param codigo Código da atividade
     * @throws SQLiteException
     */
    public void adicionarAtividadeFavorita(int codigo) throws SQLiteException{
        objBanco.execSQL("UPDATE TB_ATIVIDADE SET MODALIDADE_FAVORITA = 1 WHERE CODIGO = "+codigo);
        objBanco.close();
    }

    /**
     * Remove uma atividade como favorita a partir do seu respectivo código
     * @param codigo Código da atividade
     * @throws SQLiteException
     */
    public void removerAtividadeFavorita(int codigo) throws SQLiteException{
        objBanco.execSQL("UPDATE TB_ATIVIDADE SET MODALIDADE_FAVORITA = 0 WHERE CODIGO = "+codigo);
        objBanco.close();
    }

    /**
     * Remove todas as metas favoritadas marcando a flag MODALIDADE_FAVORITA = false
     * @throws SQLiteException
     */
    public void resetarMetasFavoritas() throws SQLiteException{
        objBanco.execSQL("UPDATE TB_ATIVIDADE SET MODALIDADE_FAVORITA = 0 WHERE MODALIDADE_FAVORITA = 1;");
        objBanco.close();
    }

    /**
     * Retorna uma lista de atividades de acordo com o filtro enviado por parâmetro
     * @param filterType código com o filtro da consulta das atividades <br>
     *                   <b>-1 = Seleciona as não-favoritas;</b><br>
     *                   <b>0 = Seleciona todas;</b><br>
     *                   <b>1 = Seleciona as favoritas;</b><br>
     *                   <b>2 = Seleciona as cotidianas;</b><br>
     *                   <b>3 = Seleciona as escolares;</b><br>
     *                   <b>4 = Seleciona as esportivas;</b><br>
     *                   <b>5 = Seleciona as do lazer;</b><br>
     *                   <b>6 = Seleciona as sedentárias;</b><br>
     * @return
     * @throws SQLiteException
     */
    public ArrayList<Atividade> getListaAtividades(byte filterType) throws SQLiteException{

        //Criando objetos
        ArrayList<Atividade> listaAtividades = new ArrayList<>();
        Cursor cursor;

        //Equivalente à um --- select * from ---
        switch (filterType){
            case -1:
                //Select nas não favoritadas
                cursor = objBanco.query("TB_ATIVIDADE", null, "MODALIDADE_FAVORITA = 0", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 0:
                //Select em todas
                cursor = objBanco.query("TB_ATIVIDADE", null, null, null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 1:
                //Select nas favoritadas
                cursor = objBanco.query("TB_ATIVIDADE", null, "MODALIDADE_FAVORITA = 1", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 2:
                //Select nas cotidiandas
                cursor = objBanco.query("TB_ATIVIDADE", null, "CATEGORIA = 0", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 3:
                //Select nas escolares
                cursor = objBanco.query("TB_ATIVIDADE", null, "CATEGORIA = 1", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 4:
                //Select nas esportivas
                cursor = objBanco.query("TB_ATIVIDADE", null, "CATEGORIA = 2", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 5:
                //Select nas do lazer
                cursor = objBanco.query("TB_ATIVIDADE", null, "CATEGORIA = 3", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            case 6:
                //Select nas sedentárias
                cursor = objBanco.query("TB_ATIVIDADE", null, "CATEGORIA = 4", null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
            default:
                //Select em todas
                cursor = objBanco.query("TB_ATIVIDADE", null, null, null, null, null, "DESCRICAO, CATEGORIA ASC", null);
                break;
        }

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            do{

                listaAtividades.add(new Atividade(
                        cursor.getInt(cursor.getColumnIndex("CODIGO")),
                        cursor.getInt(cursor.getColumnIndex("RESOURCE_ID")),
                        cursor.getShort(cursor.getColumnIndex("CATEGORIA")),
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        cursor.getFloat(cursor.getColumnIndex("METS_LEVE")),
                        cursor.getFloat(cursor.getColumnIndex("METS_MODERADO")),
                        cursor.getFloat(cursor.getColumnIndex("METS_INTENSO")),
                        cursor.getInt(cursor.getColumnIndex("PASSOS_ATIVO")) == 1,
                        cursor.getInt(cursor.getColumnIndex("DISTANCIA_ATIVA")) == 1,
                        (cursor.getInt(cursor.getColumnIndex("MODALIDADE_FAVORITA"))==1)
                ));

            }while(cursor.moveToNext());

        }

        cursor.close();
        objBanco.close();
        return listaAtividades;
    }

}
