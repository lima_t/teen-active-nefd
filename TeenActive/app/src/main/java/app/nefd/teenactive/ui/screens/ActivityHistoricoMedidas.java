package app.nefd.teenactive.ui.screens;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMedidas;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.dao.DAOReferencia;
import app.nefd.teenactive.database.entities.Medidas;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Referencia;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.GraphMarkerView;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.CientificUtils;
import app.nefd.teenactive.controller.utils.ColorUtils;

public class ActivityHistoricoMedidas extends AppCompatActivity {

    //region Variáveis globais
    private LineChart graficoLinha;
    private RadioButton radFiltroAltura;//O RadioButton de peso não é necessário
    private Spinner spinnerFiltroPeso;
    private TextView labTituloGrafico;
    private GraphMarkerView objMarkerView;
    private StringBuilder dataToCsv;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_medidas);

        try{

            //region Inicialização das variáveis
            TabHost painelTabulado = (TabHost) findViewById(R.id.painelTabuladoHistoricoMedidas);

            graficoLinha = (LineChart) findViewById(R.id.graficoHistoricoPeso);
            radFiltroAltura = (RadioButton) findViewById(R.id.radFiltroAltura);
            spinnerFiltroPeso = (Spinner) findViewById(R.id.spinnerFiltroPeso);
            labTituloGrafico = (TextView) findViewById(R.id.labTituloGraficoMedidas);
            objMarkerView = new GraphMarkerView(ActivityHistoricoMedidas.this);

            dataToCsv = new StringBuilder();
            //endregion

            //region Setando os valores
            loadDadosIndicadores();
            loadDadosGraficoLinha((byte)0);

            painelTabulado.setup();//Setando Painel Tabulado

            //Setando as aba Tempo
            TabHost.TabSpec abaHistorico = painelTabulado.newTabSpec("Histórico");
            abaHistorico.setContent(R.id.tabHistoricoMedidas);
            abaHistorico.setIndicator("Histórico");
            painelTabulado.addTab(abaHistorico);

            //Setando as aba Calorias
            TabHost.TabSpec abaIndicadores = painelTabulado.newTabSpec("Indicadores");
            abaIndicadores.setContent(R.id.tabIndicadores);
            abaIndicadores.setIndicator("Indicadores");
            painelTabulado.addTab(abaIndicadores);

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityHistoricoMedidas.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        //endregion

        //region Evento dos componentes
        spinnerFiltroPeso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {

                        loadDadosGraficoLinha((byte) position);

                    } catch (final Exception erro) {

                        MessageDialog.exibirDialogoErro(ActivityHistoricoMedidas.this, "Erro",
                                "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                                .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //Abre o compositor de e-mails para enviar o relatório do erro
                                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                EmailType.RELATORIO_BUG,
                                                new String[]{"tarcisio.lima.amorim@outlook.com"},
                                                AppUtils.comporEmailErro(erro)),
                                                "Enviar E-mail"));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //Não faz nada
                }
            });

        radFiltroAltura.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {

                    loadDadosGraficoLinha((byte)spinnerFiltroPeso.getSelectedItemPosition());

                } catch (final Exception erro) {

                    MessageDialog.exibirDialogoErro(ActivityHistoricoMedidas.this, "Erro",
                            "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                            EmailType.RELATORIO_BUG,
                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                            AppUtils.comporEmailErro(erro)),
                                            "Enviar E-mail"));
                                    finish();
                                }
                            })
                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
            }
        });
        //endregion

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de acação
        getMenuInflater().inflate(R.menu.menu_historico_medidas, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.itemExportarDadosMedidas:
                MessageDialog.exibirDialogoQuestao(ActivityHistoricoMedidas.this, "Exportar Dados",
                        String.format("Você deseja exportar os dados atuais da consulta para um arquivo \"csv\" para a seguinte pasta?\n\n%s", AppUtils.APP_CSV_DIRECTORY))
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try{

                                    if(dataToCsv.toString().isEmpty()){
                                        Toast.makeText(ActivityHistoricoMedidas.this, "A consulta não tem registros para serem exportados", Toast.LENGTH_SHORT).show();

                                    }else{
                                        AppUtils.exportarDadosCsv("Dados das Medidas", dataToCsv);
                                        Toast.makeText(ActivityHistoricoMedidas.this, "O dados foram exportados com sucesso!", Toast.LENGTH_SHORT).show();
                                    }

                                }catch (final Exception erro){
                                    //region Mensagem para reportar Bug
                                    MessageDialog.exibirDialogoErro(ActivityHistoricoMedidas.this, "Erro",
                                            "Ocorreu um erro ao tentar exportar os dados.\n[Erro]:\n\n" + erro.getMessage())
                                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                            EmailType.RELATORIO_BUG,
                                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                            AppUtils.comporEmailErro(erro)),
                                                            "Enviar E-mail"));
                                                    finish();
                                                }
                                            })
                                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            })
                                            .show();
                                    //endregion
                                }
                            }
                        })
                        .setNegativeButton("Nâo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                break;
            default:
                //Não faz nada aqui
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Carrega os dados de pesos registrados em um gráfico de linha X,Y
     * @param filterType
     * @throws Exception
     */
    private void loadDadosGraficoLinha(byte filterType) throws Exception{

        //Variáveis e Objetos
        final ArrayList<String> listaRotulosAxisX = new ArrayList<>();
        ArrayList<Medidas> listaMedidas;
        final ArrayList<Entry> listaEntradaMedidas = new ArrayList<>();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
        Description objDescription = new Description();

        //Exclui os dados anteriores para evitar que o append fique duplicando os dados
        if(dataToCsv.toString().contains("HISTÓRICO DE MEDIDAS")){
            int indexStarts = dataToCsv.toString().indexOf("HISTÓRICO DE MEDIDAS");
            dataToCsv.delete(indexStarts, dataToCsv.toString().length());

        }

        dataToCsv.append("HISTÓRICO DE MEDIDAS (");

        switch (filterType){
            case 0:
                objDescription.setText("Registros do último mês");//Define uma descrição para o gráfico
                dataToCsv.append("Registros do último mês);\nData do registro;Altura (cm); Peso (kg);\n");//Arquivo Csv
                break;
            case 1:
                objDescription.setText("Registros dos últimos 6 meses");//Define uma descrição para o gráfico
                dataToCsv.append("Registros dos últimos 6 meses);\nData do registro;Altura (cm); Peso (kg);\n");//Arquivo Csv
                break;
            case 2:
                objDescription.setText("Registros do último ano");//Define uma descrição para o gráfico
                dataToCsv.append("Registros do último ano);\nData do registro;Altura (cm); Peso (kg);\n");//Arquivo Csv
                break;
            case 3:
                objDescription.setText("Todos os registros salvos");//Define uma descrição para o gráfico
                dataToCsv.append("Todos os registros salvos);\nData do registro;Altura (cm); Peso (kg);\n");//Arquivo Csv
                break;
        }

        //Preenche a lista de acordo com o filtro
        listaMedidas = new DAOMedidas(ActivityHistoricoMedidas.this).getListaMedidas(filterType);

        if(!listaMedidas.isEmpty()){

            //Preenche os Arrays com os respectivos valores da consulta
            for(int index = 0; index < listaMedidas.size(); index++){

                if(radFiltroAltura.isChecked()){
                    listaEntradaMedidas.add(new Entry(index, listaMedidas.get(index).getAltura()));//X = Codigo, Y = Altura
                }else{
                    listaEntradaMedidas.add(new Entry(index, listaMedidas.get(index).getPeso()));//X = Codigo, Y = Peso
                }

                dataToCsv.append(String.format(AppUtils.pt_BR, "%s;%d;%.3f;\n", objFormatoData.format(listaMedidas.get(index).getDataRegistro()), listaMedidas.get(index).getAltura(), listaMedidas.get(index).getPeso()));
                listaRotulosAxisX.add(objFormatoData.format(listaMedidas.get(index).getDataRegistro()));
            }

            //Definindo propriedades do Dataset
            LineDataSet objLineDataSet = new LineDataSet(
                    listaEntradaMedidas,
                    radFiltroAltura.isChecked()?"Altura (cm)":"Peso (kg)"
            );
            objLineDataSet.setDrawFilled(true);//Preenche com uma cor o espaço abaixo das linhas

            if(radFiltroAltura.isChecked()){

                labTituloGrafico.setText("Histórico da Altura");
                objLineDataSet.setColor(Color.rgb(0, 0, 255));//Seta a cor laranja para a linha
                objLineDataSet.setFillColor(Color.rgb(0, 0, 255));//Seta a cor do espaço abaixo das linhas laranja
                objLineDataSet.setCircleColor(Color.rgb(0, 0, 255));//Seta a cor dos pontos laranja

            }else{

                labTituloGrafico.setText("Histórico do Peso");
                objLineDataSet.setColor(Color.rgb(255, 0, 0));//Seta a cor laranja para a linha
                objLineDataSet.setFillColor(Color.rgb(255, 0, 0));//Seta a cor do espaço abaixo das linhas laranja
                objLineDataSet.setCircleColor(Color.rgb(255, 0, 0));//Seta a cor dos pontos laranja

            }

            LineData objLineData = new LineData(objLineDataSet);//Inicia um linha de valores no gráfico

            //Definindo propriedades do eixo X
            XAxis objXAxis = graficoLinha.getXAxis();
            objXAxis.setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {

                    if(value == axis.mEntries[0]){
                        return listaRotulosAxisX.get(0);//Mostra a data no primeiro eixo X
                    }else if(value == axis.mEntries[(axis.mEntries.length - 1)]){
                        return listaRotulosAxisX.get(listaRotulosAxisX.size()-1);//Mostra a data no último eixo X
                    }else{
                        return "";//Mostra um valor vazio nos demais eixos X
                    }
                }
            });

            //Definindo propriedades do eixo Y Direito
            YAxis objYAxis = graficoLinha.getAxisRight();
            objYAxis.setEnabled(false);

            //Defininado atributos do gráfico
            graficoLinha.setDescription(objDescription);
            graficoLinha.animateY(1000);//Define uma animação de 1 segundo para o eixo Y
            graficoLinha.setHardwareAccelerationEnabled(true);//Habilita a aceleração de hardware
            graficoLinha.setData(objLineData);//seta os dados que serão mostrados no gráfico
            graficoLinha.setMarker(objMarkerView);//Seta o marcador
            graficoLinha.setVisibleXRangeMaximum(4f);//Seta a visibilidade do gráfico
            graficoLinha.invalidate();//Atualiza o gráfico

            graficoLinha.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    //Essa linha está dando problemas quando o filtro é aplicado
                    objMarkerView.labMarkerViewText1.setText("Data:"+listaRotulosAxisX.get((int)h.getX()));

                    if(radFiltroAltura.isChecked()){
                        objMarkerView.labMarkerViewText2.setText("Altura: "+((int)e.getY())+" cm");

                    }else{
                        objMarkerView.labMarkerViewText2.setText("Peso: "+e.getY()+" kg");
                    }
                }

                @Override
                public void onNothingSelected() {
                    //Não faz nada
                }
            });

        }else{
            Toast.makeText(ActivityHistoricoMedidas.this, "Não existe registros de medidas salvos ainda", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Carrega e calcula os componentes que estão na aba indicadores
     * @throws Exception
     */
    @SuppressLint("SetTextI18n")
    private void loadDadosIndicadores()throws Exception{

        //Carrega o perfil
        Perfil objPerfil = new DAOPerfil(ActivityHistoricoMedidas.this).carregarPerfil();

        //region Setando informações do painel do usuário
        CircularImageView imgFotoPerfil = (CircularImageView) findViewById(R.id.imgFotoPerfil);
        TextView labInfoUsuario   = (TextView) findViewById(R.id.labNomeAtividade),
                 labIdadeUsuario  = (TextView) findViewById(R.id.labIdadeUsuario),
                 labSexoUsuario   = (TextView) findViewById(R.id.labSexoUsuario),
                 labAlturaUsuario = (TextView) findViewById(R.id.labAlturaUsuario),
                 labPesoUsuario   = (TextView) findViewById(R.id.labPesoUsuario);

        //Carregando as informações no Painel do usuário
        if(objPerfil.getFotoPerfil() != null && !objPerfil.getFotoPerfil().isEmpty()) {

            if (new File(AppUtils.APP_PROFILE_DIRECTORY + "foto_perfil.jpg").exists()) {
                imgFotoPerfil.setImageBitmap(BitmapFactory.decodeFile(objPerfil.getFotoPerfil()));//Seta a imagem no CircularImageView

            } else {
                imgFotoPerfil.setImageResource(R.mipmap.icone_imagem_perfil);
                Toast.makeText(
                        ActivityHistoricoMedidas.this,
                        "Não foi possível carregar a foto do perfil (arquivo inexistente).",
                        Toast.LENGTH_LONG)
                        .show();

            }
        }

        labInfoUsuario.setText(objPerfil.getNome());
        labIdadeUsuario.setText(CientificUtils.calcularIdadeAnos(objPerfil.getDataNascimento())+" anos");
        labSexoUsuario.setText(objPerfil.getSexo()==0?"Masculino":"Feminino");
        labAlturaUsuario.setText(objPerfil.getAltura()+" cm");
        labPesoUsuario.setText(objPerfil.getPeso()+" kg");
        //endregion

        //region Preenchendo o dataToCsv para a Exportação de dados
        dataToCsv.append("DADOS DO PERFIL DO USUÁRIO;\n");
        dataToCsv.append(String.format(AppUtils.pt_BR, "Nome:;%s;\n", objPerfil.getNome()));
        dataToCsv.append(String.format(AppUtils.pt_BR, "Sexo:;%s;\n", objPerfil.getSexo()==0?"Masculino":"Feminino"));
        dataToCsv.append(String.format(AppUtils.pt_BR, "Idade:;%d;\n", CientificUtils.calcularIdadeAnos(objPerfil.getDataNascimento())));
        dataToCsv.append(String.format(AppUtils.pt_BR, "Altura (cm):;%d;\n", objPerfil.getAltura()));
        dataToCsv.append(String.format(AppUtils.pt_BR, "Peso (kg):;%.3f;\n\n", objPerfil.getPeso()));

        dataToCsv.append("IMC;\nResultado;Classificação;\n");
        dataToCsv.append(String.format(AppUtils.pt_BR, "%.2f;", CientificUtils.calcularIMC(objPerfil.getAltura(), objPerfil.getPeso())));
        //endregion

        //region Setando IMC
        TextView labInfoIMC = (TextView) findViewById(R.id.labInfoIMC);
        labInfoIMC.setText(String.format(AppUtils.pt_BR, "IMC → %.2f", CientificUtils.calcularIMC(objPerfil.getAltura(), objPerfil.getPeso())));

        switch (CientificUtils.classificarIMC(objPerfil, ActivityHistoricoMedidas.this)){
            case 0:
                //IMC Muito Baixo
                TextView labImcMuitoBaixo = (TextView) findViewById(R.id.labImcMuitoBaixo);
                labImcMuitoBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                dataToCsv.append("Muito baixo;\n\n");
                break;
            case 1:
                //IMC Abaixo do ideal
                TextView labImcBaixo = (TextView) findViewById(R.id.labImcBaixo);
                labImcBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                dataToCsv.append("Baixo;\n\n");
                break;
            case 2:
                //IMC Normal
                TextView labImcNormal = (TextView) findViewById(R.id.labImcNormal);
                labImcNormal.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                dataToCsv.append("Normal;\n\n");
                break;
            case 3:
                //IMC Acima do ideal
                TextView labImcSobrepeso = (TextView) findViewById(R.id.labImcSobrepeso);
                labImcSobrepeso.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                dataToCsv.append("Sobrepeso;\n\n");
                break;
            case 4:
                //IMC Muito alto
                TextView labImcObesidade = (TextView) findViewById(R.id.labImcObesidade);
                labImcObesidade.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                dataToCsv.append("Obesidade;\n\n");
                break;
        }
        //endregion

        dataToCsv.append("PESO IDEAL;\n");

        //region Setando Peso Ideal
        TextView labInfoPeso = (TextView) findViewById(R.id.labInfoPesoIdeal);
        labInfoPeso.setText(String.format(AppUtils.pt_BR, "Peso ideal → %.2f kg",
                CientificUtils.calcularPesoIdeal(
                        objPerfil.getAltura(),
                        new DAOReferencia(ActivityHistoricoMedidas.this).getMedianaImc(objPerfil))
        ));

        switch (CientificUtils.classificarPeso(objPerfil, ActivityHistoricoMedidas.this)){
            case -2:
                //Maiores de 10 anos não tem classificação de peso
                TextView labAuxiliar = (TextView) findViewById(R.id.labPesoMuitoBaixo);
                labAuxiliar.setText("Indisponível > 10 anos");
                labAuxiliar = (TextView) findViewById(R.id.labPesoBaixo);
                labAuxiliar.setText("Indisponível > 10 anos");
                labAuxiliar = (TextView) findViewById(R.id.labPesoBaixo);
                labAuxiliar.setText("Indisponível > 10 anos");
                labAuxiliar = (TextView) findViewById(R.id.labPesoNormal);
                labAuxiliar.setText("Indisponível > 10 anos");
                labAuxiliar = (TextView) findViewById(R.id.labPesoSobrepeso);
                labAuxiliar.setText("Indisponível > 10 anos");
                labAuxiliar = (TextView) findViewById(R.id.labPesoObesidade);
                labAuxiliar.setText("Indisponível > 10 anos");
                break;
            case 0:
                //PESO Muito Baixo
                TextView labPesoMuitoBaixo = (TextView) findViewById(R.id.labPesoMuitoBaixo);
                labPesoMuitoBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 1:
                //PESO Abaixo do ideal
                TextView labPesoBaixo = (TextView) findViewById(R.id.labPesoBaixo);
                labPesoBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 2:
                //PESO Normal
                TextView labPesoNormal = (TextView) findViewById(R.id.labPesoNormal);
                labPesoNormal.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 3:
                //PESO Acima do ideal
                TextView labPesoSobrepeso = (TextView) findViewById(R.id.labPesoSobrepeso);
                labPesoSobrepeso.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 4:
                //PESO Muito alto
                TextView labPesoObesidade = (TextView) findViewById(R.id.labPesoObesidade);
                labPesoObesidade.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
        }
        //endregion

        //region Setando Altura Ideal
        TextView labAlturaIdeal = (TextView) findViewById(R.id.labInfoAlturaIdeal);
        labAlturaIdeal.setText(String.format(AppUtils.pt_BR, "Altura ideal → %d cm",
                (short)new DAOReferencia(ActivityHistoricoMedidas.this).getAlturaIdeal(objPerfil))
        );

        switch (CientificUtils.classificarAltura(objPerfil, ActivityHistoricoMedidas.this)){
            case -2:
                //Calcular peso ideal em adultos aqui                ;
                TextView labAlturaIndisponivel = (TextView) findViewById(R.id.labInfoAlturaIdeal);
                labAlturaIndisponivel.setText("Indisponível para adultos");
                labAlturaIndisponivel = (TextView) findViewById(R.id.labAlturaMuitoBaixo);
                labAlturaIndisponivel.setText("Indisponível");
                labAlturaIndisponivel = (TextView) findViewById(R.id.labAlturaBaixo);
                labAlturaIndisponivel.setText("Indisponível");
                labAlturaIndisponivel = (TextView) findViewById(R.id.labAlturaNormal);
                labAlturaIndisponivel.setText("Indisponível");
                labAlturaIndisponivel = (TextView) findViewById(R.id.labAlturaAlto);
                labAlturaIndisponivel.setText("Indisponível");
                labAlturaIndisponivel = (TextView) findViewById(R.id.labAlturaMuitoAlto);
                labAlturaIndisponivel.setText("Indisponível");
                break;
            case 0:
                //ALTURA Muito Baixo
                TextView labAlturaMuitoBaixo = (TextView) findViewById(R.id.labAlturaMuitoBaixo);
                labAlturaMuitoBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 1:
                //ALTURA Abaixo do ideal
                TextView labAlturaBaixo = (TextView) findViewById(R.id.labAlturaBaixo);
                labAlturaBaixo.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 2:
                //ALTURA Normal
                TextView labAlturaNormal = (TextView) findViewById(R.id.labAlturaNormal);
                labAlturaNormal.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 3:
                //ALTURA Acima do ideal
                TextView labAlturaAlto = (TextView) findViewById(R.id.labAlturaAlto);
                labAlturaAlto.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
            case 4:
                //ALTURA Muito alto
                TextView labAlturaMuitoAlto = (TextView) findViewById(R.id.labAlturaMuitoAlto);
                labAlturaMuitoAlto.setBackgroundColor(ColorUtils.LARANJA_SELECAO);
                break;
        }
        //endregion
    }
}
