package app.nefd.teenactive.ui.screens;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.GraphMarkerView;
import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.services.ServicoMonitoramento;
import app.nefd.teenactive.controller.services.ServicoNotificacao;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.ColorUtils;

/**
 * Tela principal do aplicativo e ponto de entrada
 */
public class ActivityPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private PieChart graficoResumoDia;
    private TextView labNomeUsuario, labValorTotal;
    private CircularImageView imgFotoPerfilDrawer;
    private Spinner spinnerFiltroResumoDia;
    private GraphMarkerView objMarkerView;

    private Perfil objPerfil;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Recarregando componentes da tela principal
        try {
            //Carregando perfil
            objPerfil = new DAOPerfil(ActivityPrincipal.this).carregarPerfil();
            labNomeUsuario.setText(objPerfil.getNome());
            if(objPerfil.getFotoPerfil() != null && !objPerfil.getFotoPerfil().isEmpty()){

                if(new File(AppUtils.APP_PROFILE_DIRECTORY+"foto_perfil.jpg").exists()){
                    imgFotoPerfilDrawer.setImageBitmap(BitmapFactory.decodeFile(objPerfil.getFotoPerfil()));//Seta a imagem no CircularImageView
                }else{
                    imgFotoPerfilDrawer.setImageResource(R.mipmap.icone_imagem_perfil);
                    Toast.makeText(ActivityPrincipal.this, "Não foi possível carregar a foto do perfil (arquivo inexistente).", Toast.LENGTH_LONG).show();
                }
            }

            loadDataResumoDia();

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityPrincipal.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    protected void onDestroy() {

        executarServicoNotificacao(true);//Só executa se o tempo > 0
        //executarServicoMonitoramento(true);//So executa se treino autonomo == true

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        Intent objIntent;

        vibrator.vibrate(AppUtils.CLICK_VIBRATION);
        //Seleção do menu do Drawer
        switch (menuItem.getItemId()){
            case R.id.nav_nova_atividade:
                objIntent = new Intent(ActivityPrincipal.this, ActivityNovaAtividade.class);
                startActivity(objIntent);
                break;
            case R.id.nav_gerenciar_atividades:
                objIntent = new Intent(ActivityPrincipal.this, ActivityAtividade.class);
                startActivity(objIntent);
                break;
            case R.id.nav_gerenciar_metas:
                objIntent = new Intent(ActivityPrincipal.this, ActivityMeta.class);
                startActivity(objIntent);
                break;
            case R.id.nav_registrar_medidas:
                objIntent = new Intent(ActivityPrincipal.this, ActivityMedidas.class);
                startActivity(objIntent);
                break;
            case R.id.nav_historico_atividade:
                objIntent = new Intent(ActivityPrincipal.this, ActivityHistoricoTreino.class);
                startActivity(objIntent);
                break;
            case R.id.nav_historico_medidas:
                objIntent = new Intent(ActivityPrincipal.this, ActivityHistoricoMedidas.class);
                startActivity(objIntent);
                break;
            case R.id.nav_historico_meta:
                objIntent = new Intent(ActivityPrincipal.this, ActivityRanking.class);
                startActivity(objIntent);
                break;
            case R.id.nav_configuracao:
                objIntent = new Intent(ActivityPrincipal.this, ActivityConfiguracao.class);
                startActivity(objIntent);
                break;
            case R.id.nav_sobre:
                objIntent = new Intent(ActivityPrincipal.this, ActivitySobre.class);
                startActivity(objIntent);
                break;
            case R.id.nav_sair:
                MessageDialog.exibirDialogoQuestao(ActivityPrincipal.this, "Info", "Você realmente deseja sair o aplicativo?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("Não", null).show();
                break;
            default:
                //Nada a fazer
                break;
        }

        //Cria a instância do Drawer no layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Carrega os dados do histórico diário das atividades
     * @throws Exception
     */
    private void loadDataResumoDia()throws Exception{

        //Variáveis e Objetos
        ArrayList<Treino> listaTreinosDia;
        ArrayList<PieEntry> listaEntryAtividadesDia = new ArrayList<>();
        float valorTotal = 0f;
        Legend legendaGrafico = new Legend();
        Description objDescription = new Description();

        //region Switch da Descrição do Gráfico
        switch (spinnerFiltroResumoDia.getSelectedItemPosition()){
            case 0:
                //Tempo
                objDescription.setText("Tempo Hoje");//Define uma descrição para o gráfico
                break;
            case 1:
                //Kcal
                objDescription.setText("Kcal Hoje");//Define uma descrição para o gráfico
                break;
            case 2:
                //Passos
                objDescription.setText("Quantidade de Passos Hoje");//Define uma descrição para o gráfico
                break;
            case 3:
                //Distância
                objDescription.setText("Distância Hoje");//Define uma descrição para o gráfico
                break;
        }
        //endregion

        listaTreinosDia = new DAOTreino(ActivityPrincipal.this).getAtividadesHoje((byte)spinnerFiltroResumoDia.getSelectedItemPosition());

        //region For e Switch para preencher o gráfico
        if(!listaTreinosDia.isEmpty()){

            //region Preenche os Arrays com os respectivos valores da consulta
            for(int index = 0; index < listaTreinosDia.size(); index++) {

                switch (spinnerFiltroResumoDia.getSelectedItemPosition()){
                    case 0:
                        //Tempo
                        listaEntryAtividadesDia.add(new PieEntry(AppUtils.converterMillisToFloat(listaTreinosDia.get(index).getTempoTotal()), listaTreinosDia.get(index).getAtividadeDescricao()));
                        valorTotal += AppUtils.converterMillisToMinutos(listaTreinosDia.get(index).getTempoTotal());
                        break;
                    case 1:
                        //Kcal
                        listaEntryAtividadesDia.add(new PieEntry(listaTreinosDia.get(index).getKcalConsumida(), listaTreinosDia.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosDia.get(index).getKcalConsumida();
                        break;
                    case 2:
                        //Passos
                        listaEntryAtividadesDia.add(new PieEntry(listaTreinosDia.get(index).getQuantidadePassos(), listaTreinosDia.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosDia.get(index).getQuantidadePassos();
                        break;
                    case 3:
                        //Distância
                        listaEntryAtividadesDia.add(new PieEntry(listaTreinosDia.get(index).getDistanciaPercorrida(), listaTreinosDia.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosDia.get(index).getDistanciaPercorrida();
                        break;
                }

                //Adicionar rótulos aqui se necessário
            }
            //endregion

            PieDataSet pieDataSet = new PieDataSet(listaEntryAtividadesDia, null);
            pieDataSet.setColors(ColorUtils.CORES_SINTILANTES);
            PieData objPieData = new PieData(pieDataSet);

            legendaGrafico = graficoResumoDia.getLegend();
            legendaGrafico.setWordWrapEnabled(true);
            legendaGrafico.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

            //Defininado atributos do gráfico
            graficoResumoDia.setDescription(objDescription);
            graficoResumoDia.setUsePercentValues(true);
            graficoResumoDia.setDrawEntryLabels(false);
            graficoResumoDia.animateX(1000);
            graficoResumoDia.setHardwareAccelerationEnabled(true);//Habilita a aceleração de hardware
            graficoResumoDia.setData(objPieData);//seta os dados que serão mostrados no gráfico
            graficoResumoDia.setMarker(objMarkerView);//Seta o marcador
            graficoResumoDia.invalidate();//Atualiza o gráfico

            graficoResumoDia.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    switch (spinnerFiltroResumoDia.getSelectedItemPosition()){
                        case 0:
                            //Tempo
                            objMarkerView.labMarkerViewText1.setText(graficoResumoDia.getLegend().getEntries()[(int)h.getX()].label);
                            objMarkerView.labMarkerViewText2.setText((int)e.getY() < 1?"Menos de 1 min":String.format(AppUtils.pt_BR, "Aproximadamente %d min", (int)e.getY()));
                            break;
                        case 1:
                            //Kcal
                            objMarkerView.labMarkerViewText1.setText(graficoResumoDia.getLegend().getEntries()[(int)h.getX()].label);
                            objMarkerView.labMarkerViewText2.setText(String.format(AppUtils.pt_BR, "Aproximadamente %.2f kcal", e.getY()));
                            break;
                        case 2:
                            //Passos
                            objMarkerView.labMarkerViewText1.setText(graficoResumoDia.getLegend().getEntries()[(int)h.getX()].label);
                            objMarkerView.labMarkerViewText2.setText(String.format(AppUtils.pt_BR, "Aproximadamente %d pas", (int)e.getY()));
                            break;
                        case 3:
                            //Distância
                            objMarkerView.labMarkerViewText1.setText(graficoResumoDia.getLegend().getEntries()[(int)h.getX()].label);
                            objMarkerView.labMarkerViewText2.setText((
                                    e.getY() >= 1_000?
                                            "Distância percorrida hoje: "+(e.getY()/1_000f)+" km":
                                            "Distância percorrida hoje: "+(int)(e.getY())+" m"
                            ));
                            break;
                    }

                }

                @Override
                public void onNothingSelected() {
                    //Não faz nada
                }
            });

            //region Definindo o rótulo total
            switch (spinnerFiltroResumoDia.getSelectedItemPosition()){
                case 0:
                    //Tempo
                    labValorTotal.setText(valorTotal<1?"Tempo total hoje: <1 min":String.format(AppUtils.pt_BR, "Tempo total hoje: %d min", (int)valorTotal));
                    break;
                case 1:
                    //Kcal
                    labValorTotal.setText(String.format(AppUtils.pt_BR, "Calorias gastas hoje: %.2f kcal", valorTotal));
                    break;
                case 2:
                    //Passos
                    labValorTotal.setText(String.format(AppUtils.pt_BR, "Passos efetuados hoje: %d pas", (int)valorTotal));
                    break;
                case 3:
                    //Distância
                    labValorTotal.setText((
                            valorTotal >= 1_000?
                                    String.format(AppUtils.pt_BR, "Distância percorrida hoje: %.2f km", (valorTotal/1_000f)):
                                    String.format(AppUtils.pt_BR, "Distância percorrida hoje: %d m", (int)valorTotal)
                            ));
                    break;
            }
            //endregion

        }else{
            Toast.makeText(ActivityPrincipal.this, "Ainda não existe registros de atividades hoje", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * INICIA ou PARA a execução do serviço de notificação do usuário.
     * @param enable Se o parametro é definido como <b>TRUE</b> então é feita uma verificação nas configurações do aplicativo
     *               afim de confirmar se as notificações estão habilitadas e possuem tempo de intervalo, caso contrário
     *               nada irá acontecer.
     *               <br/><br/>
     *               Se o parametro é definido como <b>FALSE</b> então será feita uma solicitação para que  um o serviço de
     *               notificação previamente em execução seja finalizado.
     */
    private void executarServicoNotificacao(boolean enable){
        Intent intentServicoNotificacao = new Intent(getApplicationContext(), ServicoNotificacao.class);

        if(enable){

            Configuracao config = new Configuracao(ActivityPrincipal.this);

            if(config.getTempoNotificacao() > 0){
                startService(intentServicoNotificacao);
            }

        }else{
            stopService(intentServicoNotificacao);
        }

    }

    /**
     * INICIA ou PARA a execução do serviço de monitoramento de atividades em segundo plano.
     * @param enable Se o parametro é definido como <b>TRUE</b> então é feita uma verificação nas configurações do aplicativo
     *               afim de confirmar se há permissão para a execução do serviço do modo autonomo, caso contrário
     *               nada irá acontecer.
     *               <br/><br/>
     *               Se o parametro é definido como <b>FALSE</b> então será feita uma solicitação para que  um o serviço de
     *               monitoramento previamente em execução seja finalizado.
     */
    private void executarServicoMonitoramento(boolean enable){
        Intent intentServicoMonitoramento = new Intent(getApplicationContext(), ServicoMonitoramento.class);

        if(enable){

            Configuracao config = new Configuracao(ActivityPrincipal.this);

            if(config.isModoAutonomo()){
                startService(intentServicoMonitoramento);
            }

        }else{
            stopService(intentServicoMonitoramento);
        }

    }

    /**
     * Inicia os componentes da Activity
     */
    private void initComponents(){

        Toolbar barraAcaoPrincipal = (Toolbar) findViewById(R.id.barraAcaoPrincipal);

        //region Vinculando os componentes ao XML
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        labValorTotal = (TextView) findViewById(R.id.labValorTotal);
        graficoResumoDia = (PieChart) findViewById(R.id.graficoResumoDia);
        spinnerFiltroResumoDia = (Spinner) findViewById(R.id.spinnerFiltroResumoDia);
        objMarkerView = new GraphMarkerView(ActivityPrincipal.this);

        View navigationViewHeader = navigationView.getHeaderView(0);
        labNomeUsuario = (TextView) navigationViewHeader.findViewById(R.id.labNomeAtividade);
        imgFotoPerfilDrawer = (CircularImageView) navigationViewHeader.findViewById(R.id.imgFotoPerfilDrawer);
        //endregion

        //region Inicializando os componentes da tela principal
        try {

            setSupportActionBar(barraAcaoPrincipal);
            objPerfil = new DAOPerfil(ActivityPrincipal.this).carregarPerfil();

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, barraAcaoPrincipal, R.string.open_drawer_navigation,  R.string.close_drawer_navigation);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            //Carregando perfil
            labNomeUsuario.setText(objPerfil.getNome());
            if(objPerfil.getFotoPerfil() != null && !objPerfil.getFotoPerfil().isEmpty()){

                if(new File(AppUtils.APP_PROFILE_DIRECTORY+"foto_perfil.jpg").exists()){
                    imgFotoPerfilDrawer.setImageBitmap(BitmapFactory.decodeFile(objPerfil.getFotoPerfil()));//Seta a imagem no CircularImageView
                }else{
                    imgFotoPerfilDrawer.setImageResource(R.mipmap.icone_imagem_perfil);
                    Toast.makeText(ActivityPrincipal.this, "Não foi possível carregar a foto do perfil (arquivo inexistente).", Toast.LENGTH_LONG).show();
                }

            }

            navigationView.setNavigationItemSelectedListener(this);//Painel de navegação
            executarServicoNotificacao(false);//Para o serviço de notificações
            //executarServicoMonitoramento(false);//Para o serviço de monitoramento
            loadDataResumoDia();//Carrega o gráfico
            //endregion

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityPrincipal.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        //endregion

        //region Definindo os eventos dos componentes
        imgFotoPerfilDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Alterar para tela original, por enquanto e usado como teste
                Intent objIntent = new Intent(ActivityPrincipal.this, ActivityPerfil.class);
                startActivity(objIntent);
            }
        });

        spinnerFiltroResumoDia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{

                    loadDataResumoDia();

                }catch (final Exception erro){
                    MessageDialog.exibirDialogoErro(ActivityPrincipal.this, "Erro",
                            "Ocorreu um erro ao tentar carregar os dados do gráfico.\n[Erro]:\n\n"+erro.getMessage())
                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                            EmailType.RELATORIO_BUG,
                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                            AppUtils.comporEmailErro(erro)),
                                            "Enviar E-mail"));
                                    finish();
                                }
                            })
                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion
    }
}