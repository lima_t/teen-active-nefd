package app.nefd.teenactive.database.vdo;

import android.content.Context;

import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Treino;

/**
 * Classe responsável por armazenar os atributos, construtores e metodos responsáveis por manipular as
 * pré-operações de validação do registro de treinos no banco de dados
 */
public class VDOTreino {

    //region Variáveis globais
    private Context telaContexto;
    //endregion

    //region Construtores da classe
    public VDOTreino(Context telaContexto) {
        this.telaContexto = telaContexto;
    }

    /**
     * Valida as informações do treino registrado para inserir no banco de dados
     * @param treino
     * @throws Exception
     */
    public void inserirTreino(Treino treino) throws Exception{

        DAOTreino objDAOTreino = new DAOTreino(telaContexto);

        if(treino.getAtividadeFk() <= 0){
            throw new Exception("O código da atividade é nulo ou igual à 0");
        }

        if(treino.getDataTreino() == null){
            throw new Exception("A data do treino é nula");
        }

        if(treino.getTempoTotal() <= 0){
            throw new Exception("O tempo total da atividade é igual à 0");
        }

        objDAOTreino.inserirTreino(treino);
    }
}
