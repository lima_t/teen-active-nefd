package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import app.nefd.teenactive.R;

/**
 * Created by tarcisio on 30/01/17.
 */

public class DecimalPickerDialog extends AlertDialog {

    //region Variáveis Globais
    /**
     * Objetos NumberPicker global para recuperar as informações em outros contextos
     */
    public NumberPicker seletorNumericoMajor, seletorNumericoMinor;
    //endregion

    //region Construtores


    /**
     * Construtor para o Dialogo de Seletor Númerico Decimal, implementar o valor inicial do seletor númerico
     * @param context Tela de referência
     * @param titulo Título do Dialog
     * @param valorMax Valor máximo do seletor maior e menor
     */
    public DecimalPickerDialog(Context context, String titulo, int valorMax){
        super(context);

        LayoutInflater objLayoutInflater = LayoutInflater.from(context);
        View objTela = objLayoutInflater.inflate(R.layout.decimalpicker_dialog, null);

        ((TextView)objTela.findViewById(R.id.labTituloDecimalPickerDialog)).setText(titulo);
        seletorNumericoMajor = (NumberPicker) objTela.findViewById(R.id.seletorNumericoMajor);
        seletorNumericoMinor = (NumberPicker) objTela.findViewById(R.id.seletorNumericoMinor);

        seletorNumericoMajor.setMinValue(1);
        seletorNumericoMajor.setMaxValue(valorMax);

        seletorNumericoMinor.setMinValue(0);
        seletorNumericoMinor.setMaxValue(9);

        this.setCancelable(false);
        this.setView(objTela);

    }
}
