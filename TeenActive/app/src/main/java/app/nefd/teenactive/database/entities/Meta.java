package app.nefd.teenactive.database.entities;

/**
 * Classe que representa a entidade META do banco de dados
 */
public class Meta {
    //region Atributos
    private int codigo = 0;
    private String descricao = null;
    private short tipo = 0;
    private int valorSimbolico = 0;
    private boolean metaPadrao = false;
    private boolean metaAtingida = false;
    //endregion

    //region Construtores

    public Meta() {}

    public Meta(int codigo, String descricao, short tipo, int valorSimbolico, boolean metaPadrao, boolean metaAtingida) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tipo = tipo;
        this.valorSimbolico = valorSimbolico;
        this.metaPadrao = metaPadrao;
        this.metaAtingida = metaAtingida;
    }

    //endregion

    //region Getters e Setters
    /**
     * Pega o código da DAOMeta
     * @return código da meta (int)
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Define o código da DAOMeta
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Pega o tipo simbólico da DAOMeta
     * @return tipo da DAOMeta (Passos, Distância, Tempo de DAOAtividade, Etc)
     */
    public short getTipo() {
        return tipo;
    }

    /**
     * Define o tipo da DAOMeta (Passos, Distância, Tempo de DAOAtividade, Etc)<br>
     * <b>0 = Tempo em min</b>;
     * <b>1 = Calorias</b>;
     * <b>2 = Passos</b>;
     * <b>3 = Distância em metros</b>;
     * @param tipo
     */
    public void setTipo(short tipo) {
        this.tipo = tipo;
    }

    /**
     * Pega o valor simbólico do tipo da DAOMeta
     * @return valor númerico contável da DAOMeta
     */
    public int getValorSimbolico() {
        return valorSimbolico;
    }

    /**
     * Define o valor simbólico da DAOMeta
     * @param valorSimbolico
     */
    public void setValorSimbolico(int valorSimbolico) {
        this.valorSimbolico = valorSimbolico;
    }

    /**
     * Pega o valor TRUE ou FALSE se a DAOMeta é o objetivo padrão atual
     * @return TRUE ou FALSE
     */
    public boolean isMetaPadrao() {
        return metaPadrao;
    }

    /**
     * Define se a meta corrente é a meta padrão
     * @param metaPadrao
     */
    public void setMetaPadrao(boolean metaPadrao) {
        this.metaPadrao = metaPadrao;
    }

    /**
     * Pega o valor da descrição para DAOMeta
     * @return
     */
    public String getDescricao() { return descricao; }

    /**
     * Define a descrição da DALmeta
     * @param descricao
     */
    public void setDescricao(String descricao) { this.descricao = descricao; }

    /**
     * Pega o valor TRUE ou FALSE se a DAOMeta para o objeto tiver sido aitingida ou não
     * @return
     */
    public boolean isMetaAtingida() { return metaAtingida; }

    /**
     * Define o valor TRUE ou FALSE em DAOMeta se a meta já foi atingida
     * @param metaAtingida
     */
    public void setMetaAtingida(boolean metaAtingida) { this.metaAtingida = metaAtingida; }
    //endregion
}
