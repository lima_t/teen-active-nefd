package app.nefd.teenactive.controller.utils;

import android.os.Handler;
import android.util.Log;

import app.nefd.teenactive.controller.interfaces.OnTimeCounterPaused;
import app.nefd.teenactive.controller.interfaces.OnTimeCounterResumed;
import app.nefd.teenactive.controller.interfaces.OnTimeCounterStarted;
import app.nefd.teenactive.controller.interfaces.OnTimeCounterTickListener;

/**
 * Classe responsável por criar um cronômetro simples que pode rodar em background.
 *
 * Eu posso colocar widgets nessa classe criando seus objetos e alimentado-os a partir de um construtor
 * para ter acesso à thread principal e atualizar a interface a partir de um serviço se for conveniente.
 */
public class TimeCounter {

    private String timeText = "00:00";

    private long mStartTime = 0;

    private Handler mTimerHandler;
    private OnTimeCounterTickListener onTimeCounterTickListener;
    private OnTimeCounterStarted onTimeCounterStarted;
    private OnTimeCounterPaused onTimeCounterPaused;
    private OnTimeCounterResumed onTimeCounterResumed;

    //region Getters e Setters
    public String getTimeText() {
        return timeText;
    }

    /**
     * Seta um evento que é disparado toda vez que o cronometro é alterado no ritmo do tic-tac
     * @param listener
     */
    public void setOnTimeCounterTickListener(OnTimeCounterTickListener listener){
        onTimeCounterTickListener = listener;
    }

    /**
     * Seta um evento que é disparado toda vez que o metodo <b>start()</b> é invocado.
     * @param listener
     */
    public void setOnTimeCounterStarted(OnTimeCounterStarted listener){
        onTimeCounterStarted = listener;
    }

    /**
     * Seta um evento que é disparado toda vez que o metodo <b>pause()</b> é invocado.
     * @param listener
     */
    public void setOnTimeCounterPaused(OnTimeCounterPaused listener){
        onTimeCounterPaused = listener;
    }

    /**
     * Seta um evento que é disparado toda vez que o metodo <b>resumed()</b> é invodaco
     * @param listener
     */
    public void setOnTimeCounterResumed(OnTimeCounterResumed listener){
        onTimeCounterResumed = listener;
    }
    //endregion

    /**
     * Construtor padrão do TimeCounter
     */
    public TimeCounter() {
        mStartTime = System.currentTimeMillis();
        mTimerHandler = new Handler();
    }

    /**
     * Efetua a rotina do tic-tac do relógio
     */
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {

            //Efetua os calculos
            long milliseconds = System.currentTimeMillis() - mStartTime;
            short seconds = (short) (milliseconds / 1000);
            int minutes = seconds / 60;
            seconds = (short) (seconds % 60);
            timeText = String.format(AppUtils.pt_BR, "%d:%02d", minutes, seconds);

            //Dispara o listener TickListener
            if(onTimeCounterTickListener != null){
                onTimeCounterTickListener.onTimeCounterTickListener(milliseconds, minutes, seconds);
            }

            mTimerHandler.postDelayed(this, 1000);
        }
    };

    /**
     * Inicia o TimeCounter
     */
    public void start() {
        //Dispara o listener StartListener
        if(onTimeCounterStarted != null){
            onTimeCounterStarted.onTimeCounterStarted();
        }

        Log.i("Teen-Active Service", "TimerCounter.start() foi chamado.");
        mTimerHandler.postDelayed(mRunnable, 1000);
    }

    /**
     * Pause o TimeCounter
     */
    public void pause() {
        //Dispara o listener PausedListener
        if(onTimeCounterPaused != null){
            onTimeCounterPaused.onTimeCounterPaused();
        }

        Log.i("Teen-Active Service", "TimerCounter.pause() foi chamado.");
        mTimerHandler.removeCallbacks(mRunnable);
    }

    /**
     * Continua a contagem do TimeCounter previamente pausado.
     */
    public void resume() {
        //Dispara o listener ResumedListener
        if(onTimeCounterResumed != null){
            onTimeCounterResumed.onTimeCounterResumed();
        }

        Log.i("Teen-Active Service", "TimerCounter.resume() foi chamado.");
        mTimerHandler.postDelayed(mRunnable,1000);
    }
}
