package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMeta;
import app.nefd.teenactive.database.entities.Meta;
import app.nefd.teenactive.database.vdo.VDOMeta;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.dialogs.NumberPickerDialog;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityCrudMeta extends AppCompatActivity {

    //region Declarando variáveis globais
    private EditText txtNomeMeta, txtValorSimbolicoMeta;
    private Spinner spinnerTipoMeta;
    private TextView labValorSimbolicoMeta;
    private CheckBox checkIsMetaFavorita;

    private Meta objMeta;
    private VDOMeta objVDOMeta;

    private int valorMax = 0;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud_meta);

        Toolbar barraAcaoCrudMeta = (Toolbar) findViewById(R.id.barraAcaoCrudMeta);

        //region Vinculando os componentes ao XML
        objVDOMeta = new VDOMeta(ActivityCrudMeta.this);
        objMeta = new Meta();

        txtNomeMeta = (EditText) findViewById(R.id.txtNomeMeta);
        txtValorSimbolicoMeta = (EditText) findViewById(R.id.txtValorSimbolicoMeta);
        spinnerTipoMeta = (Spinner) findViewById(R.id.spinnerTipoMeta);
        labValorSimbolicoMeta = (TextView) findViewById(R.id.labValorSimbolicoMeta);
        checkIsMetaFavorita = (CheckBox) findViewById(R.id.checkIsMetaFavorita);
        //endregion

        //region Iniciando os componentes
        try{

            setSupportActionBar(barraAcaoCrudMeta);

            //Pega o código da meta passado pela outra Activity para verificar se é uma atualização ou um insert
            //Caso nada seja passado como parametro o padrão é zero = inserção)
            objMeta.setCodigo(getIntent().getIntExtra("codigoMeta", 0));

            //Se o código for maior que zero, indica que a operação será um atualização de registro
            if(objMeta.getCodigo() > 0){

                objMeta = new DAOMeta(ActivityCrudMeta.this).consultarMeta(objMeta.getCodigo());//Preenche um objeto meta

                txtNomeMeta.setText(objMeta.getDescricao());//Seta o nome da meta
                spinnerTipoMeta.setSelection(objMeta.getTipo());//Seta o tipo da meta
                txtValorSimbolicoMeta.setText(String.valueOf(objMeta.getValorSimbolico()));//Seta o valor simbólico
                checkIsMetaFavorita.setChecked(objMeta.isMetaPadrao());//Seta se a meta é padrão

            }else{
                spinnerTipoMeta.setSelection(0);
            }

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityCrudMeta.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{"tarcisio.lima.amorim@outlook.com"},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        //endregion

        //region Evento dos componentes;
        spinnerTipoMeta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        labValorSimbolicoMeta.setText("Minutos por dia:");
                        valorMax = 1_440;//min = 1 min / máx = 24 horas
                        break;
                    case 1:
                        labValorSimbolicoMeta.setText("Calorias por dia:");
                        valorMax = 5_000;//min = 1 kcal / máx = 5000 kcal
                        break;
                    case 2:
                        labValorSimbolicoMeta.setText("Passos por dia:");
                        valorMax = 100_000;//min = 1 passo / máx = 100.000 passos
                        break;
                    case 3:
                        labValorSimbolicoMeta.setText("Metros por dia:");
                        valorMax = 100_000;//
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Não faz nada
            }
        });

        checkIsMetaFavorita.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    checkIsMetaFavorita.setText("Favoritada");
                }else{
                    checkIsMetaFavorita.setText("Não Favoritada");
                }
            }
        });

        txtValorSimbolicoMeta.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    chamarNumberPickerDialog();
                }
            }
        });

        txtValorSimbolicoMeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarNumberPickerDialog();
            }
        });
        //endregion

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de ação
        getMenuInflater().inflate(R.menu.menu_crud_meta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try{

            if(item.getItemId() == R.id.itemSalvarMeta){

                objMeta.setDescricao(txtNomeMeta.getText().toString());
                objMeta.setTipo((short) spinnerTipoMeta.getSelectedItemPosition());
                objMeta.setValorSimbolico(Integer.parseInt(txtValorSimbolicoMeta.getText().toString()));
                objMeta.setMetaPadrao(checkIsMetaFavorita.isChecked());

                if(objMeta.getCodigo() > 0){

                    //Efetua um update
                    objVDOMeta.alterarMeta(objMeta);

                    MessageDialog.exibirDialogoInformacao(ActivityCrudMeta.this, "Sucesso", "Meta alterada com sucesso!")
                            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();

                }else{

                    //Efetua um insert
                    objVDOMeta.inserirMeta(objMeta);

                    MessageDialog.exibirDialogoInformacao(ActivityCrudMeta.this, "Sucesso", "Meta adicionada com sucesso!")
                            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();

                }
            }

        }catch(Exception erro){
            MessageDialog.exibirDialogoErro(ActivityCrudMeta.this, "Erro", erro.getMessage()).setNeutralButton("Ok", null).show();
            txtNomeMeta.requestFocus();
        }

        return super.onOptionsItemSelected(item);
    }

    private void chamarNumberPickerDialog(){
        final NumberPickerDialog objNumberPickerDialog = new NumberPickerDialog(ActivityCrudMeta.this, labValorSimbolicoMeta.getText().toString(), 1, valorMax);
        objNumberPickerDialog.seletorNumerico.setValue(objMeta.getValorSimbolico() > 0?objMeta.getValorSimbolico():1);//Setando valor inicial

        objNumberPickerDialog.setButton(NumberPickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.seletorNumerico.clearFocus();
                txtValorSimbolicoMeta.setText(String.valueOf(objNumberPickerDialog.seletorNumerico.getValue()));
                objNumberPickerDialog.dismiss();
            }
        });
        objNumberPickerDialog.setButton(NumberPickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.dismiss();
            }
        });

        objNumberPickerDialog.show();
    }
}
