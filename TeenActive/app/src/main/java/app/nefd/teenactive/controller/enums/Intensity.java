package app.nefd.teenactive.controller.enums;

/**
 * Lista de enumerações que representam
 */
public enum Intensity {
    SEDENTARIA,
    LEVE,
    MODERADA,
    VIGOROSA
}
