package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOAtividade;
import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.ui.dialogs.DatePickerDialog;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.AdapterPhotoListView;
import app.nefd.teenactive.ui.itens.GraphMarkerView;
import app.nefd.teenactive.ui.itens.PhotoListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.ColorUtils;

public class ActivityHistoricoTreino extends AppCompatActivity {

    private Spinner spinnerFiltroHistoricoTreino, spinnerTipoMeta;
    private EditText txtDataInicial, txtDataFinal;
    private TextView labTituloGenerico, labValorTotal;
    private BarChart graficoHistoricoTreino;
    private GraphMarkerView objMarkerView;
    private ListView listViewHistoricoTreino;
    private Calendar objData;
    private ArrayList<Treino> listaTreinosIntervalo;
    private LinearLayout painelFiltroHistoricoTreino;
    private StringBuilder dataToCsv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_treino);

        try {

            //region Iniciando os componentes
            final SimpleDateFormat objFormataData = new SimpleDateFormat("dd/MM/yyyy");
            Toolbar toolbar = (Toolbar) findViewById(R.id.barraAcaoHistoricoTreino);
            TabHost painelTabulado = (TabHost) findViewById(R.id.painelTabuladoHistoricoTreino);
            Button btFiltrar = (Button) findViewById(R.id.btFiltrar);

            objData = Calendar.getInstance();
            painelFiltroHistoricoTreino = (LinearLayout) findViewById(R.id.painelFiltroHistoricoTreino);
            spinnerFiltroHistoricoTreino = (Spinner) findViewById(R.id.spinnerFiltroHistoricoTreino);
            spinnerTipoMeta = (Spinner) findViewById(R.id.spinnerTipoMeta);
            txtDataInicial = (EditText) findViewById(R.id.txtDataInicial);
            txtDataFinal = (EditText) findViewById(R.id.txtDataFinal);
            labValorTotal = (TextView) findViewById(R.id.labValorTotal);
            labTituloGenerico = (TextView) findViewById(R.id.labTituloGraficoHistoricoTreino);
            graficoHistoricoTreino = (BarChart) findViewById(R.id.graficoHistoricoTreino);
            objMarkerView = new GraphMarkerView(ActivityHistoricoTreino.this);
            listViewHistoricoTreino = (ListView) findViewById(R.id.listViewHistoricoTreino);
            //endregion

            //region Iniciando os valores dos componentes
            txtDataFinal.setText(objFormataData.format(objData.getTime()));
            objData.set(objData.get(Calendar.YEAR), objData.get(Calendar.MONTH), (objData.get(Calendar.DAY_OF_MONTH)-7));
            txtDataInicial.setText(objFormataData.format(objData.getTime()));
            txtDataInicial.setEnabled(false);
            txtDataFinal.setEnabled(false);

            setSupportActionBar(toolbar);
            painelTabulado.setup();

            //Setando as aba do Gráfico
            TabHost.TabSpec abaGrafico = painelTabulado.newTabSpec("Gráfico");
            abaGrafico.setContent(R.id.tabGrafico);
            abaGrafico.setIndicator("Resumo");
            painelTabulado.addTab(abaGrafico);

            //Setando as aba da lista dos treinos
            TabHost.TabSpec abaAtividades = painelTabulado.newTabSpec("Atividades");
            abaAtividades.setContent(R.id.tabAtividades);
            abaAtividades.setIndicator("Atividades");
            painelTabulado.addTab(abaAtividades);

            loadGraficoHistoricoTreino();
            loadListViewHistoricoTreino();
            //endregion

            //region Eventos dos componentes
            txtDataInicial.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chamarDataPickerDialog(txtDataInicial);
                }
            });

            txtDataFinal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chamarDataPickerDialog(txtDataFinal);
                }
            });

            btFiltrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{

                        loadGraficoHistoricoTreino();
                        loadListViewHistoricoTreino();

                    }catch(final Exception erro){
                        //region Mensagem para reportar Bug
                        MessageDialog.exibirDialogoErro(ActivityHistoricoTreino.this, "Erro",
                                "Ocorreu um erro ao tentar filtrar os resultados do gráfico.\n[Erro]:\n\n" + erro.getMessage())
                                .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //Abre o compositor de e-mails para enviar o relatório do erro
                                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                EmailType.RELATORIO_BUG,
                                                new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                AppUtils.comporEmailErro(erro)),
                                                "Enviar E-mail"));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                        //endregion
                    }
                }
            });

            spinnerFiltroHistoricoTreino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position){
                        case 0:
                            //Última semana
                            objData = Calendar.getInstance();
                            txtDataFinal.setText(objFormataData.format(objData.getTime()));
                            objData.set(objData.get(Calendar.YEAR), objData.get(Calendar.MONTH), (objData.get(Calendar.DAY_OF_MONTH)-7));
                            txtDataInicial.setText(objFormataData.format(objData.getTime()));
                            txtDataInicial.setEnabled(false);
                            txtDataFinal.setEnabled(false);
                            break;
                        case 1:
                            //Último mês
                            objData = Calendar.getInstance();
                            txtDataFinal.setText(objFormataData.format(objData.getTime()));
                            objData.set(objData.get(Calendar.YEAR), (objData.get(Calendar.MONTH)-1), objData.get(Calendar.DAY_OF_MONTH));
                            txtDataInicial.setText(objFormataData.format(objData.getTime()));
                            txtDataInicial.setEnabled(false);
                            txtDataFinal.setEnabled(false);
                            break;
                        case 2:
                            //Últimos 6 meses
                            objData = Calendar.getInstance();
                            txtDataFinal.setText(objFormataData.format(objData.getTime()));
                            objData.set(objData.get(Calendar.YEAR), (objData.get(Calendar.MONTH)-6), objData.get(Calendar.DAY_OF_MONTH));
                            txtDataInicial.setText(objFormataData.format(objData.getTime()));
                            txtDataInicial.setEnabled(false);
                            txtDataFinal.setEnabled(false);
                            break;
                        case 3:
                            //Último ano
                            objData = Calendar.getInstance();
                            txtDataFinal.setText(objFormataData.format(objData.getTime()));
                            objData.set((objData.get(Calendar.YEAR)-1), objData.get(Calendar.MONTH), objData.get(Calendar.DAY_OF_MONTH));
                            txtDataInicial.setText(objFormataData.format(objData.getTime()));
                            txtDataInicial.setEnabled(false);
                            txtDataFinal.setEnabled(false);
                            break;
                        case 4:
                            //Todo o histórico
                            txtDataInicial.setEnabled(false);
                            txtDataFinal.setEnabled(false);
                            break;
                        case 5:
                            //Personalizado
                            txtDataInicial.setEnabled(true);
                            txtDataFinal.setEnabled(true);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //Não faz nada
                }
            });

            listViewHistoricoTreino.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try{

                        Atividade atividade = new DAOAtividade(ActivityHistoricoTreino.this).getAtividade(listaTreinosIntervalo.get(position).getAtividadeFk());

                        Intent objIntent = new Intent(ActivityHistoricoTreino.this, ActivityDetalheTreino.class);
                        objIntent.putExtra("treino", listaTreinosIntervalo.get(position));
                        objIntent.putExtra("atividade_icone", atividade.getResourceId());
                        startActivity(objIntent);

                    }catch (final Exception erro){
                        //region Mensagem para reportar Bug
                        MessageDialog.exibirDialogoErro(ActivityHistoricoTreino.this, "Erro",
                                "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n" + erro.getMessage())
                                .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //Abre o compositor de e-mails para enviar o relatório do erro
                                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                EmailType.RELATORIO_BUG,
                                                new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                AppUtils.comporEmailErro(erro)),
                                                "Enviar E-mail"));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                        //endregion
                    }
                }
            });
            //endregion

        } catch (final Exception erro) {
            //region Mensagem para reportar Bug
            MessageDialog.exibirDialogoErro(ActivityHistoricoTreino.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n" + erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            //endregion
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de acação
        getMenuInflater().inflate(R.menu.menu_historico_treino, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        ViewGroup.LayoutParams layoutParams = painelFiltroHistoricoTreino.getLayoutParams();

        switch (item.getItemId()){
            case R.id.itemFiltrarHistoricoTreino:

                if(layoutParams.height == ViewGroup.LayoutParams.WRAP_CONTENT){
                    layoutParams.height = 0;
                }else{
                    layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                }

                painelFiltroHistoricoTreino.setLayoutParams(layoutParams);

                if(item.isChecked()){
                    item.setChecked(false);
                }else{
                    item.setChecked(true);
                }

                break;
            case R.id.itemExportarDadosTreino:
                MessageDialog.exibirDialogoQuestao(ActivityHistoricoTreino.this, "Exportar Dados",
                        String.format("Você deseja exportar os dados atuais da consulta para um arquivo \"csv\" para a seguinte pasta?\n\n%s", AppUtils.APP_CSV_DIRECTORY))
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try{


                                    if(dataToCsv.toString().isEmpty()){
                                        Toast.makeText(ActivityHistoricoTreino.this, "A consulta não tem registros para serem exportados", Toast.LENGTH_SHORT).show();

                                    }else{
                                        AppUtils.exportarDadosCsv("Dados dos Treinos", dataToCsv);
                                        Toast.makeText(ActivityHistoricoTreino.this, "O dados foram exportados com sucesso!", Toast.LENGTH_SHORT).show();
                                    }

                                }catch (final Exception erro){
                                    //region Mensagem para reportar Bug
                                    MessageDialog.exibirDialogoErro(ActivityHistoricoTreino.this, "Erro",
                                            "Ocorreu um erro ao tentar exportar os dados.\n[Erro]:\n\n" + erro.getMessage())
                                            .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    //Abre o compositor de e-mails para enviar o relatório do erro
                                                    startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                            EmailType.RELATORIO_BUG,
                                                            new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                            AppUtils.comporEmailErro(erro)),
                                                            "Enviar E-mail"));
                                                    finish();
                                                }
                                            })
                                            .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            })
                                            .show();
                                    //endregion
                                }
                            }
                        })
                        .setNegativeButton("Nâo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                break;
            default:
                //Não faz nada
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //Metodos personalizados
    /**
     * Exibe um dialogo para seleção de datas para data inicial e final do filtro
     */
    private void chamarDataPickerDialog(final EditText editText) {
        Calendar objCalendario = Calendar.getInstance();//Cria a instância de um calendário
        final SimpleDateFormat objDateFormat = new SimpleDateFormat("dd/MM/yyyy");//SimpleDateFormat para converter para o padrão pt-BR
        final DatePickerDialog objDatePickerDialog = new DatePickerDialog(ActivityHistoricoTreino.this, "Data de Nascimento");
        objDatePickerDialog.seletorData.setMaxDate(objCalendario.getTimeInMillis());

        try {

            //Altera a data do datapicker para a salva no cadastro quando ele abrir caso o atributo dataNascimento não for nulo
            if (!editText.getText().toString().isEmpty()) {

                objCalendario.setTime(objDateFormat.parse(editText.getText().toString()));
                objDatePickerDialog.seletorData.init(objCalendario.get(Calendar.YEAR), objCalendario.get(Calendar.MONTH), objCalendario.get(Calendar.DAY_OF_MONTH), null);

            }

            objDatePickerDialog.setButton(DatePickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    objDatePickerDialog.seletorData.clearFocus();//Limpa o focus para commmitar as alterações
                    editText.setText(objDateFormat.format(objDatePickerDialog.seletorData.getCalendarView().getDate()));
                    objDatePickerDialog.dismiss();
                }
            });
            objDatePickerDialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    objDatePickerDialog.cancel();
                }
            });

            objDatePickerDialog.show();

        } catch (final Exception erro) {
            //region Mesamge para reportar Bug
            MessageDialog.exibirDialogoErro(ActivityHistoricoTreino.this, "Erro",
                    "Ocorreu um erro ao tentar alterar a data no campo de texto.\n[Erro]:\n\n" + erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            //endregion
        }

    }

    /**
     * Carrega os dados do gráfico de acordo com o filtro selecionado
     */
    private void loadGraficoHistoricoTreino() throws ParseException{
        //Variáveis e Objetos
        ArrayList<Treino> listaTreinosGroupBy;
        final ArrayList<BarEntry> listaEntryTreinosIntervalo = new ArrayList<>();
        ArrayList<LegendEntry> listaEntryLegenda = new ArrayList<>();
        float valorTotal = 0f;
        Legend legendaGrafico;
        SimpleDateFormat objFormataData = new SimpleDateFormat("dd/MM/yyyy");
        Description objDescription = new Description();

        //region Switch da Descrição do Gráfico
        switch (spinnerTipoMeta.getSelectedItemPosition()){
            case 0:
                //Tempo
                objDescription.setText("Duração dos Treinos");//Define uma descrição para o gráfico
                break;
            case 1:
                //Kcal
                objDescription.setText("Kcal dos Treinos");//Define uma descrição para o gráfico
                break;
            case 2:
                //Passos
                objDescription.setText("Qtd de Passos dos Treinos");//Define uma descrição para o gráfico
                break;
            case 3:
                //Distância
                objDescription.setText("Km Percorrida dos Treinos");//Define uma descrição para o gráfico
                break;
        }
        //endregion

        listaTreinosGroupBy = new DAOTreino(ActivityHistoricoTreino.this).getAtividadesPorIntervalo(objFormataData.parse(txtDataInicial.getText().toString()), objFormataData.parse(txtDataFinal.getText().toString()), (byte)spinnerTipoMeta.getSelectedItemPosition(), true);

        //region For e Switch para preencher o gráfico
        if(!listaTreinosGroupBy.isEmpty()){

            //region Preenche os Arrays com os respectivos valores da consulta
            for(int index = 0; index < listaTreinosGroupBy.size(); index++) {

                switch (spinnerTipoMeta.getSelectedItemPosition()){
                    case 0:
                        //Tempo
                        listaEntryTreinosIntervalo.add(new BarEntry(index, AppUtils.converterMillisToFloat(listaTreinosGroupBy.get(index).getTempoTotal()), listaTreinosGroupBy.get(index).getAtividadeDescricao()));
                        valorTotal += AppUtils.converterMillisToMinutos(listaTreinosGroupBy.get(index).getTempoTotal());
                        break;
                    case 1:
                        //Kcal
                        listaEntryTreinosIntervalo.add(new BarEntry(index, listaTreinosGroupBy.get(index).getKcalConsumida(), listaTreinosGroupBy.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosGroupBy.get(index).getKcalConsumida();
                        break;
                    case 2:
                        //Passos
                        listaEntryTreinosIntervalo.add(new BarEntry(index, listaTreinosGroupBy.get(index).getQuantidadePassos(), listaTreinosGroupBy.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosGroupBy.get(index).getQuantidadePassos();
                        break;
                    case 3:
                        //Distância
                        listaEntryTreinosIntervalo.add(new BarEntry(index, listaTreinosGroupBy.get(index).getDistanciaPercorrida(), listaTreinosGroupBy.get(index).getAtividadeDescricao()));
                        valorTotal += listaTreinosGroupBy.get(index).getDistanciaPercorrida();
                        break;
                }
            }
            //endregion

            BarDataSet barDataSet = new BarDataSet(listaEntryTreinosIntervalo, null);
            barDataSet.setColors(ColorUtils.CORES_SINTILANTES);
            BarData objBarData = new BarData(barDataSet);

            YAxis objYAxis = graficoHistoricoTreino.getAxisRight();
            objYAxis.setEnabled(false);

            XAxis objXAxis = graficoHistoricoTreino.getXAxis();
            objXAxis.setEnabled(false);

            //Defininado atributos do gráfico
            graficoHistoricoTreino.setDescription(objDescription);
            graficoHistoricoTreino.animateY(1000);
            graficoHistoricoTreino.setHardwareAccelerationEnabled(true);//Habilita a aceleração de hardware
            graficoHistoricoTreino.setData(objBarData);//seta os dados que serão mostrados no gráfico
            graficoHistoricoTreino.setMarker(objMarkerView);//Seta o marcador
            graficoHistoricoTreino.setVisibleXRangeMaximum(5f);//Seta a visibilidade do gráfico para 5 barras
            graficoHistoricoTreino.invalidate();//Atualiza o gráfico

            //region Definindo a legenda
            for(int index = 0; index < listaTreinosGroupBy.size(); index++){

                //Adicionar rótulos aqui se necessário
                listaEntryLegenda.add(new LegendEntry(
                        listaTreinosGroupBy.get(index).getAtividadeDescricao(),
                        Legend.LegendForm.DEFAULT,
                        1f,
                        1f,
                        graficoHistoricoTreino.getLegend().getFormLineDashEffect(),
                        graficoHistoricoTreino.getLegend().getEntries()[index].formColor)
                );
            }

            legendaGrafico = graficoHistoricoTreino.getLegend();
            legendaGrafico.setEntries(listaEntryLegenda);
            legendaGrafico.setWordWrapEnabled(true);
            legendaGrafico.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
            legendaGrafico.setXEntrySpace(5f);
            //endregion

            graficoHistoricoTreino.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    switch (spinnerTipoMeta.getSelectedItemPosition()){
                        case 0:
                            //Tempo
                            objMarkerView.labMarkerViewText1.setText(listaEntryTreinosIntervalo.get((int)e.getX()).getData().toString());
                            objMarkerView.labMarkerViewText2.setText((int)e.getY() < 1?"Menos de 1 min":String.format(AppUtils.pt_BR, "Aproximadamente %d min", (int)e.getY()));
                            break;
                        case 1:
                            //Kcal
                            objMarkerView.labMarkerViewText1.setText(listaEntryTreinosIntervalo.get((int)e.getX()).getData().toString());
                            objMarkerView.labMarkerViewText2.setText(String.format(AppUtils.pt_BR, "Aproximadamente %.2f kcal", e.getY()));
                            break;
                        case 2:
                            //Passos
                            objMarkerView.labMarkerViewText1.setText(listaEntryTreinosIntervalo.get((int)e.getX()).getData().toString());
                            objMarkerView.labMarkerViewText2.setText(String.format(AppUtils.pt_BR, "Aproximadamente %d pas", (int)e.getY()));
                            break;
                        case 3:
                            //Distância
                            objMarkerView.labMarkerViewText1.setText(listaEntryTreinosIntervalo.get((int)e.getX()).getData().toString());
                            objMarkerView.labMarkerViewText2.setText((
                                    e.getY() >= 1_000?
                                            "Distância percorrida hoje: "+(e.getY()/1_000f)+" km":
                                            "Distância percorrida hoje: "+(int)(e.getY())+" m"
                            ));
                            break;
                    }

                }

                @Override
                public void onNothingSelected() {
                    //Não faz nada
                }
            });

            //region Definindo titulo do gráfico
            switch (spinnerFiltroHistoricoTreino.getSelectedItemPosition()){
                case 0:
                    //Última semana
                    labTituloGenerico.setText("Registros da Última Semana");
                    break;
                case 1:
                    //Último mês
                    labTituloGenerico.setText("Registros do Último Mês");
                    break;
                case 2:
                    //Últimos 6 meses
                    labTituloGenerico.setText("Registros dos 6 Meses");
                    break;
                case 3:
                    //Último ano
                    labTituloGenerico.setText("Registros do Último Ano");
                    break;
                case 4:
                    //Todo o histórico
                    labTituloGenerico.setText("Todo o Histórico");
                    break;
                case 5:
                    //Personalizado
                    labTituloGenerico.setText(String.format(AppUtils.pt_BR, "Registros de %s à %s", txtDataInicial.getText().toString(), txtDataFinal.getText().toString()));
                    break;
            }
            //endregion

            //region Definindo o rótulo total
            switch (spinnerTipoMeta.getSelectedItemPosition()){
                case 0:
                    //Tempo
                    labValorTotal.setText(valorTotal<1?"Tempo total: <1 min":String.format(AppUtils.pt_BR, "Tempo total: %d min", (int)valorTotal));
                    break;
                case 1:
                    //Kcal
                    labValorTotal.setText(String.format(AppUtils.pt_BR, "Calorias total: %.2f kcal", valorTotal));
                    break;
                case 2:
                    //Passos
                    labValorTotal.setText(String.format(AppUtils.pt_BR, "Passos total: %d pas", (int)valorTotal));
                    break;
                case 3:
                    //Distância
                    labValorTotal.setText((
                            valorTotal >= 1_000?
                                    String.format(AppUtils.pt_BR, "Distância percorrida hoje: %.2f km", (valorTotal/1_000f)):
                                    String.format(AppUtils.pt_BR, "Distância percorrida hoje: %d m", (int)valorTotal)
                    ));
                    break;
            }
            //endregion

        }else{
            Toast.makeText(ActivityHistoricoTreino.this, "Não existem atividades registradas nesse período", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Carrega os valores
     */
    private void loadListViewHistoricoTreino() throws ParseException {

        SimpleDateFormat objFormataData = new SimpleDateFormat("dd/MM/yyyy");
        listaTreinosIntervalo = new DAOTreino(ActivityHistoricoTreino.this).getAtividadesPorIntervalo(objFormataData.parse(txtDataInicial.getText().toString()), objFormataData.parse(txtDataFinal.getText().toString()), (byte)spinnerTipoMeta.getSelectedItemPosition(), false);
        dataToCsv = new StringBuilder();

        if(!listaTreinosIntervalo.isEmpty()){
            ArrayList<PhotoListItem> listaTreinos = new ArrayList<>();
            objFormataData.applyPattern("dd/MM/yyyy HH:mm:ss");

            //region Cabeçalho do arquivo csv
            dataToCsv.append(String.format(AppUtils.pt_BR,
                    "HISTÓRICO DE ATIVIDADES (%s);\nCódigo;Atividade;Data do Treino;Tempo Total;Tempo Ocioso;Tempo Int. Leve;Tempo Int. Moderada;Tempo Int. Vigorosa;Distância Percorrida;Kcal Consumida;Quantidade de Passos;Atividade Autonoma;Atividade Passiva;\n",
                    labTituloGenerico.getText().toString()));
            //endregion

            for(Treino treino: listaTreinosIntervalo){
                listaTreinos.add(new PhotoListItem(
                        treino.getAtividadeDescricao(),
                        String.format(AppUtils.pt_BR, "Atividade realizada: %s", objFormataData.format(treino.getDataTreino()))
                ));

                dataToCsv.append(String.format(AppUtils.pt_BR, "%d;%s;%s;%d;%d;%d;%d;%d;%.2f;%.2f;%d;%s;%s\n",
                        treino.getCodigo(),
                        treino.getAtividadeDescricao(),
                        objFormataData.format(treino.getDataTreino()),
                        AppUtils.converterMillisToMinutos(treino.getTempoTotal()),
                        AppUtils.converterMillisToMinutos(treino.getTempoOcioso()),
                        AppUtils.converterMillisToMinutos(treino.getTempoIntensidadeLeve()),
                        AppUtils.converterMillisToMinutos(treino.getTempoIntensidadeModerada()),
                        AppUtils.converterMillisToMinutos(treino.getTempoIntensidadeVigorosa()),
                        treino.getDistanciaPercorrida(),
                        treino.getKcalConsumida(),
                        treino.getQuantidadePassos(),
                        treino.isTreinoAutonomo()?"Sim":"Não",
                        treino.isTreinoPassivo()?"Sim":"Não"
                        ));
            }

            listViewHistoricoTreino.setAdapter(new AdapterPhotoListView(ActivityHistoricoTreino.this, listaTreinos));
        }
    }
}
