package app.nefd.teenactive.database.entities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import app.nefd.teenactive.R;

/**
 * Classe que representa a entidade CONFIGURAÇÃO do banco de dados
 */
public class Configuracao {

    //region Atributos
    private boolean isPrimeiroAcesso = false;
    private short tempoNotificacao = 0;
    private float sensibilidade = 0;
    private boolean modoAutonomo = true;
    private boolean treinoPassivo = false;
    //endregion

    public Configuracao(Context context){

        //Define as configurações padrões pela primeira vez (Só é executado uma vez)
        PreferenceManager.setDefaultValues(context, R.xml.fragment_configuracao, false);

        //Carrega as preferências automaticamente do SharedPreferences
        SharedPreferences objPreferencias = PreferenceManager.getDefaultSharedPreferences(context);

        if(!objPreferencias.contains("primeiro_acesso")){
            SharedPreferences.Editor editorPreferencias = objPreferencias.edit();
            editorPreferencias.putBoolean("primeiro_acesso", true);
            editorPreferencias.apply();
        }

        this.isPrimeiroAcesso = objPreferencias.getBoolean("primeiro_acesso", true);
        this.tempoNotificacao = Short.parseShort(objPreferencias.getString("tempo_notificacao", "0"));
        this.sensibilidade = Float.parseFloat(objPreferencias.getString("sensitividade_pedometro", "12.72"));
        this.modoAutonomo = objPreferencias.getBoolean("modo_autonomo", true);
        this.treinoPassivo = objPreferencias.getBoolean("treino_passivo", false);

    }

    public boolean isTreinoPassivo() {
        return treinoPassivo;
    }

    public boolean isPrimeiroAcesso() {
        return isPrimeiroAcesso;
    }

    public short getTempoNotificacao() {
        return tempoNotificacao;
    }

    public boolean isModoAutonomo() {
        return modoAutonomo;
    }
    /**
     * Pega a sensibilidade do acelerometro
     * @return
     */
    public float getSensibilidade() {
        return sensibilidade;
    }
}