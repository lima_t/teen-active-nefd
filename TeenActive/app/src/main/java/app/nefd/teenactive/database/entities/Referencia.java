package app.nefd.teenactive.database.entities;

/**
 * Classe de entidade para as referências de resultados dos indicadores corporais de saúde
 */
public class Referencia {

    //region Variaveis globais
    public static final byte VAR_IMC = 0;
    public static final byte VAR_PESO = 1;
    public static final byte VAR_ALTURA = 2;

    private int codigo = 0;
    private byte variavelReferencia = 0;
    private short idadeMeses, sexo;
    private float percentil3, percentil15, percentil50, percentil85, percentil97;
    //endregion

    //region Construtores
    public Referencia() {}

    public Referencia(int codigo, byte variavelReferencia, byte sexo, short idadeMeses, float percentil3, float percentil15, float percentil50, float percentil85, float percentil97) {
        this.codigo = codigo;
        this.variavelReferencia = variavelReferencia;
        this.sexo = sexo;
        this.idadeMeses = idadeMeses;
        this.percentil3 = percentil3;
        this.percentil15 = percentil15;
        this.percentil50 = percentil50;
        this.percentil85 = percentil85;
        this.percentil97 = percentil97;
    }
    //endregion

    //region Metodos Getters e Setters
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Retorna o valor da variável de referência<br>
     *     <b>0 = IMC;</b><br>
     *         <b>1 = Peso;</b><br>
     *             <b>2 = Altura;</b><br>
     * @return
     */
    public byte getVariavelReferencia() {
        return variavelReferencia;
    }

    /**
     * Define o valor da variável de referência<br>
     *     <b>0 = IMC;</b><br>
     *         <b>1 = Peso;</b><br>
     *             <b>2 = Altura;</b><br>
     */
    public void setVariavelReferencia(byte variavelReferencia) {
        this.variavelReferencia = variavelReferencia;
    }

    /**
     * Retorna o valor do sexo<br>
     *     <b>0 = Masculino;</b><br>
     *     <b>1 = Feminino;</b><br>
     * @return
     */
    public short getSexo() {
        return sexo;
    }

    /**
     * Define o valor do sexo<br>
     *     <b>0 = Masculino;</b><br>
     *     <b>1 = Feminino;</b><br>
     * @return
     */
    public void setSexo(short sexo) {
        this.sexo = sexo;
    }

    public short getIdadeMeses() {
        return idadeMeses;
    }

    public void setIdadeMeses(short idadeMeses) {
        this.idadeMeses = idadeMeses;
    }

    public float getPercentil3() {
        return percentil3;
    }

    public void setPercentil3(float percentil3) {
        this.percentil3 = percentil3;
    }

    public float getPercentil15() {
        return percentil15;
    }

    public void setPercentil15(float percentil15) {
        this.percentil15 = percentil15;
    }

    public float getPercentil50() {
        return percentil50;
    }

    public void setPercentil50(float percentil50) {
        this.percentil50 = percentil50;
    }

    public float getPercentil85() {
        return percentil85;
    }

    public void setPercentil85(float percentil85) {
        this.percentil85 = percentil85;
    }

    public float getPercentil97() {
        return percentil97;
    }

    public void setPercentil97(float percentil97) {
        this.percentil97 = percentil97;
    }
    //endregion
}
