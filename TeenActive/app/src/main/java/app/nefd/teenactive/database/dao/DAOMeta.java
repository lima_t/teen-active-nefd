package app.nefd.teenactive.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Meta;

/**
 * * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto DAOMeta
 */
public class DAOMeta {

    private SQLiteDatabase objBanco = null;

    /**
     * Construtor default da classe, automaticamente ela já retorna uma conexão para a classe para que possa efetuar
     * as operações DML durante o NEW
     * @param telaContexto
     */
    public DAOMeta(Context telaContexto){
        ConexaoBanco objConexaoBanco = new ConexaoBanco(telaContexto);
        objBanco = objConexaoBanco.getWritableDatabase();
    }

    /**
     * Método para inserir um novo registro de meta no banco de dados.
     *
     * @param meta objeto do tipo Meta para ser inserido.
     * @throws SQLiteException
     */
    public void inserirMeta(Meta meta) throws SQLiteException{
        //Criando objetos
        ContentValues objValoresParametros = new ContentValues();

        //Passando valores do perfil
        objValoresParametros.put("DESCRICAO", meta.getDescricao());
        objValoresParametros.put("TIPO", meta.getTipo());
        objValoresParametros.put("VALOR_SIMBOLICO", meta.getValorSimbolico());
        objValoresParametros.put("META_PADRAO", meta.isMetaPadrao());
        objValoresParametros.put("META_ATINGIDA", meta.isMetaAtingida());

        objBanco.insertOrThrow("TB_META", null, objValoresParametros);
        objBanco.close();
    }

    /**
     * Método para alterar um registro de meta existente no banco de dados.
     *
     * @param meta objeto do tipo Meta para ser alterado.
     * @throws SQLiteException
     */
    public void alterarMeta(Meta meta) throws SQLiteException{
        //Criando objetos
        ContentValues objValoresParametros = new ContentValues();

        //Passando valores do perfil
        objValoresParametros.put("DESCRICAO", meta.getDescricao());
        objValoresParametros.put("TIPO", meta.getTipo());
        objValoresParametros.put("VALOR_SIMBOLICO", meta.getValorSimbolico());
        objValoresParametros.put("META_PADRAO", meta.isMetaPadrao());
        objValoresParametros.put("META_ATINGIDA", meta.isMetaAtingida());

        objBanco.update("TB_META", objValoresParametros, "CODIGO = "+meta.getCodigo(), null);
        objBanco.close();
    }

    /**
     * Método para excluir um registro de uma meta existente no banco de dados
     *
     * @param codigo Código de cadastro da meta à ser excluída.
     * @throws SQLiteException
     */
    public void excluirMeta(int codigo) throws SQLiteException{

        objBanco.delete("TB_META", "CODIGO = "+codigo, null);
        objBanco.close();
    }

    /**
     * Retorna um objeto <b>Meta</b> sem o código preenchido de um consulta
     * @param codigo Código da meta a ser consultada
     * @return
     * @throws SQLiteException
     */
    public Meta consultarMeta(int codigo) throws SQLiteException{

        Cursor cursor = objBanco.query("TB_META", null, "CODIGO = "+codigo, null, null, null, null, null);
        Meta objMeta = new Meta();

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            objMeta.setCodigo(codigo);//O código retornado da consulta sempre será o mesmo que foi passado para ela
            objMeta.setDescricao(cursor.getString(cursor.getColumnIndex("DESCRICAO")));
            objMeta.setTipo(cursor.getShort(cursor.getColumnIndex("TIPO")));
            objMeta.setValorSimbolico(cursor.getInt(cursor.getColumnIndex("VALOR_SIMBOLICO")));
            objMeta.setMetaAtingida(cursor.getInt(cursor.getColumnIndex("META_ATINGIDA"))==1);
            objMeta.setMetaPadrao(cursor.getInt(cursor.getColumnIndex("META_PADRAO"))==1);

        }

        cursor.close();
        objBanco.close();

        return objMeta;
    }

    /**
     * Retorna um objeto <b>Meta</b> que foi definido como meta padrão
     * @return
     * @throws SQLiteException
     */
    public Meta consultarMetaFavorita() throws SQLiteException{

        Cursor cursor = objBanco.query("TB_META", null, "META_PADRAO = 1", null, null, null, null, null);
        Meta objMeta = new Meta();

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            objMeta.setCodigo(cursor.getInt(cursor.getColumnIndex("CODIGO")));
            objMeta.setDescricao(cursor.getString(cursor.getColumnIndex("DESCRICAO")));
            objMeta.setTipo(cursor.getShort(cursor.getColumnIndex("TIPO")));
            objMeta.setValorSimbolico(cursor.getInt(cursor.getColumnIndex("VALOR_SIMBOLICO")));
            objMeta.setMetaAtingida(cursor.getInt(cursor.getColumnIndex("META_ATINGIDA"))==1);
            objMeta.setMetaPadrao(cursor.getInt(cursor.getColumnIndex("META_PADRAO"))==1);

        }

        cursor.close();
        objBanco.close();

        return objMeta;
    }

    /**
     * Metodo para definir uma meta como padrão
     * @param codigo Còdigo de cadastrado da meta a ser atualizada como meta padrão
     * @throws SQLiteException
     */
    public void favoritarMeta(int codigo) throws SQLiteException{
        objBanco.execSQL("UPDATE TB_META SET META_PADRAO = 0 WHERE META_PADRAO = 1;");//Seta todas as metas como Favorita = falso
        objBanco.execSQL("UPDATE TB_META SET META_PADRAO = 1 WHERE CODIGO = "+codigo);
        objBanco.close();

    }

    /**
     * Define uma determinada como META_ATINGIDA = TRUE para o código especificado
     * @param codigo ID da meta à ser alterada
     */
    public void setarMetaVencida(int codigo) throws SQLiteException{
        objBanco.execSQL("UPDATE TB_META SET META_ATINGIDA = 1 WHERE CODIGO = "+codigo);
        objBanco.close();
    }

    /**
     * Retorna todos os registros de metas cadastradas no banco de dados em ordem ascendente
     * @return
     * @throws SQLiteException
     */
    public ArrayList<Meta> getListaMetas() throws SQLiteException{

        ArrayList<Meta> listaMetas = new ArrayList<>();
        Cursor cursor = objBanco.query("TB_META", null, null, null, null, null, "DESCRICAO ASC");

        if(cursor.getCount() > 0 && cursor.moveToFirst()){
            do{
                listaMetas.add(new Meta(
                        cursor.getInt(cursor.getColumnIndex("CODIGO")),
                        cursor.getString(cursor.getColumnIndex("DESCRICAO")),
                        cursor.getShort(cursor.getColumnIndex("TIPO")),
                        cursor.getInt(cursor.getColumnIndex("VALOR_SIMBOLICO")),
                        (cursor.getInt(cursor.getColumnIndex("META_PADRAO"))==1),
                        (cursor.getInt(cursor.getColumnIndex("META_ATINGIDA"))==1)
                ));
            }while(cursor.moveToNext());
        }

        cursor.close();
        objBanco.close();
        return listaMetas;
    }
}
