package app.nefd.teenactive.controller.sensors;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import java.util.Date;

import app.nefd.teenactive.controller.enums.AccelerometerSignals;
import app.nefd.teenactive.controller.utils.ScalarKalmanFilter;

/**
 * Classe para o processamento de dados advindos do acelerômetro
 */
public class AccelDataProcessor {

    /**
     * Parâmetro para detecção de passos. How many periods it is sleeping.
     * <br>
     * If DELAY_GAME: T ~= 20ms => f = 50Hz
     * and MAX_TEMPO = 240bpms
     * then:
     * 60bpm - 1000milliseconds
     * 240bpm - 250milliseconds
     *
     * n - periods
     * n = 250msec / T
     * n = 250 / 20 ~= 12
     */
    private static final int PERIODOS_INATIVO = 12;

    //region Variáveis globais

    //Variáveis dinâmicas
    public boolean isContadorAtivo = true;

    private int contadorInativo = 0;
    private static float limiar = 12.72f;
    private double[] vValoresAceleracao = new double[AccelerometerSignals.count];
    private double[] vUltimosValoresAceleracao = new double[AccelerometerSignals.count];

    private SensorEvent evento;

    //Variáveis computacionais
    private double[] gravity = new double[3];
    private double[] aceleracaoLinear = new double[3];
    private ScalarKalmanFilter vCascataFiltro[] = new ScalarKalmanFilter[3];
    private static AccelDataProcessor instance = null;
    //endregion

    //region Metodos

    /**
     * Retorna a instância estática da classe
     * @return
     */
    public static AccelDataProcessor getInstance() {

        if (instance == null)
            return new AccelDataProcessor();
        return instance;
    }

    /**
     * Pega o valor corrente do evento
     * @param evento evento
     */
    public void setEvent(SensorEvent evento) {
        this.evento = evento;
    }

    /**
     * Pega o valor do limiar
     * @return
     */
    public float getLimiar() {
        return limiar;
    }

    /**
     * Define o limiar que considera o passo
     * @param limiar
     */
    public void setLimiar(float limiar){
        AccelDataProcessor.limiar = limiar;
    }

    /**
     * Pega o tempo do evento
     * @see <a href="http://stackoverflow.com/questions/5500765/accelerometer-sensorevent-timestamp">Para milisegundos.</a>
     * @return tempo em milisegundos
     */
    public long timestampToMilliseconds() {
        return (new Date()).getTime() + (evento.timestamp - System.nanoTime()) / 1000000L;
    }

    /**
     * Inicia o filtro de Kalman
     */
    public void initKalman() {
        // Define o filtro
        vCascataFiltro[0] = new ScalarKalmanFilter(1, 1, 0.01f, 0.0025f);
        vCascataFiltro[1] = new ScalarKalmanFilter(1, 1, 0.01f, 0.0025f);
        vCascataFiltro[2] = new ScalarKalmanFilter(1, 1, 0.01f, 0.0025f);
    }

    /**
     * Suaviza o sinal do acelerometro
     * @param measurement valor de mensuração
     */
    private double filter(double measurement){
        double f1 = vCascataFiltro[0].corrigir(measurement);
        double f2 = vCascataFiltro[1].corrigir(f1);
        double f3 = vCascataFiltro[2].corrigir(f2);
        return f3;
    }

    /**
     * Calcular filtro de Kalman
     * @param i
     * @return
     */
    public double calcKalman(int i) {
        vValoresAceleracao[i] = filter(vValoresAceleracao[i]);
        return vValoresAceleracao[i];
    }

    /**
     * Filtra o sinal da gravidade<br>
     * Neste exemplo, alpha é calculado com <b>t / (t + dT)</b>,
     * onde <b>t</b> é o filtro de baixa intensidade em tempo constante e
     * <b>dT</b> é o <b>evento</b> delivery rate.
     */
    public void calcFilterGravity() {

        final float alpha = 0.9f;

        //Isola a força da gravidade no filtro de baixo limiar
        gravity[0] = alpha * gravity[0] + (1 - alpha) * evento.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * evento.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * evento.values[2];
    }

    /**
     * Calcula o vetor de magnitude do movimento |V| = sqrt(x^2 + y^2 + z^2)
     * @param i Indentificador do sinal.
     * @return
     */
    public double calcMagnitudeVector(int i) {

        // Remove a contribuição da gravidade das leituras
        aceleracaoLinear[0] = evento.values[0] - gravity[0];
        aceleracaoLinear[1] = evento.values[1] - gravity[1];
        aceleracaoLinear[2] = evento.values[2] - gravity[2];

        vValoresAceleracao[i] = Math.sqrt(
                aceleracaoLinear[0] * aceleracaoLinear[0] +
                aceleracaoLinear[1] * aceleracaoLinear[1] +
                aceleracaoLinear[2] * aceleracaoLinear[2]
                );

        return vValoresAceleracao[i];
    }

    /**
     * Calcula a diferença da gravidade entre: (x^2 + y^2 + z^2) / G^2
     * @param i Indentificador do sinal
     * @return
     */
    public double calcGravityDiff(int i) {
        vValoresAceleracao[i] = (
                evento.values[0] * evento.values[0] +
                        evento.values[1] * evento.values[1] +
                        evento.values[2] * evento.values[2]) /
                (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        return vValoresAceleracao[i];
    }

    /**
     * Calcula o valor exponencial do movimento
     * @see <a href="http://stackoverflow.com/questions/16392142/android-accelerometer-profiling">Stack Overflow discussion</a>
     * @param i indentificador do sinal
     * @return
     */
    public double calcExpMovAvg(int i) {
        final double alpha = 0.1;
        vValoresAceleracao[i] = alpha * vValoresAceleracao[i] + (1 - alpha) * vUltimosValoresAceleracao[i];
        vUltimosValoresAceleracao[i] = vValoresAceleracao[i];
        return vValoresAceleracao[i];
    }

    /**
     * My step detection algorithm.
     * When the value is over the threshold, the step is found and the algorithm sleeps for
     * the specified distance which is {@link #PERIODOS_INATIVO this }.
     * @param i signal identifier.
     * @return step found / not found
     */
    public boolean stepDetected(int i) {
        if (contadorInativo == PERIODOS_INATIVO) {
            contadorInativo = 0;
            if (!isContadorAtivo)
                isContadorAtivo = true;
        }
        if (vValoresAceleracao[i] > limiar) {
            if (isContadorAtivo) {
                contadorInativo = 0;
                isContadorAtivo = false;
                return true;
            }
        }
        ++contadorInativo;
        return false;
    }
    //endregion
}
