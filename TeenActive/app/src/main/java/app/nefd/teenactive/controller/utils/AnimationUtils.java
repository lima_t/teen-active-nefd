package app.nefd.teenactive.controller.utils;

import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Created by tarcisio on 01/05/17.
 */

public class AnimationUtils {

    /**
     * Anima um objeto com Fadding In ou Fadding Out
     * @param view - objeto
     * @param duracao - duração em milisegundos
     * @param percentageStart - alfa inicial
     * @param percentageEnd - alfa final
     */
    public static void fadding(View view, long duracao, short percentageStart, short percentageEnd){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "alpha", percentageStart/100, percentageEnd/100);
        animation.setDuration(duracao);
        animation.start();
    }
}
