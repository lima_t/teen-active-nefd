package app.nefd.teenactive.ui.screens;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMeta;
import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Meta;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.CientificUtils;

public class ActivityDetalheTreino extends AppCompatActivity {

    //region Variáveis globais
    private Treino treino;
    //endregion

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_treino);

        try{

            //region Iniciando os Componentes XML
            Toolbar toolbar = (Toolbar) findViewById(R.id.barraAcaoDetalheTreino);
            Intent itGetDados = this.getIntent();

            //Rótulos do painel de indicadores gerais
            TextView labNomeAtividade = (TextView) findViewById(R.id.labNomeAtividade);
            TextView labDescricaoTreino = (TextView) findViewById(R.id.labDescricaoTreino);
            TextView labTempoTotal = (TextView) findViewById(R.id.labTempoTotal);
            TextView labDistanciaPercorrida = (TextView) findViewById(R.id.labDistanciaPercorrida);
            TextView labKcalConsumida = (TextView) findViewById(R.id.labKcalConsumida);
            TextView labPassosEfetuados = (TextView) findViewById(R.id.labPassosEfetuados);

            //Rótulos do painel de indicadores extras
            TextView labTempoOcioso = (TextView) findViewById(R.id.labTempoOcioso);
            TextView labTempoLeve = (TextView) findViewById(R.id.labTempoLeve);
            TextView labTempoModerado = (TextView) findViewById(R.id.labTempoModerado);
            TextView labTempoVigoroso = (TextView) findViewById(R.id.labTempoVigoroso);
            TextView labMediaPassosMin = (TextView) findViewById(R.id.labMediaPassosMin);
            TextView labVelocidadeMedia = (TextView) findViewById(R.id.labVelocidadeMedia);

            //Rótulos do painel de indicadores técnicos
            TextView labTreinoAutonomo = (TextView) findViewById(R.id.labTreinoAutonomo);
            TextView labTreinoPassivo = (TextView) findViewById(R.id.labTreinoPassivo);

            //Botões
            Button btDescartarTreino = (Button) findViewById(R.id.btDescartarTreino);
            Button btSalvarTreino = (Button) findViewById(R.id.btSalvarTreino);

            //ImageViews
            CircularImageView imgIconeAtividade = (CircularImageView) findViewById(R.id.imgFotoPerfil);
            //endregion

            //region Carregando os valores dos componentes
            setSupportActionBar(toolbar);

            //Preenche o objeto Treino com outro objeto treino passado por parâmetro
            treino = itGetDados.getParcelableExtra("treino");

            //Habilita ou desabilita os botões de salvar e descartar
            if(treino.getCodigo() > 0){
                btDescartarTreino.setVisibility(View.INVISIBLE);
                btSalvarTreino.setVisibility(View.INVISIBLE);
            }

            if(treino != null){

                //Painel dos indicadores gerais
                assert imgIconeAtividade != null;
                imgIconeAtividade.setImageResource(itGetDados.getIntExtra("atividade_icone", 0));

                labNomeAtividade.setText("Resumo: "+treino.getAtividadeDescricao());
                labDescricaoTreino.setText(
                        treino.getAtividadeDescricao()+
                        " iniciada às "+SimpleDateFormat.getTimeInstance().format(treino.getDataTreino())+
                        " do dia "+SimpleDateFormat.getDateInstance().format(treino.getDataTreino())
                );
                labTempoTotal.setText(AppUtils.formatarMillisToHoras(treino.getTempoTotal()));
                labDistanciaPercorrida.setText(
                        (
                                treino.getDistanciaPercorrida() >= 1_000?
                                        String.format(AppUtils.pt_BR, "%.2f km", (treino.getDistanciaPercorrida()/1_000f)):
                                        String.format(AppUtils.pt_BR, "%d m", (int)treino.getDistanciaPercorrida())
                        )
                );
                labKcalConsumida.setText(String.format(AppUtils.pt_BR,"%.2f kcal", treino.getKcalConsumida()));
                labPassosEfetuados.setText(treino.getQuantidadePassos()+" passos");

                //Painel dos indicadores extras
                labTempoOcioso.setText(AppUtils.formatarMillisToHoras(treino.getTempoOcioso()));
                labTempoLeve.setText(AppUtils.formatarMillisToHoras(treino.getTempoIntensidadeLeve()));
                labTempoModerado.setText(AppUtils.formatarMillisToHoras(treino.getTempoIntensidadeModerada()));
                labTempoVigoroso.setText(AppUtils.formatarMillisToHoras(treino.getTempoIntensidadeVigorosa()));
                labMediaPassosMin.setText(
                        CientificUtils.calcularMediaPassosMin(treino.getTempoTotal(), treino.getQuantidadePassos()) > 0?
                                String.format(AppUtils.pt_BR, "%d passos", CientificUtils.calcularMediaPassosMin(treino.getTempoTotal(), treino.getQuantidadePassos())):
                                "< 1 pas/min"
                );
                labVelocidadeMedia.setText(
                        CientificUtils.calcularVelocidadeMedia(treino.getTempoTotal(), treino.getDistanciaPercorrida())>0?
                        String.format(AppUtils.pt_BR, "%.2f km/h", CientificUtils.calcularVelocidadeMedia(treino.getTempoTotal(), treino.getDistanciaPercorrida())):
                                "< 1 km/h"
                );

                //Painel dos indicadores técnicos
                labTreinoAutonomo.setText(treino.isTreinoAutonomo()?"Sim":"Não");
                labTreinoPassivo.setText(treino.isTreinoPassivo()?"Sim":"Não");
            }
            //endregion

            //region Setandos os eventos dos componentes
            btDescartarTreino.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessageDialog.exibirDialogoQuestao(ActivityDetalheTreino.this, "Aviso",
                            "Você tem certeza que deseja descartar os registros desse treino?")
                            .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            })
                            .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            });

            btSalvarTreino.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{

                        verificarMetaFoiAtingida();
                        new DAOTreino(ActivityDetalheTreino.this).inserirTreino(treino);
                        Toast.makeText(ActivityDetalheTreino.this, "O treino foi registrado com sucesso!", Toast.LENGTH_SHORT).show();
                        finish();

                    }catch (final Exception erro){
                        //region Dialogo de reportar Bug
                        MessageDialog.exibirDialogoErro(ActivityDetalheTreino.this, "Erro",
                                "Ocorreu um erro ao tentar salvar as informaões do treino.\n[Erro]:\n\n"+erro.getMessage())
                                .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //Abre o compositor de e-mails para enviar o relatório do erro
                                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                EmailType.RELATORIO_BUG,
                                                new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                AppUtils.comporEmailErro(erro)),
                                                "Enviar E-mail"));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                        //endregion
                    }
                }
            });
            //endregion

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityDetalheTreino.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onBackPressed() {

        //Verifica se o parametro é de uma atividade que já foi salva, se sim ele não mostra a mensagem de salvar
        if(treino.getCodigo() == 0){
            MessageDialog.exibirDialogoQuestao(ActivityDetalheTreino.this, "Aviso",
                    "Você tem certeza que deseja retornar a tela principal e descartar os registros desse treino?")
                    .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }else{
            super.onBackPressed();
        }
    }

    /**
     * Verifica se os dados registrados no treino atingiram a meta vigente, caso seja positivo a mesma é definida como
     * meta atingida
     * @throws Exception
     */
    private void verificarMetaFoiAtingida() throws Exception{
        Meta meta = new DAOMeta(ActivityDetalheTreino.this).consultarMetaFavorita();

        switch (meta.getTipo()){
            case 0:
                //Tempo
                if(AppUtils.converterMillisToMinutos(treino.getTempoTotal()) >= meta.getValorSimbolico()){
                    new DAOMeta(ActivityDetalheTreino.this).setarMetaVencida(meta.getCodigo());
                }
                break;
            case 1:
                //Kcal
                if(treino.getKcalConsumida() >= meta.getValorSimbolico()){
                    new DAOMeta(ActivityDetalheTreino.this).setarMetaVencida(meta.getCodigo());
                }
                break;
            case 2:
                //Passos
                if(treino.getQuantidadePassos() >= meta.getValorSimbolico()){
                    new DAOMeta(ActivityDetalheTreino.this).setarMetaVencida(meta.getCodigo());
                }
                break;
            case 3:
                //Distância
                if(treino.getDistanciaPercorrida() >= meta.getValorSimbolico()){
                    new DAOMeta(ActivityDetalheTreino.this).setarMetaVencida(meta.getCodigo());
                }
                break;
            default:
                //Nenhuma das opções válidas
                break;
        }
    }

}
