package app.nefd.teenactive.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.nefd.teenactive.database.ConexaoBanco;
import app.nefd.teenactive.database.entities.Medidas;

/**
 * A Classe contém atributos, construtores e metodos necessários para efetuar as operações DML e acesso à dados
 * da entidade/objeto Registro de pesagens
 */
public class DAOMedidas {

    private SQLiteDatabase objBanco = null;

    /**
     * Construtor default da classe, automaticamente ela já retorna uma conexão para a classe para que possa efetuar
     * as operações DML durante o NEW
     * @param telaContexto
     */
    public DAOMedidas(Context telaContexto){
        ConexaoBanco objConexaoBanco = new ConexaoBanco(telaContexto);
        objBanco = objConexaoBanco.getWritableDatabase();
    }

    /**
     * Método para inserir um registro de medidas no banco de dados.
     * @param medidas objeto do tipo Medidas para ser inserido.
     */
    public void inserirMedidas(Medidas medidas) throws SQLiteException, ParseException{

        ContentValues objValoresParametros = new ContentValues();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
        objFormatoData.applyPattern("yyyy-MM-dd");//Altera a data para o padrão americano (o SQLite só entende esse formato)

        objValoresParametros.put("ALTURA", medidas.getAltura());
        objValoresParametros.put("PESO", medidas.getPeso());
        objValoresParametros.put("DATA", objFormatoData.format(medidas.getDataRegistro()));

        objBanco.insert("TB_MEDIDAS", null, objValoresParametros);
        objBanco.close();
    }

    /**
     * Atualiza os dados de um registro de medidas no banco de dados, essa ocasião ocorre quando se tenta registrar novos
     * valores de medidas em um mesmo dia, é verificado se o objeto medidas já possui codigo definido caso positivo
     * os dados são atualizados ao invés de serem inseridos.
     * @param medidas
     * @throws SQLiteException
     * @throws ParseException
     */
    public void atualizarMedidas(Medidas medidas) throws SQLiteException, ParseException{

        ContentValues objValoresParametros = new ContentValues();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("dd/MM/yyyy");
        objFormatoData.applyPattern("yyyy-MM-dd");//Altera a data para o padrão americano (o SQLite só entende esse formato)

        objValoresParametros.put("ALTURA", medidas.getAltura());
        objValoresParametros.put("PESO", medidas.getPeso());

        objBanco.update("TB_MEDIDAS", objValoresParametros, "CODIGO = "+medidas.getCodigo(), null);
        objBanco.close();
    }

    /**
     * Tenta retornar um objeto medidas preenchido com supostos registros que tenham sido cadastrados no mesmo dia
     * caso exista um registro com a data corrente ele retornará um objeto <b>Medidas</b> indicando que a operação a
     * ser feita posteriormente na tela de cadastro de medidas não será uma inserção e sim uma atualização de dados
     * @return
     * @throws SQLiteException
     * @throws ParseException
     */
    public Medidas getMedidasHoje() throws SQLiteException, ParseException{
        Medidas medidasHoje = new Medidas();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd");//Convertendo a String da data recuperada no banco

        Cursor cursor = objBanco.rawQuery("SELECT * FROM TB_MEDIDAS WHERE DATA = CURRENT_DATE ORDER BY DATA DESC LIMIT 1", null);

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            medidasHoje = new Medidas(
                    cursor.getInt(cursor.getColumnIndex("CODIGO")),
                    cursor.getShort(cursor.getColumnIndex("ALTURA")),
                    cursor.getFloat(cursor.getColumnIndex("PESO")),
                    objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA")))
            );

        }

        cursor.close();
        return medidasHoje;
    }

    /**
     * Retorna uma lista de objetos do tipo Medidas com todos os registros do banco de dados
     * @return
     * @throws SQLiteException
     */
    public ArrayList<Medidas> getListaMedidas(byte filterType) throws SQLiteException, ParseException {

        //Criando objetos utilizaveis
        ArrayList<Medidas> listaMedidas = new ArrayList<>();
        SimpleDateFormat objFormatoData = new SimpleDateFormat("yyyy-MM-dd");//Convertendo a String da data recuperada no banco

        //Crianco consulta e cursor
        Cursor cursor = null;

        switch (filterType){
            case 0:
                cursor = objBanco.rawQuery(
                        "SELECT * FROM TB_MEDIDAS WHERE DATA BETWEEN datetime('now', 'start of month', '-1 month') AND datetime('now', 'localtime');", null);
                break;
            case 1:
                cursor = objBanco.rawQuery(
                        "SELECT * FROM TB_MEDIDAS WHERE DATA BETWEEN datetime('now', 'start of month', '-6 months') AND datetime('now', 'localtime');", null);
                break;
            case 2:
                cursor = objBanco.rawQuery(
                        "SELECT * FROM TB_MEDIDAS WHERE DATA BETWEEN datetime('now', 'start of year', '-1 year') AND datetime('now', 'localtime');", null);
                break;
            case 3:
                cursor = objBanco.rawQuery("SELECT * FROM TB_MEDIDAS;", null);
                break;
        }

        if(cursor.getCount() > 0 && cursor.moveToFirst()){

            do{

                //Adiciona o item à lista
                listaMedidas.add(new Medidas(
                        cursor.getInt(cursor.getColumnIndex("CODIGO")),
                        cursor.getShort(cursor.getColumnIndex("ALTURA")),
                        cursor.getFloat(cursor.getColumnIndex("PESO")),
                        objFormatoData.parse(cursor.getString(cursor.getColumnIndex("DATA")))
                ));

            }while(cursor.moveToNext());

        }

        cursor.close();
        objBanco.close();
        return listaMedidas;
    }
}