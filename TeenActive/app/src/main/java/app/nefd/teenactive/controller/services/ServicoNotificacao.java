package app.nefd.teenactive.controller.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import java.text.ParseException;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.ui.screens.ActivityPrincipal;
import app.nefd.teenactive.database.entities.Configuracao;

/**
 * Classe responsável por controlar o serviço de notificação do usuário
 */
public class ServicoNotificacao extends Service {

    //region Variáveis globais
    private NotificationManager objNotificationManager;
    private ThreadNotificacao taskNotificacao;
    private Configuracao config;
    //endregion

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try{

            //region Iniciando variáveis e objetos
            config = new Configuracao(getApplicationContext());
            taskNotificacao = new ThreadNotificacao();
            objNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            //endregion

            Log.i("Teen-Active Service", "Serviço de notificação iniciado");
            taskNotificacao.start();


        }catch(Exception erro){
            Log.e("Teen-Active Service", "Ocorreu um erro no onCreate() do serviço: ", erro);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*Esse retorno garante que o serviço irá voltar a rodar sempre que o processo for removido da memória
        pelo sistema operacional*/
        return ServicoNotificacao.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("Teen-Active Service", "Serviço Finalizado pelo onDestroy");
        taskNotificacao.continueLoop = false;
        Log.i("Teen-Active Service", "Thread "+taskNotificacao.getName()+" notificada para parar");
        objNotificationManager.cancel(0);
        Log.i("Teen-Active Service", "Notificações canceladas, aplicativo está aberto");

        super.onDestroy();
    }

    /**
     * Constrói uma notificação e exibe na barra de noticações do android
     */
    protected void exibirNotificacao() throws ParseException, SQLiteException{


        Perfil perfil = new DAOPerfil(getApplicationContext()).carregarPerfil();
        Intent objItentNotification = new Intent(ServicoNotificacao.this, ActivityPrincipal.class);

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.content_notificacao);
        contentView.setImageViewResource(R.id.imgIconeNotificacao, R.mipmap.icone_app);
        contentView.setTextViewText(R.id.labTituloNotificacao, "Hey! "+perfil.getNome());
        contentView.setTextViewText(R.id.labDetalheNotificacao, "Que tal praticarmos uma nova atividade hoje?");

        NotificationCompat.Builder notificacao = new NotificationCompat.Builder(getApplicationContext());
        notificacao.setContent(contentView);
        notificacao.setSmallIcon(R.drawable.icone_lembrete);
        notificacao.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.PRIORITY_HIGH);
        notificacao.setContentIntent(
                PendingIntent.getActivity(getApplicationContext(), 0, objItentNotification, PendingIntent.FLAG_UPDATE_CURRENT)
        );

        objNotificationManager.notify(getApplicationContext().hashCode(), notificacao.build());

    }

    //region Classe interna para a Thread
    private class ThreadNotificacao extends Thread {

        //Variáveis globais
        boolean continueLoop = true;

        //Construtores
        ThreadNotificacao() {
            this.continueLoop = true;
            this.setPriority(ThreadNotificacao.NORM_PRIORITY);
        }

        //Métodos
        @Override
        public void run() {
            super.run();

            int segundosPassados = 0;

            Log.i("Teen-Active Service", "Thread "+getName()+" iniciada!...");
            Log.i("Teen-Active Service", "Thread "+getName()+" aguardando "+(60*config.getTempoNotificacao())+" segundos para exibir a notificação...");

            while(continueLoop) {
                try{

                    Thread.sleep(1000);//Dorme a thread por 1 segundo

                    if(segundosPassados >= (60*config.getTempoNotificacao())) {
                        exibirNotificacao();
                        segundosPassados = 0;
                        Log.i("Teen-Active Service", "Thread "+getName()+" chamou a notificação!");
                    }else{
                        segundosPassados++;
                        Log.i("Teen-Active Service", "Thread "+getName()+" não atingiu o intervalo estipulado -> "+segundosPassados+" segundos passados...");
                    }

                }catch(Exception erro){
                    Log.e("Teen-Active Service", "Ocorreu um erro no onCreate() do serviço: ", erro);
                }
            }
            Log.i("Teen-Active Service", "Thread "+getName()+" finalizada!...");
        }
    }
    //endregion
}
