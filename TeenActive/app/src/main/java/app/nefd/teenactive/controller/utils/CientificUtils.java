package app.nefd.teenactive.controller.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;

import app.nefd.teenactive.database.dao.DAOReferencia;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Referencia;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.ui.screens.ActivityHistoricoMedidas;

/**
 * Classe responsavel por armazenar os metodos para calculos de resultados cientificos
 */
public class CientificUtils {

    /**
     * Classifica o IMC a partir das medidas corporais do individuo,
     * @param perfil Perfil do individuo para ser utilizado como base do calculo
     * @return Retorna os valores:<br>
     *     <b>0 = Muito baixo</b>;<br>
     *     <b>1 = Baixo</b>;<br>
     *     <b>2 = Normal</b>;<br>
     *     <b>3 = Alto</b>;<br>
     *     <b>4 = Muito alto</b>;<br>
     */
    public static byte classificarIMC(Perfil perfil, final Context context){

        float imc = calcularIMC(perfil.getAltura(), perfil.getPeso());

        try{

            int meses = calcularIdadeMeses(perfil.getDataNascimento());

            if(calcularIdadeAnos(perfil.getDataNascimento()) > 19){

                /*Classificação do IMC para adultos lembrar de pegar a tabela de referência que tem 5 classificações*/
                if(imc < 17.00){
                    return 0;//Classificado como muito baixo
                }else if(imc < 18.50){
                    return 1;//Classificado como baixo
                }else if(imc < 25.00){
                    return 2;//Classificado como normal pois abrange a escala entre 15 & 85 percentil
                }else if(imc < 30.00){
                    return 3;//Classificado como alto;
                }else{
                    return 4;//Classificado como Muito alto pois passou por todos os IFs e não é maior 30
                }

            }else{

                Referencia referenciaIMC = new DAOReferencia(context).
                        consultarReferencia(calcularIdadeMeses(perfil.getDataNascimento()), Referencia.VAR_IMC, perfil.getSexo());

                if(referenciaIMC == null){
                    throw new Exception("O valor do objeto referenciaIMC retornou nulo na consulta");
                }

                //Classificação do IMC para crianças e adolescentes
                if(imc < referenciaIMC.getPercentil3()){
                    return 0;//Clasificado como muito baixo
                }else if(imc < referenciaIMC.getPercentil15()){
                    return 1;//Classificado como baixo
                }else if(imc < referenciaIMC.getPercentil85()){
                    return 2;//Classificado como normal pois abrange a escala entre 15 & 85 percentil
                }else if(imc < referenciaIMC.getPercentil97()){
                    return 3;//Classificado como alto;
                }else{
                    return 4;//Classificado como Muito alto pois passou por todos os IFs e não é maior do que o percentil 97
                }
            }

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(context, "Erro",
                    "Ocorreu um erro ao tentar classificar o IMC.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            context.startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return -1;//Sem classificação pois deu erro
        }
    }

    /**
     * Classifica o PESO a partir das medidas corporais do individuo,
     * @param perfil Perfil do individuo para ser utilizado como base do calculo
     * @return Retorna os valores:<br>
     *     <b>0 = Muito baixo</b>;<br>
     *     <b>1 = Baixo</b>;<br>
     *     <b>2 = Normal</b>;<br>
     *     <b>3 = Alto</b>;<br>
     *     <b>4 = Muito alto</b>;<br><br>
     *     <b>-2 = Usado para quando o individuo tem idade acima de 18 anos e não possui mais curva de peso</b>;<br>
     */
    public static byte classificarPeso(Perfil perfil, final Context context){
        try{

            //float pesoIdeal = calcularPesoIdeal(perfil.getAltura(), 25f-((25f - 18.5f)/2));

            if(calcularIdadeAnos(perfil.getDataNascimento()) > 10){

                return -2;

            }else{

                Referencia referenciaPeso = new DAOReferencia(context).
                        consultarReferencia(calcularIdadeMeses(perfil.getDataNascimento()), Referencia.VAR_PESO, perfil.getSexo());

                if(referenciaPeso == null){
                    throw new Exception("O valor do objeto referenciaPeso retornou nulo na consulta");
                }

                //Classificação do PESO para crianças e adolescentes
                if(perfil.getPeso() < referenciaPeso.getPercentil3()){
                    return 0;//Clasificado como muito baixo
                }else if(perfil.getPeso() < referenciaPeso.getPercentil15()){
                    return 1;//Classificado como baixo
                }else if(perfil.getPeso() < referenciaPeso.getPercentil85()){
                    return 2;//Classificado como normal pois abrange a escala entre 15 & 85 percentil
                }else if(perfil.getPeso() < referenciaPeso.getPercentil97()){
                    return 3;//Classificado como alto;
                }else{
                    return 4;//Classificado como Muito alto pois passou por todos os IFs e não é maior do que o percentil 97
                }

            }

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(context, "Erro",
                    "Ocorreu um erro ao tentar classificar o peso.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            context.startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return -1;//Sem classificação pois deu erro
        }
    }

    /**
     * Classifica o PESO a partir das medidas corporais do individuo,
     * @param perfil Perfil do individuo para ser utilizado como base do calculo
     * @return Retorna os valores:<br>
     *     <b>0 = Muito baixo</b>;<br>
     *     <b>1 = Baixo</b>;<br>
     *     <b>2 = Normal</b>;<br>
     *     <b>3 = Alto</b>;<br>
     *     <b>4 = Muito alto</b>;<br><br>
     *     <b>-2 = Usado para quando o individuo tem idade acima de 18 anos e não possui mais curva de peso</b>;<br>
     */
    public static byte classificarAltura(Perfil perfil, final Context context){
        try{

            if(calcularIdadeAnos(perfil.getDataNascimento()) > 19){
                return -2;
            }else{

                Referencia referenciaAltura = new DAOReferencia(context).
                        consultarReferencia(calcularIdadeMeses(perfil.getDataNascimento()), Referencia.VAR_ALTURA, perfil.getSexo());

                if(referenciaAltura == null){
                    throw new Exception("O valor do objeto referenciaAltura retornou nulo na consulta");
                }

                //Classificação do PESO para crianças e adolescentes
                if(perfil.getAltura() < referenciaAltura.getPercentil3()){
                    return 0;//Clasificado como muito baixo
                }else if(perfil.getAltura() < referenciaAltura.getPercentil15()){
                    return 1;//Classificado como baixo
                }else if(perfil.getAltura() < referenciaAltura.getPercentil85()){
                    return 2;//Classificado como normal pois abrange a escala entre 15 & 85 percentil
                }else if(perfil.getAltura() < referenciaAltura.getPercentil97()){
                    return 3;//Classificado como alto;
                }else{
                    return 4;//Classificado como Muito alto pois passou por todos os IFs e não é maior do que o percentil 97
                }
            }

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(context, "Erro",
                    "Ocorreu um erro ao tentar classificar o peso.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            context.startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return -1;//Sem classificação pois deu erro
        }
    }

    /**
     * Calcula a distância entre dois pontos de coordenadas
     * @param pontoInicial coordenadas do ponto inicial
     * @param pontoFinal coordenadas do ponto final
     * @return distância entre os pontos em <b>metros</b>
     */
    public static double calcularDistanciaMetros(LatLng pontoInicial, LatLng pontoFinal){

        double dLat = Math.toRadians(pontoFinal.latitude-pontoInicial.latitude);
        double dLon = Math.toRadians(pontoFinal.latitude-pontoInicial.latitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(pontoInicial.latitude)) * Math.cos(Math.toRadians(pontoFinal.latitude)) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return 6366000 * c;
    }

    /**
     * Calcula a distância entre dois pontos baseado no algoritmo do Google
     * @param pontoInicial coordenadas do ponto inicial
     * @param pontoFinal coordenadas do ponto final
     * @return distância entre os pontos em <b>metros</b>
     */
    public static double calcularDistanciaByGoogle(LatLng pontoInicial, LatLng pontoFinal){
        Location pontoA = new Location("Ponto A");
        Location pontoB = new Location("Ponto B");

        pontoA.setLatitude(pontoInicial.latitude);
        pontoA.setLongitude(pontoInicial.longitude);
        pontoB.setLatitude(pontoFinal.latitude);
        pontoB.setLongitude(pontoFinal.longitude);

        return pontoA.distanceTo(pontoB);
    }

    /**
     * Calcula o IMC
     * @param alturaCm Altura em cm (convertida posteriormente em metros)
     * @param pesoKg Peso em Kilos
     * @return Float com o IMC
     */
    public static float calcularIMC(short alturaCm, float pesoKg){
        return pesoKg / ((alturaCm/100f)*(alturaCm/100f));
    }

    /**
     * Calcula o peso ideal a partir da constante mediana indicada pelo centro entre a escala do IMC baixo e IMC sobrepeso
     * @param alturaCm Altura em cm (convertida posteriormente em metros)
     * @param medianaEscala valor mediano entre o ínicio e o fim da escala que classifica como normal (Ex: na escala de adultos o limiar
     *                      para considerar um sobre peso é 25 e o subpeso é 18, então 25-18 = 7 --> 7 / 2 = 3,5 --> calcularPesoIdeal(altura, 18+3,5 = <b>21,50</b>))
     * @return Float com o valor do peso ideal
     */
    public static float calcularPesoIdeal(short alturaCm, float medianaEscala){
        return medianaEscala * ((alturaCm/100f)*(alturaCm/100f));
    }

    /**
     * Calcula o gasto calórico baseada na fórmula da revisão da autora <b>Kate Ridley</b>
     * @return o dispêndio energético para jovens
     */
    public static float calcularGastoCalorico(float mets, float tmbMin, long tempoMillis){
        return mets * tmbMin * AppUtils.converterMillisToFloat(tempoMillis);
    }

    /**
     * Calcula a taxa metabólica basal por minuto kcal.kg-1.min-1 utlizando a equação de
     * <i>Schofield</i>.
     * @return TMB/min
     *
     */
    public static float calcularTMBmin(Perfil perfil){

        if(perfil.getSexo() == 0){

            //Se o sexo for masculino então
            if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 10){

                //Se a idade for até 10 anos então
                return ((0.095f * perfil.getPeso() + 2.110f) * 239)/1440;

            }else if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 18){

                //Se a idade for até 18 anos então
                return ((0.074f * perfil.getPeso() + 2.754f) * 239)/1440;

            }else if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 30){

                //Se a idade for até 30 anos então
                return ((0.063f * perfil.getPeso() + 2.896f) * 239)/1440;

            }else{

                //Se a idade for até 60 anos então
                return ((0.048f * perfil.getPeso() + 3.653f) * 239)/1440;
            }

        }else{

            //Se o sexo for feminino então
            if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 10){

                //Se a idade for até 10 anos então
                return ((0.085f * perfil.getPeso() + 2.033f) * 239)/1440;

            }else if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 18){

                //Se a idade for até 18 anos então
                return ((0.056f * perfil.getPeso() + 2.898f) * 239)/1440;

            }else if(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()) <= 30){

                //Se a idade for até 30 anos então
                return ((0.062f * perfil.getPeso() + 2.036f) * 239)/1440;

            }else{

                //Se a idade for até 60 anos então
                return ((0.034f * perfil.getPeso() + 3.538f) * 239)/1440;
            }
        }
    }

    /**
     * Calcula a idade em anos
     * @param dataNascimento Data de nascimento
     * @return
     */
    public static short calcularIdadeAnos(Date dataNascimento){

        if(dataNascimento.getDay() > Calendar.getInstance().getTime().getDay() &&
           dataNascimento.getMonth() > Calendar.getInstance().getTime().getMonth())
        {
            return (short)(Calendar.getInstance().getTime().getYear() - (dataNascimento.getYear()-1));

        }else{
            return (short)(Calendar.getInstance().getTime().getYear() - dataNascimento.getYear());
        }
    }

    /**
     * Calcula a quantidade média de passos dados por min através da regra de 3 simples onde seria por exemplo<br/>
     * 10min -> 400pas<br/>
     * 01min -> ? pas<br/>
     *
     * 400pas / 10min = 4 passos/min<br/>
     * É possível também usar a proporcionalidade inversa quando o tempo for maior que a quantidade de passos.
     *
     * @param tempoTotalTreino Tempo total do treino em millisegundos (será convertido em minutos)
     * @param quantidadePassos Quantidade total de passos efetuada na atividade
     * @return
     */
    public static short calcularMediaPassosMin(long tempoTotalTreino, int quantidadePassos){
        float tempoMinDecimal = AppUtils.converterMillisToFloat(tempoTotalTreino);

        if(quantidadePassos > 0){

            return (short)(quantidadePassos/tempoMinDecimal);

        }else{
            return 0;
        }
    }

    /**
     * Calcula velocidade média distância(km) / tempo
     * @param tempoTotal Tempo total da atividade em millisegundos
     * @param distanciaMetros Distância total percorrida
     * @return
     */
    public static float calcularVelocidadeMedia(long tempoTotal, float distanciaMetros){
        if(distanciaMetros > 0){
            return (distanciaMetros/1000) / (AppUtils.converterMillisToMinutos(tempoTotal)/60f);
        }else{
            return 0.0f;
        }
    }

    /**
     * Calcula a idade em meses
     * @param dataNascimento Data de nascimento
     * @return
     */
    public static short calcularIdadeMeses(Date dataNascimento){
        Calendar cDataNascimento = Calendar.getInstance();
        Calendar cDataAtual = Calendar.getInstance();

        cDataNascimento.setTime(dataNascimento);

        return (short)((cDataAtual.get(Calendar.YEAR) * 12 + cDataAtual.get(Calendar.MONTH)) - (cDataNascimento.get(Calendar.YEAR) * 12 + cDataNascimento.get(Calendar.MONTH)));
    }
}
