package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOAtividade;
import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.AdapterPhotoListView;
import app.nefd.teenactive.ui.itens.PhotoListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivityAtividade extends AppCompatActivity {

    private ListView listViewFavorita, listViewNaoFavorita;
    private ArrayList<Atividade> listaFavoritas, listaNaoFavoritas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade);

        try{

            //region Iniciando a instância dos componentes
            Toolbar toolbar = (Toolbar) findViewById(R.id.barraAcaoAtividade);
            TabHost painelTabulado = (TabHost) findViewById(R.id.painelTabuladoAtividade);

            listViewFavorita = (ListView) findViewById(R.id.listViewFavorita);
            listViewNaoFavorita = (ListView) findViewById(R.id.listViewNaoFavorita);

            listaFavoritas = new ArrayList<>();
            listaNaoFavoritas = new ArrayList<>();
            //endregion

            //region Setando valores iniciais dos componentes
            setSupportActionBar(toolbar);
            painelTabulado.setup();

            //Setando as aba Tempo
            TabHost.TabSpec abaFavoritas = painelTabulado.newTabSpec("Favoritas");
            abaFavoritas.setContent(R.id.tabFavoritas);
            abaFavoritas.setIndicator("Favoritas");
            painelTabulado.addTab(abaFavoritas);

            //Setando as aba Calorias
            TabHost.TabSpec abaNaoFavoritas = painelTabulado.newTabSpec("Não-Favoritas");
            abaNaoFavoritas.setContent(R.id.tabNaoFavoritas);
            abaNaoFavoritas.setIndicator("Não-Favoritas");
            painelTabulado.addTab(abaNaoFavoritas);

            loadListViewsData();
            //endregion

            //region Setando eventos dos componentes
            listViewFavorita.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    try{
                        //region Dialogo de confirmação
                        MessageDialog.exibirDialogoQuestao(
                                ActivityAtividade.this, "Aviso", "Você deseja remover essa atividade das favoritas?")
                                .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        new DAOAtividade(ActivityAtividade.this).
                                                removerAtividadeFavorita(listaFavoritas.get(position).getCodigo());
                                        Toast.makeText(ActivityAtividade.this,
                                                listaFavoritas.get(position).getDescricao()+" foi removida das atividades favoritas",
                                                Toast.LENGTH_SHORT
                                                ).show();
                                        loadListViewsData();

                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .show();
                        //endregion
                    }catch (final Exception erro){
                        //region Mesamge para reportar Bug
                        MessageDialog.exibirDialogoErro(ActivityAtividade.this, "Erro",
                                "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                                .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        //Abre o compositor de e-mails para enviar o relatório do erro
                                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                                EmailType.RELATORIO_BUG,
                                                new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                                AppUtils.comporEmailErro(erro)),
                                                "Enviar E-mail"));
                                        finish();
                                    }
                                })
                                .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .show();
                        //endregion
                    }
                }
            });

            listViewNaoFavorita.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    //region Dialogo de confirmação
                    MessageDialog.exibirDialogoQuestao(
                            ActivityAtividade.this, "Aviso", "Você deseja tornar essa atividade favorita?")
                            .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    new DAOAtividade(ActivityAtividade.this).
                                            adicionarAtividadeFavorita(listaNaoFavoritas.get(position).getCodigo());
                                    Toast.makeText(ActivityAtividade.this,
                                            listaNaoFavoritas.get(position).getDescricao()+" foi adicionada as atividades favoritas",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                    loadListViewsData();

                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                    //endregion
                }
            });
            //endregion

        }catch (final Exception erro){
            //region Mesamge para reportar Bug
            MessageDialog.exibirDialogoErro(ActivityAtividade.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            //endregion
        }

    }

    /**
     * Carrega os dados da lista de acordo com o filtro selecionado no Spinner
     * @return
     */
    private void loadListViewsData(){
        ArrayList<PhotoListItem> listaItensFavoritas = new ArrayList<>();
        ArrayList<PhotoListItem> listaItensNaoFavoritas = new ArrayList<>();

        try{

            listaFavoritas = new DAOAtividade(ActivityAtividade.this).getListaAtividades((byte)1);
            listaNaoFavoritas = new DAOAtividade(ActivityAtividade.this).getListaAtividades((byte)-1);

            for(Atividade item: listaFavoritas){
                listaItensFavoritas.add(new PhotoListItem(item.getResourceId(), item.getDescricao(), ""));
            }

            for(Atividade item: listaNaoFavoritas){
                listaItensNaoFavoritas.add(new PhotoListItem(item.getResourceId(), item.getDescricao(), ""));
            }

            listViewFavorita.setAdapter(new AdapterPhotoListView(ActivityAtividade.this, listaItensFavoritas));
            listViewNaoFavorita.setAdapter(new AdapterPhotoListView(ActivityAtividade.this, listaItensNaoFavoritas));

        }catch(final Exception erro){

            MessageDialog.exibirDialogoErro(ActivityAtividade.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            ActivityAtividade.this.startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

        }
    }

}
