package app.nefd.teenactive.controller.utils;

import com.github.mikephil.charting.utils.ColorTemplate;

/**
 * Contém os metodos e variáveis necessários para diversificação das cores
 */
public class ColorUtils {

    public static final int[] CORES_SINTILANTES = {
            ColorTemplate.rgb("#FF6600"),
            ColorTemplate.rgb("#EF2929"),
            ColorTemplate.rgb("#4E9A06"),
            ColorTemplate.rgb("#3465A4"),
            ColorTemplate.rgb("#EDB700"),
            ColorTemplate.rgb("#75507B"),
            ColorTemplate.rgb("#8F5902"),
            ColorTemplate.rgb("#888A85"),
            ColorTemplate.rgb("#DB00FF"),
            ColorTemplate.rgb("#20998F"),
            ColorTemplate.rgb("#FF0080"),
            ColorTemplate.rgb("#8DA020")
    };

    public static final int LARANJA_SELECAO = ColorTemplate.rgb("#FFAE73");

}
