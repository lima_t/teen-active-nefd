package app.nefd.teenactive.controller.interfaces;

/**
 * Classe responsável por definir a interface para o evento da classe TimeCounter
 */
public interface OnTimeCounterPaused {

    /**
     * Ocorre quando o cronometro é pausado
     */
    void onTimeCounterPaused();
}
