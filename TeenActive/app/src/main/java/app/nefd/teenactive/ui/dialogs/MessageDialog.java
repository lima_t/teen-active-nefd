package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import app.nefd.teenactive.R;

/**
 * Classe conteém todas as funções necessárias para exibir dialogos de mensagem
 */
public class MessageDialog {

    /**
     * Cria um construtor (Builder) para complementar o AlertDialog.Builder passando um layout XML personalizado
     * @param telaContexto Tela de referência para o contexto do AlertDialog
     * @param iconeResource ID do ícone que irá aparecer no Dialog (usar R.mipmap do projeto TeenActive)
     * @param tituloDialog Titulo do dialog
     * @param mensagemDialog Mensagem do dialog
     * @return AlertDialog.Builder
     */
    private static AlertDialog.Builder create(final Context telaContexto, final int iconeResource, final CharSequence tituloDialog, final CharSequence mensagemDialog){
        LayoutInflater objLayoutInflater = LayoutInflater.from(telaContexto);
        View objTela = objLayoutInflater.inflate(R.layout.message_dialog, null);
        ((ImageView)objTela.findViewById(R.id.imgIconeDialog)).setImageResource(iconeResource);
        ((TextView)objTela.findViewById(R.id.labTituloMessageDialog)).setText(tituloDialog);

        TextView labMensagem = (TextView)objTela.findViewById(R.id.labMensagemDialog);
        labMensagem.setText(mensagemDialog);
        labMensagem.setMovementMethod(new ScrollingMovementMethod());

        return new AlertDialog.Builder(telaContexto).setView(objTela);
    }

    /**
     * Exibe um Dialogo informativo de alerta básico.
     * @param telaContexto Tela de contexto à qual o dialogo deve abrir sobre
     * @param titulo Título do dialogo
     * @param mensagem Mensagem do dialogo
     * @return AlertDialog.Builder para ser implementado com os botões e o metodo "show()"
     */
    public static AlertDialog.Builder exibirDialogoInformacao(Context telaContexto, CharSequence titulo, CharSequence mensagem){
        AlertDialog.Builder objDialogoAlerta = MessageDialog.create(telaContexto,R.mipmap.icone_information_dialog, titulo, mensagem);
        objDialogoAlerta.setCancelable(false);
        return objDialogoAlerta;
    }

    /**
     * Exibe um Dialogo de aviso básico.
     * @param telaContexto Tela de contexto à qual o dialogo deve abrir sobre
     * @param titulo Título do dialogo
     * @param mensagem Mensagem do dialogo
     * @return AlertDialog.Builder para ser implementado com os botões e o metodo "show()"
     */
    public static AlertDialog.Builder exibirDialogoAlerta(Context telaContexto, CharSequence titulo, CharSequence mensagem){
        AlertDialog.Builder objDialogoAlerta = MessageDialog.create(telaContexto,R.mipmap.icone_warning_dialog, titulo, mensagem);
        objDialogoAlerta.setCancelable(false);
        return objDialogoAlerta;
    }

    /**
     * Exibe um Dialogo de alerta para erros e exceções
     * @param telaContexto Tela de contexto à qual o dialogo deve abrir sobre
     * @param titulo Título do dialogo
     * @param mensagem Mensagem do dialogo
     * @return um AlertDialog.Builder para ser implementado com um botão com um PositiveButton e um NegativeButton e suas respectivas
     * funções ao ser clicado (o metodo SHOW() também deve ser chamado para poder exibir o Dialog)
     */
    public static AlertDialog.Builder exibirDialogoErro(Context telaContexto, CharSequence titulo, CharSequence mensagem){
        AlertDialog.Builder objDialogoAlerta = MessageDialog.create(telaContexto,R.mipmap.icone_error_dialog, titulo, mensagem);
        objDialogoAlerta.setCancelable(false);
        return objDialogoAlerta;
    }

    /**
     * Exibe um dialogo de alerta para questões em que precise de uma ação <b>positiva</b> e uma <b>negativa</b>
     * @param telaContexto Tela de contexto à qual o dialogo deve abrir sobre
     * @param titulo Título da janela
     * @param mensagem Mensagem do Dialog
     * @return AlertDialog.Builder para ser implementado com os botões de <b>Sim</b> e <b>Não</b> e o metodo "show()"
     */
    public static AlertDialog.Builder exibirDialogoQuestao(Context telaContexto, CharSequence titulo, CharSequence mensagem){
        AlertDialog.Builder objDialogoAlerta = MessageDialog.create(telaContexto,R.mipmap.icone_question_dialog, titulo, mensagem);
        objDialogoAlerta.setCancelable(false);
        return objDialogoAlerta;
    }

}
