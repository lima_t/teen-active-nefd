package app.nefd.teenactive.controller.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.services.ServicoMonitoramento;
import app.nefd.teenactive.controller.services.ServicoNotificacao;

/**
 * Classe utilitária para executar ações após o boot do celular
 */
public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Configuracao config = new Configuracao(context);
        Log.i("Teen-Active Bootup", "Entrou no onReceive, tentando startar os serviços");

        //Ele só ativa o serviço se o intervalo for > 0 (0 indica que as noticações estão desativadas)
        if(config.getTempoNotificacao() > 0){
            Intent intentServicoNotificacao = new Intent(context, ServicoNotificacao.class);
            context.startService(intentServicoNotificacao);
            Log.i("Teen-Active Bootup", "Intervalo das notificações > 0, tentando serviço deverá começar a rodar em breve...");
        }

        if(config.isModoAutonomo()){
            Intent intentServicoMonitoramento = new Intent(context, ServicoMonitoramento.class);
            context.startService(intentServicoMonitoramento);
            Log.i("Teen-Active Bootup", "O serviço de monitoramento das atividades foi iniciado");
        }
    }
}
