package app.nefd.teenactive.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import app.nefd.teenactive.BuildConfig;
import app.nefd.teenactive.R;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.itens.AdapterPhotoListView;
import app.nefd.teenactive.ui.itens.PhotoListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

public class ActivitySobre extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        //region Declaração os componentes
        ListView listaViewDesenvolvedores = (ListView) findViewById(R.id.listViewDesenvolvedores);
        Toolbar barraAcaoSobre = (Toolbar) findViewById(R.id.barraAcaoSobre);
        TextView labVersaoAplicativo = (TextView) findViewById(R.id.labVersaoAplicativo);
        Button btTutorialBasico = (Button) findViewById(R.id.btTutorial);
        //endregion

        //region Inicializando os componentes
        setSupportActionBar(barraAcaoSobre);
        labVersaoAplicativo.setText("Versão: "+ BuildConfig.VERSION_NAME);
        listaViewDesenvolvedores.setAdapter(getListaDesenvolvedores());
        //endregion

        listaViewDesenvolvedores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                EmailType.FALAR_DESENVOLVEDOR, new String[]{"tarcisio.lima.amorim@outlook.com"}, null), "Enviar para Tarcísio"));
                        break;
                    case 1:
                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                EmailType.FALAR_DESENVOLVEDOR, new String[]{"flororenataef@hotmail.com"}, null), "Enviar para Renata"));
                        break;
                    case 2:
                        startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                EmailType.FALAR_DESENVOLVEDOR, new String[]{"vinicius.damasceno@gmail.com"}, null), "Enviar para Vinicius"));
                        break;
                }
            }
        });

        btTutorialBasico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.exibirDialogoInformacao(ActivitySobre.this, "Manual do Usuário",
                        Html.fromHtml(getString(R.string.tutorial_basico)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de acação
        getMenuInflater().inflate(R.menu.menu_sobre, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.itemEnviarOpiniao:
                startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                        EmailType.ENVIAR_OPINIAO, new String[]{"tarcisio.lima.amorim@outlook.com"}, null), "Enviar Email"));
                break;
            case R.id.itemLicencasCodigo:
                MessageDialog.exibirDialogoInformacao(ActivitySobre.this, "Licenças de Terceiros",
                        Html.fromHtml(getString(R.string.licenca_terceiros)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
                break;
            case R.id.itemReportarBug:
                startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                        EmailType.ENVIAR_OPINIAO, new String[]{"tarcisio.lima.amorim@outlook.com"},
                        "Descreva como e quando ocorreu o erro (também pode ser conveniente anexar PrintScreens do erro)"), "Enviar Email"));
                break;
            case R.id.itemEula:
                MessageDialog.exibirDialogoInformacao(ActivitySobre.this, "Licença do Usuário",
                        Html.fromHtml(getString(R.string.licenca_usuario)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
                break;
            case R.id.itemPoliticaPrivacidade:
                MessageDialog.exibirDialogoInformacao(ActivitySobre.this, "Política de Privacidade",
                        Html.fromHtml(getString(R.string.politica_privacidade)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
                break;
            default:
                //Não faz nada por enquanto.
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //region Metodos personalizados
    /**
     * Monta a lista de desenvolvedores para o ListView da tela
     * @return
     */
    private AdapterPhotoListView getListaDesenvolvedores(){

        //Criando objetos
        AdapterPhotoListView adapterPhotoListView;
        ArrayList<PhotoListItem> listaItens = new ArrayList<>();
        PhotoListItem item;

        //Setando os itens e os adicionando à lista
        item = new PhotoListItem(R.mipmap.icone_email, "Tarcísio de Lima Amorim", "Email: tarcísio.lima.amorim@outlook.com");
        listaItens.add(item);
        item = new PhotoListItem(R.mipmap.icone_email, "Renata Vanessa Alves Floro", "Email: flororenataef@hotmail.com");
        listaItens.add(item);
        item = new PhotoListItem(R.mipmap.icone_email, "Vinicius de Oliveira Damasceno", "Email: vinicius.damasceno@gmail.com");
        listaItens.add(item);

        adapterPhotoListView = new AdapterPhotoListView(ActivitySobre.this, listaItens);

        return adapterPhotoListView;
    }
    //endregion
}
