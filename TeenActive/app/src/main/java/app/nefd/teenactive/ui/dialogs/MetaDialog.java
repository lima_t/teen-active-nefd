package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import app.nefd.teenactive.R;

/**
 * Classe contém todas as funções necessárias para exibir o dialogo de contexto da tela de <b>Metas Cadastradas</b>
 */

public class MetaDialog extends AlertDialog {

    //region Variáveis globais
    public Button btEditarMeta, btExcluirMeta, btFavoritarMeta;
    //endregion

    //region Construtores

    /**
     * Construtor para o dialogo de contexto da tela de <b>Metas Cadastradas</b>
     * @param context Tela de contexto
     */
    public MetaDialog(Context context) {
        super(context);

        LayoutInflater objLayoutInflater = LayoutInflater.from(context);
        View objTela = objLayoutInflater.inflate(R.layout.meta_dialog, null);

        btEditarMeta = (Button) objTela.findViewById(R.id.btEditarMeta);
        btExcluirMeta = (Button) objTela.findViewById(R.id.btExcluirMeta);
        btFavoritarMeta = (Button) objTela.findViewById(R.id.btFavoritarMeta);

        this.setCancelable(true);
        this.setView(objTela);
    }

    //endregion
}
