package app.nefd.teenactive.controller.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;

import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.dao.DAOTreino;
import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.entities.Treino;
import app.nefd.teenactive.database.entities.Configuracao;
import app.nefd.teenactive.controller.enums.Intensity;
import app.nefd.teenactive.controller.interfaces.OnTimeCounterTickListener;
import app.nefd.teenactive.controller.interfaces.StepListener;
import app.nefd.teenactive.controller.sensors.AccelDataProcessor;
import app.nefd.teenactive.controller.sensors.StepDetector;
import app.nefd.teenactive.controller.utils.CientificUtils;
import app.nefd.teenactive.controller.utils.IntensityUtils;
import app.nefd.teenactive.controller.utils.TimeCounter;


public class ServicoMonitoramento extends Service implements LocationListener{

    //region Variáveis globais
    private Treino treino;
    private StepDetector stepDetector;
    private TimeCounter counter;
    private LocationManager locationManager;

    private LatLng coordenadasA, coordenadasB;
    private short limitSeconds = -1;
    private short passosLast15seg = 0;
    private Intensity intensidadeAtividade = Intensity.SEDENTARIA;
    //endregion

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            Log.i("Teen-Active Service", "Serviço de monitoramento iniciado...");
            final IntensityUtils intensityUtils;

            //region Inicializando itens
            final Atividade atividade = new Atividade();
            final Perfil perfil = new DAOPerfil(getApplicationContext()).carregarPerfil();
            treino = new Treino();
            counter = new TimeCounter();

            intensityUtils = new IntensityUtils(atividade, CientificUtils.calcularIdadeAnos(perfil.getDataNascimento()));
            stepDetector = new StepDetector((SensorManager) getSystemService(Context.SENSOR_SERVICE));
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            configureLimiarPedometro();
            //endregion

            //region Verificações de segurança
            //É obrigatório haver a checagem da permissão em tempo real, caso não haja acesso a localização
            //É lançada uma exceção.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //Define critérios de localização e o melhor provedor (Internet ou GPS)
                Criteria bestProvider = new Criteria();
                bestProvider.setAccuracy(Criteria.ACCURACY_FINE);

                //Registra o listener para ficar capturando a localização do usuário
                locationManager.requestLocationUpdates(locationManager.getBestProvider(bestProvider, true), 0, 0, ServicoMonitoramento.this);


            } else {
                throw new Exception("As permissões de acesso a localização pelo GPS foram negadas ou desabilitadas, portanto o serviço não pode continuar.");
            }
            //endregion

            //region Eventos
            stepDetector.setOnStepListener(new StepListener() {
                @Override
                public void onStep(long eventMsecTime) {
                    treino.setQuantidadePassos((treino.getQuantidadePassos()+1));
                    passosLast15seg++;
                }
            });

            counter.setOnTimeCounterTickListener(new OnTimeCounterTickListener() {
                @Override
                public void onTimeCounterTickListener(long milliseconds, int minutes, short seconds) {
                    treino.setTempoTotal(milliseconds);

                    //Consertar isso aqui e deixar igual ao da tela de nova atividade
                    if(limitSeconds == 15){

                        intensidadeAtividade = intensityUtils.getActivityIntensityBySteps(passosLast15seg);
                        limitSeconds = 1;
                        passosLast15seg = 0;

                        //region Seta o texto da intensidade e incrementa o tempo da mesma
                        switch (intensidadeAtividade){
                            case SEDENTARIA:
                                treino.setTempoOcioso(treino.getTempoOcioso()+15000);
                                break;
                            case LEVE:
                                treino.setTempoIntensidadeLeve(treino.getTempoIntensidadeLeve()+15000);
                                break;
                            case MODERADA:
                                treino.setTempoIntensidadeModerada(treino.getTempoIntensidadeModerada()+15000);
                                break;
                            case VIGOROSA:
                                treino.setTempoIntensidadeVigorosa(treino.getTempoIntensidadeVigorosa()+15000);
                                break;
                            default:
                                //Não faz nada.
                                break;
                        }
                        //endregion

                    }else{
                        limitSeconds++;
                    }

                    //region Calcular o gasto calórico a partir da intensidade
                    switch (intensidadeAtividade){
                        case SEDENTARIA:
                            treino.setKcalConsumida(CientificUtils.calcularGastoCalorico(1.2f, 0, treino.getTempoTotal()));
                            break;
                        case LEVE:
                            treino.setKcalConsumida(CientificUtils.calcularGastoCalorico(atividade.getMetsLeve(), 0, treino.getTempoTotal()));
                            break;
                        case MODERADA:
                            treino.setKcalConsumida(CientificUtils.calcularGastoCalorico(atividade.getMetsModerado(), 0, treino.getTempoTotal()));
                            break;
                        case VIGOROSA:
                            treino.setKcalConsumida(CientificUtils.calcularGastoCalorico(atividade.getMetsIntenso(), 0, treino.getTempoTotal()));
                            break;
                        default:
                            //Não faz nada.
                            break;
                    }
                    //endregion

                }
            });
            //endregion

            treino.setDataTreino(Calendar.getInstance().getTime());
            treino.setTreinoAutonomo(true);
            treino.setTreinoPassivo(false);
            stepDetector.iniciarDetector();
            counter.start();

        }catch (Exception erro){
            Log.e("Teen-Active Service", "Ocorreu um erro no onCreate() do serviço de monitoramento: ", erro);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*Esse retorno garante que o serviço irá voltar a rodar sempre que o processo for removido da memória
        pelo sistema operacional*/
        Log.i("Teen-Active Service", "onStartCommand() foi chamado.");
        return ServicoNotificacao.START_STICKY;
    }

    @Override
    public void onDestroy() {

        try {

            //Para os contadores
            stepDetector.pararDetector();
            counter.pause();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Para o monitoramento via GPS e desregistra o listener
                locationManager.removeUpdates(ServicoMonitoramento.this);
            }

            //region Acrescenta o tempo adicional restante à respectiva intensidade
            switch (intensidadeAtividade){
                case SEDENTARIA:
                    treino.setTempoOcioso(treino.getTempoOcioso()+(treino.getTempoTotal() - treino.getTempoOcioso()));
                    break;
                case LEVE:
                    treino.setTempoIntensidadeLeve(treino.getTempoIntensidadeLeve()+(treino.getTempoTotal() - treino.getTempoIntensidadeLeve()));
                    break;
                case MODERADA:
                    treino.setTempoIntensidadeModerada(treino.getTempoIntensidadeModerada()+(treino.getTempoTotal() - treino.getTempoIntensidadeModerada()));
                    break;
                case VIGOROSA:
                    treino.setTempoIntensidadeVigorosa(treino.getTempoIntensidadeVigorosa()+(treino.getTempoTotal() - treino.getTempoIntensidadeVigorosa()));
                    break;
                default:
                    //Não faz nada.
                    break;
            }
            //endregion

            //Tenta inserir o treino
            new DAOTreino(getApplicationContext()).inserirTreino(treino);
            Log.i("Teen-Active Service", "Serviço de monitoramento salvou a atividade com sucesso!");

        }catch (Exception erro){
            Log.e("Teen-Active Service", "Ocorreu um erro no onDestroy() do serviço de monitoramento: ", erro);

        }finally {
            Log.i("Teen-Active Service", "Serviço de monitoramento finalizado!");
            super.onDestroy();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null){

            if(coordenadasA != null){
                coordenadasA = coordenadasB;
                coordenadasB = new LatLng(location.getLatitude(), location.getLongitude());

            }else{
                coordenadasA = new LatLng(location.getLatitude(), location.getLongitude());
                coordenadasB = new LatLng(location.getLatitude(), location.getLongitude());
            }

            float distancia = (float)CientificUtils.calcularDistanciaByGoogle(coordenadasA, coordenadasB);
            treino.setDistanciaPercorrida((treino.getDistanciaPercorrida()+distancia));//Vai somando as distâncias a partir dos valores
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //Aqui não faz nada por enquanto
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Não faz nada por enquanto
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Não faz nada por enquanto
    }

    /**
     * Configura o limiar de passos inicial do acelerometro
     */
    private void configureLimiarPedometro() {
        AccelDataProcessor accelDataProcessor = AccelDataProcessor.getInstance();
        Configuracao config = new Configuracao(getApplicationContext());
        accelDataProcessor.setLimiar(config.getSensibilidade());
    }
}
