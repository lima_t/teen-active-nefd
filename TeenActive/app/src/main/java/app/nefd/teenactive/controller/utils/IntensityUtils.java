package app.nefd.teenactive.controller.utils;

import android.support.annotation.NonNull;

import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.controller.enums.Intensity;

/**
 * Classe responsável pelas funções de detecção da intensidade do exercício
 */
public class IntensityUtils {

    private int passosSedentaria;
    private int passosLeve;
    private int passosModerado;

    /**
     * Cria as predefinições dos limiares  para o usuário em atividade, desta maneira sabendo os limiares
     * de passos/15seg. da respectiva atividade.
     ** @param atividade Necessária para calcular a taxa de passos/15seg. a partir do parametro dos MET's
     * @param idadeAnos Necessário para calcular a taxa de passos/15seg. de acordo com a idade do usuário.
     */
    public IntensityUtils(@NonNull Atividade atividade, short idadeAnos){

        if(atividade.isPassosAtivo()){

            int passos15seg;

            //Faz a condicional verificar qual o resultado a partir dos parametros de idade em ano
            if(idadeAnos <= 10){
                passos15seg = (128 * 15) / 60;//128 passos x 15 seg / 60 segundos [regra de 3].

            }else if(idadeAnos <= 15){
                passos15seg = (105 * 15)/60;//105 passos x 15 seg / 60 segundos [regra de 3].

            }else {
                passos15seg = (101 * 15)/60;//101 passos x 15 seg / 60 segundos [regra de 3].
            }

            passosSedentaria = (int)((passos15seg * 1.2) / 4);//Qtd passos/15seg. x Met's mínimo / por 4 Met's
            passosLeve       = (int)((passos15seg * atividade.getMetsLeve()) / 4);//Qtd passos/15seg. x Met's leve / por 4 Met's
            passosModerado   = (int)((passos15seg * atividade.getMetsModerado()) / 4);//Qtd passos/15seg. x Met's moderado / por 4 Met's

        }else{

            /*-1 porque quando o método getActivityIntensityBySteps for chamando e ele verificar se passosLast15seg
            é menor ou igual a passosSedentaria vai ser sempre falso (passosLast15seg = 0 e passosSedentaria = -1)
            sendo assim 0 <= -1 é falso e ele será obrigado a entender como se a atividade fosse leve ou moderada
            para atividades que não contam a quantidade de passos e não pode parametrizada por steps
            */
            passosSedentaria = -1;
            passosLeve       = -1;
            passosModerado   = 0;
        }

    }

    /**
     * O calculo da intensidade do exercício se dá pela equação de equivalência proporcional utilizando a regra de 3 simples,
     * onde sabe-se que, por exemplo, crianças de 6 à 10 anos, possuem uma média de <b>128 passos/min</b> segundo o estudo de
     * <b>Catrine et al (2016).</b>, proporcionalmente se diminuirmos a taxa de atualização do tempo para 15 seg. teremos
     * <b>32 passos/15seg.</b> para uma atividade moderada de <i>4 met's</i>. O menor limiar de mets de uma atividade cotidiana
     * sedentária equivale a <i>1,2 mets</i> sendo assim, aplicando mais uma vez a equação de regra de 3 simples sobre a
     * quantidade de passos temos <u>32 passos/15seg. para 4 mets => 10 passos/15seg. para 1,2 mets</u>, sendo assim, podemos
     * concluir que o mesmo conceito pode ser aplicado as demais atividades dependendo da quantidade de passos registrados.
     *
     * @param passosLast15seg passos efetuados nos últimos 15 segundos.
     * @return Intensidade do exercício a partir da classe enum {@link Intensity}
     */
    public Intensity getActivityIntensityBySteps(int passosLast15seg){

        if(passosLast15seg <= passosSedentaria){
            return Intensity.SEDENTARIA;

        }else if(passosLast15seg <= passosLeve){
            return  Intensity.LEVE;

        }else if(passosLast15seg <= passosModerado){
            return Intensity.MODERADA;

        }else{
            return Intensity.VIGOROSA;
        }
    }
}
