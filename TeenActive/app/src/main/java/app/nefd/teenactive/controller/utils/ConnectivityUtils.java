package app.nefd.teenactive.controller.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;

import app.nefd.teenactive.ui.dialogs.MessageDialog;

/**
 * Classe responsável por armazenar as funções para verificação e modificação da propriedades do smartphone
 * voltado à conectividade de redes
 */
public class ConnectivityUtils{

    /**
     * Verifica se o GPS está ativo
     * @param context Tela de referência
     * @return TRUE = Está ativo; FALSE = Está inativo
     */
    public static boolean isGpsAtivo(@NonNull final Context context){
        LocationManager provedorLocalizacao = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if(provedorLocalizacao.isProviderEnabled(LocationManager.GPS_PROVIDER)){

            return true;

        }else{

            MessageDialog.exibirDialogoQuestao(context, "GPS Inativo", "É necessário que o GPS do seu smartphone esteja ativo para obter os dados com maior precisão, caso contrário informações como a distância percorrida não serão calculadas corretamente")
                    .setPositiveButton("Ativar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent objIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(objIntent);
                        }
                    })
                    .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return false;
        }
    }

    /**
     * Verifica se o GPS está no modo de ALTA PRECISÃO
     * @param context Tela de referência
     * @return TRUE = Está no modo de alta precisão; FALSE = não está no modo de alta precisão
     */
    public static boolean isGPSModoPrecisao(@NonNull final Context context){

        if(Settings.Secure.getInt(context.getContentResolver(),
                Settings.Secure.LOCATION_MODE, 1) == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY){

            return true;

        }else{

            MessageDialog.exibirDialogoQuestao(context, "GPS Alta Precisão", "É necessário que o GPS do seu smartphone esteja no modo de ALTA PRECISÃO para obter as informações das suas atividades")
                    .setPositiveButton("Ativar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent objIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(objIntent);
                        }
                    })
                    .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return false;

        }
    }

    /**
     * Verifica se o telefone se encontra no modo avião
     * @param context Tela de referência
     * @return TRUE = Modo Avião está Avito; FALSE = Modo Avião não está Ativo
     */
    public static boolean isModoAviaoAtivo(@NonNull final Context context){

        if(Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1){

            MessageDialog.exibirDialogoQuestao(context, "Modo Avião", "Desative o MODO AVIÃO para que o aplicativo possa recuperar informações através da conexão de rede e GPS")
                    .setPositiveButton("Desativar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent objIntent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                            context.startActivity(objIntent);
                        }
                    })
                    .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return true;

        }else{

            return false;
        }
    }

    /**
     * Verifica se o smartphone possui uma conexão com a rede (Obs: Este metodo verifica apenas se existe uma rede conectada
     * e não se a conectividade com a internet ou um servidor da web especifico)
     * @param context
     * @return TRUE = Há uma conexão ativa; FALSE = Não há uma conexão ativa
     */
    public static boolean isConnectedNetwork(@NonNull final Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){

            return true;

        }else{
            MessageDialog.exibirDialogoQuestao(context, "Rede Desconectada", "É necessário estabelecer uma conexão WiFi ou de dados móveis para que o aplicativo possa obter informações sobre sua localização com mais precisão, caso contrário dados como a rota percorrida não serão carregadas na interface.")
                    .setPositiveButton("Conectar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent objIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            context.startActivity(objIntent);
                        }
                    })
                    .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
            return false;
        }

    }

}
