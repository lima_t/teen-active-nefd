package app.nefd.teenactive.database.vdo;

import android.content.Context;

import java.util.Calendar;

import app.nefd.teenactive.database.dao.DAOMedidas;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.entities.Medidas;
import app.nefd.teenactive.database.entities.Perfil;

/**
 * Classe responsável por armazenar os atributos, construtores e metodos responsáveis por manipular as
 * pré-operações de validação do perfil no banco de dados
 */
public class VDOPerfil {

    //region Variáveis globais
    private Context telaContexto;
    //endregion

    //region Construtores da classe
    public VDOPerfil(Context telaContexto) {
        this.telaContexto = telaContexto;
    }

    //endregion

    /**
     * Valida os valores do objeto Perfil para inserir no banco de dados
     * @param perfil
     * @throws Exception
     */
    public void inserirPerfil(Perfil perfil) throws Exception{

        DAOPerfil objDAOPerfil = new DAOPerfil(telaContexto);
        DAOMedidas objDAOMedidas = new DAOMedidas(telaContexto);

        if(perfil.getNome().equals(null) || perfil.getNome().isEmpty()){
            throw new Exception("O nome é obrigatório");
        }

        if(perfil.getPeso() > 999 || perfil.getPeso() < 1 ){
            throw new Exception("O peso está inválido, o seu valor deve estar entre 1 e 999 kg");
        }

        objDAOPerfil.inserirPerfil(perfil);
        objDAOMedidas.inserirMedidas(new Medidas(perfil.getAltura(), perfil.getPeso(), Calendar.getInstance().getTime()));//Trigger para peso
    }

    /**
     * Valida os valores do objeto Perfil para alterar no banco de dados
     * @param perfil
     * @throws Exception
     */
    public void alterarPerfil(Perfil perfil) throws Exception{

        DAOPerfil objDAOPerfil = new DAOPerfil(telaContexto);
        DAOMedidas objDAOMedidas = new DAOMedidas(telaContexto);
        Medidas medidas;

        if(perfil.getNome() == null || perfil.getNome().isEmpty()){
            throw new Exception("O nome é obrigatório");
        }

        if(perfil.getAltura() > 300 || perfil.getAltura() < 1){
            throw new Exception("A altura está inválida, o seu valor deve estar entre 1 e 300 cm");
        }

        if(perfil.getPeso() > 999 || perfil.getPeso() < 1 ){
            throw new Exception("O peso está inválido, o seu valor deve estar entre 1 e 999 kg");
        }


        objDAOPerfil.alterarPerfil(perfil);//Insere o perfil
        medidas = new DAOMedidas(telaContexto).getMedidasHoje();

        if(medidas.getCodigo() > 0){
            objDAOMedidas.atualizarMedidas(new Medidas(medidas.getCodigo(), perfil.getAltura(), perfil.getPeso(), Calendar.getInstance().getTime()));//Trigger para peso

        }else{
            objDAOMedidas.inserirMedidas(new Medidas(perfil.getAltura(), perfil.getPeso(), Calendar.getInstance().getTime()));//Trigger para peso
        }

    }

}