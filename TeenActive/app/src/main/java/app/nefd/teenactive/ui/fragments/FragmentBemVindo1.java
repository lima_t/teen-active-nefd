package app.nefd.teenactive.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import app.nefd.teenactive.R;
import app.nefd.teenactive.ui.dialogs.MessageDialog;

/**
 *Classe de fragmento responsável por montar a 1ª tela do Bem vindo!
 */
public class FragmentBemVindo1 extends Fragment {

    public Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bem_vindo1, container, false);

        Button btEula = (Button) view.findViewById(R.id.btEula);
        Button btPoliticaPrivacidade = (Button) view.findViewById(R.id.btPoliticaPrivacidade);
        RadioButton radConcordoTermos = (RadioButton) view.findViewById(R.id.radConcordoTermos);

        //region Evento dos componentes
        btEula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.exibirDialogoInformacao(context, "Licença do Usuário",
                        Html.fromHtml(getString(R.string.licenca_usuario)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
            }
        });

        btPoliticaPrivacidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.exibirDialogoInformacao(context, "Política de Privacidade",
                        Html.fromHtml(getString(R.string.politica_privacidade)))
                        .setNeutralButton("OK ENTENDI!", null)
                        .show();
            }
        });

        radConcordoTermos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FragmentBemVindo3.isAceptedLicense = isChecked;
            }
        });
        // endregion

        return view;
    }
}
