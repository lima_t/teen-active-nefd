package app.nefd.teenactive.controller.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import app.nefd.teenactive.controller.enums.EmailType;

//Provê uma diversidade de metodos para várias funções não agrupavéis do aplicativo
public class AppUtils{

    //Strings
    public static final String APP_PROFILE_DIRECTORY = Environment.getExternalStorageDirectory()+"/TeenActive/Perfil/";
    public static final String APP_CSV_DIRECTORY = Environment.getExternalStorageDirectory()+"/TeenActive/Relatorios/";
    public static final String EMAIL_PARA_REPORTAR_BUG = "tarcisio.lima.amorim@outlook.com";
    public static final long CLICK_VIBRATION = 30;
    public static final Locale pt_BR = new Locale("pt", "BR");

    /**
     * Cria um diretório se ele não existir e se for um URi válido
     *
     * @param directoryToCreate Diretório a ser criado
     * @throws IOException
     */
    private static void criarDiretorio(String directoryToCreate)throws IOException{

        File diretorio = new File(directoryToCreate);
        if(!diretorio.exists()){
            if(!diretorio.mkdirs()){
                throw new IOException(String.format("Não foi possível cria o diretório: %s\n\nVerifique se o aplicativo possui as devidas permissões", directoryToCreate));
            }
        }
    }

    /**
     * Copia um arquivo especificado do caminho de origem para o caminho de destino (A copia por este tipo de metodo
     * é relativamente mais rápida no entanto possui uma limitação de copiar arquivos com até <b>2gb de tamanho</b>).
     * Caso seja especificado um arquivo existente no destino o mesmo automáticamete é <b>sobrescrito sem confirmação</b>
     *
     * @param origem String do caminho do arquivo de origem
     * @param destino String com o caminho do arquivo de destino
     * @throws IOException
     */
    public static void copiarArquivo(String origem, String destino) throws IOException {

        File arquivoOrigem = new File(origem);
        File arquivoDestino = new File(destino);

        criarDiretorio(APP_PROFILE_DIRECTORY);//tenta criar o diretório

        //Cria os objetos de transferência de bytes
        FileInputStream inStream = new FileInputStream(arquivoOrigem);
        FileOutputStream outStream = new FileOutputStream(arquivoDestino);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();

        inChannel.transferTo(0, inChannel.size(), outChannel);//Faz a cópia

        //Fecha os canais de transferencia de bytes
        inStream.close();
        outStream.close();
    }

    /**
     * Cria um arquivo CSV no cartão de memória e salva os dados de uma consulta advindos de uma <b>StringBuilder</b>.
     *
     * @param data Conteúdo dos dados à serem exportados
     * @throws IOException
     */
    public static void exportarDadosCsv(@NonNull String nomeArquivo, @NonNull StringBuilder data) throws IOException {

        nomeArquivo += String.format(" %s.csv", new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(Calendar.getInstance().getTime()));
        File arquivoCsv = new File(APP_CSV_DIRECTORY+nomeArquivo);

        criarDiretorio(APP_CSV_DIRECTORY);

        if(!arquivoCsv.getAbsoluteFile().exists()){
            arquivoCsv.getAbsoluteFile().createNewFile();
            arquivoCsv.getAbsoluteFile().setWritable(true);
            arquivoCsv.getAbsoluteFile().setReadable(true);
        }

        FileWriter fluxoEscrita = new FileWriter(arquivoCsv);
        fluxoEscrita.write(data.toString());
        fluxoEscrita.flush();
        fluxoEscrita.close();
    }

    /**
     * Prepara uma Intent para o envio de um e-mail onde é retornado um objeto pronto para ser chamado utilizando
     * a expressão <i>startActivity(Intent.createChooser(AppUtils.enviarEmail(new String[]{"e-mails"}, "assunto", "mensagem"), "Enviar E-mail"));</i>
     * @param destinos Endereço(s) de e-mail(s) à qual será(ão) enviada(s) a(s) mensagem(ns)
     * @param type Tipo da mensagem
     * @param mensagem Corpo da mensagem
     * @return
     */
    public static Intent emailParaEnviar(EmailType type, String[]destinos, String mensagem){

        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));//Seta o protocolo
        emailIntent.setType("text/plain");//Seta a formatação do e-mail como texto normal
        emailIntent.putExtra(Intent.EXTRA_EMAIL, destinos);//Indica o endereço de entrega

        if(type == EmailType.ENVIAR_OPINIAO){
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[Teen Active] Sugestão de usuário");//Indica o assunto da mensagem

        }else if(type == EmailType.FALAR_DESENVOLVEDOR){
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[Teen Active] Um usuário deseja falar com você");//Indica o assunto da mensagem

        }else{
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[Teen Active] Relatório de bug: @"+ Build.USER);//Indica o assunto da mensagem
        }

        emailIntent.putExtra(Intent.EXTRA_TEXT, mensagem);//Indica o corpo da mensagem

        return emailIntent;
    }

    /**
     * Compõe uma mensagem de Bug Report com todas as informações necessárias
     * @param erro
     * @return
     */
    public static String comporEmailErro(@NonNull Exception erro){
        StringBuilder objStringBuilder = new StringBuilder();
        StringWriter objStringWriter = new StringWriter();
        PrintWriter objPrintWriter = new PrintWriter(objStringWriter);

        //Pegando o StackTrace
        erro.printStackTrace(objPrintWriter);

        //Compondo mensagem
        objStringBuilder.append("[INFORMAÇÕES DO DISPOSITIVO]:");
        objStringBuilder.append("\nModelo:______________ "+ Build.MODEL);

        //Pegando a versão do android
        switch (Build.VERSION.SDK_INT){
            case 19:
                objStringBuilder.append("\nVersão do Android:___ 4.4 (Kitkat)");
                break;
            case 20:
                objStringBuilder.append("\nVersão do Android:___ 4.4 (Kitkat Wear)");
                break;
            case 21:
                objStringBuilder.append("\nVersão do Android:___ 5.0 (Lollipop)");
                break;
            case 22:
                objStringBuilder.append("\nVersão do Android:___ 5.1 (Lollipop r1)");
                break;
            case 23:
                objStringBuilder.append("\nVersão do Android:___ 6.0 (Marshmallow)");
                break;
            case 24:
                objStringBuilder.append("\nVersão do Android:___ 7.0 (Nougat)");
                break;
            case 25:
                objStringBuilder.append("\nVersão do Android:___ 7.1 (Nougat #1)");
                break;
            default:
                objStringBuilder.append("\nVersão do Android:___ Desconhecida");
                break;
        }

        objStringBuilder.append("\nFabricante:__________ "+ Build.MANUFACTURER);
        objStringBuilder.append("\nNome do Produto:_____ "+ Build.PRODUCT);
        objStringBuilder.append("\nUsuário:_____________ "+ Build.USER);
        objStringBuilder.append("\nID do Dispositivo:___ "+ Build.ID);
        objStringBuilder.append("\n\n[LOG DO ERRO]:\n");
        objStringBuilder.append(objStringWriter.toString());

        return objStringBuilder.toString();
    }

    /**
     * Formata o tempo de milisegundos para horas <b>HH:mm:ss</b>
     * @param millis
     * @return
     */
    public static String formatarMillisToHoras(long millis){
        return String.format(AppUtils.pt_BR, "%02d:%02d:%02d",
                (int)(millis/3600000),//Horas
                (int)(millis - (millis/3600000)*3600000)/60000,//Minutos
                (int)(millis - (millis/3600000)*3600000- ((millis - (millis/3600000)*3600000)/60000)*60000)/1000//Segundos
        );
    }

    /**
     * Converte o tempo de milisegundos para o formato inteiro em minutos ex: 30 min, 54 min, etc.
     * @param millis
     * @return
     */
    public static short converterMillisToMinutos(long millis){
        return (short)((millis - (millis/3600000)*3600000)/60000);
    }

    /**
     * Converte o tempo de milisegundos para o formato decimal em minutos ex: 1,5 min = 1:30
     * @param millis
     * @return
     */
    public static float converterMillisToFloat(long millis){
        return ((millis - (millis/3600000)*3600000)/60000f);
    }

    /**
     * Converte o tempo de minutos para o formato de milissegundos
     * @param minutos
     * @return
     */
    public static long converterMinutosToMillis(int minutos){
        return (long)(minutos * 60_000);//1 seg = 1000 então 60 seg = 60.000
    }

}
