package app.nefd.teenactive.ui.itens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import app.nefd.teenactive.R;

/**
 * Provém os metodos e atributos necessários para montar um adapter de uma ListView baseado no modelo personalizado do projeto
 */
public class AdapterPhotoListView extends BaseAdapter {

    //region Atributos
    private LayoutInflater mInflater;
    private ArrayList<PhotoListItem> itens;
    //endregion

    //region Construtores
    public AdapterPhotoListView(Context context, ArrayList<PhotoListItem> itens)
    {
        this.itens = itens;//Itens que preencheram o listview
        mInflater = LayoutInflater.from(context);//Responsável por pegar o Layout do item.
    }

    //endregion

    //region Metodos abstratos que foram implementados devido a herança
    /**
     * Retorna a quantidade de itens da lista
     * @return
     */
    @Override
    public int getCount() {
        return itens.size();
    }

    /**
     * Retorna a posição (index) do item selecionado
     * @param posicao
     * @return
     */
    @Override
    public Object getItem(int posicao) {
        return itens.get(posicao);
    }

    /**
     * retorna o ID do item na posição (Metodo sem necessidade de implementação)
     * @param posicao
     * @return
     */
    @Override
    public long getItemId(int posicao) {
        return posicao;
    }

    /**
     * Adapta o XML do layout do item da lista customizada para o adapter
     * @param posicao
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {
        //Pega o item de acordo com a posição.
        PhotoListItem item = itens.get(posicao);
        //infla o layout para podermos preencher os dados
        convertView = mInflater.inflate(R.layout.photo_list_item, null);

        //atravez do layout pego pelo LayoutInflater, pegamos cada id relacionado
        //ao item e definimos as informações.
        ((CircularImageView) convertView.findViewById(R.id.imgPhotoListItem)).setImageResource(item.getImagemResourveId());
        ((TextView) convertView.findViewById(R.id.labPhotoListItem1)).setText(item.getTextoPrimario());
        ((TextView) convertView.findViewById(R.id.labPhotoListItem2)).setText(item.getTextoSecundario());

        return convertView;
    }
}
