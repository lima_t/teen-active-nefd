package app.nefd.teenactive.database.vdo;

import android.content.Context;

import app.nefd.teenactive.database.dao.DAOMedidas;
import app.nefd.teenactive.database.entities.Medidas;

/**
 * lasse responsável por armazenar os atributos, construtores e metodos responsáveis por manipular as
 * pré-operações de validação do registro de peso no banco de dados
 */

public class VDOMedidas {

    //region Variáveis globais
    private Context telaContexto;
    //endregion

    //region Construtores da classe
    public VDOMedidas(Context telaContexto) {
        this.telaContexto = telaContexto;
    }

    //endregion

    /**
     * Valida os valores do registro de medidas antes de inserir ou alterar no banco de dados
     * @param medidas Objeto do tipo Medidas com os valores à serem validados
     * @throws Exception gerada caso os argumentos estejam inválidos para a inserção
     */
    public void inserirMedidas(Medidas medidas) throws Exception{

        DAOMedidas objDAOMedidas = new DAOMedidas(telaContexto);//Passa o contexto

        if(medidas.getPeso() <= 0 || medidas.getPeso() > 999){
            throw new IllegalArgumentException("O peso está inválido, seu valor deve estar entre 1 e 999 kg");
        }

        if(medidas.getAltura() < 1 || medidas.getAltura() > 300){
            throw new IllegalArgumentException("A altura está inválida, seu valor deve estar entre 1 e 300 cm");
        }

        if(medidas.getCodigo() > 0){
            objDAOMedidas.atualizarMedidas(medidas);//tenta atualizar os dados
        }else{
            objDAOMedidas.inserirMedidas(medidas);//tenta inserir no DAL
        }
    }
}