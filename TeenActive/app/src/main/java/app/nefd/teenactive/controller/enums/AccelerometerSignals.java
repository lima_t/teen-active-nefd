package app.nefd.teenactive.controller.enums;

/**
 * Created by tarcisio on 21/04/17.
 */

public enum AccelerometerSignals {

    MAGNITUDE, MOV_AVERAGE;

    /**
     * Quantida de itens contido na enumeração
     */
    public static final int count = AccelerometerSignals.values().length;
}
