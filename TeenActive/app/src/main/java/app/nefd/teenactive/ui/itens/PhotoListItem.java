package app.nefd.teenactive.ui.itens;

/**
 * Provê um objeto para um item de lista customizado em uma ListView com uma imagem e dois text Views
 */
public class PhotoListItem {

    //region Atributos
    private int imagemResourveId = 0;
    private String textoPrimario = "";
    private String textoSecundario = "";
    //endregion

    //region Construtores

    /**
     * Construtor default
     */
    public PhotoListItem(){}

    /**
     * Construtor sem o ícone no item da lista
     * @param textoPrimario
     * @param textoSecundario
     */
    public PhotoListItem(String textoPrimario, String textoSecundario) {
        this.textoPrimario = textoPrimario;
        this.textoSecundario = textoSecundario;
    }

    /**
     * Construtor completo
     * @param imagemResourveId
     * @param textoPrimario
     * @param textoSecundario
     */
    public PhotoListItem(int imagemResourveId, String textoPrimario, String textoSecundario) {
        this.imagemResourveId = imagemResourveId;
        this.textoPrimario = textoPrimario;
        this.textoSecundario = textoSecundario;
    }
    //endregion

    //region Getters e Setters

    public int getImagemResourveId() {
        return imagemResourveId;
    }

    public void setImagemResourveId( int imagemResourveId) {
        this.imagemResourveId = imagemResourveId;
    }

    public String getTextoPrimario() {
        return textoPrimario;
    }

    public void setTextoPrimario(String textoPrimario) {
        this.textoPrimario = textoPrimario;
    }

    public String getTextoSecundario() {
        return textoSecundario;
    }

    public void setTextoSecundario(String textoSecundario) {
        this.textoSecundario = textoSecundario;
    }
    //endregion
}
