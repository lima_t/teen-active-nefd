package app.nefd.teenactive.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOAtividade;
import app.nefd.teenactive.database.entities.Atividade;
import app.nefd.teenactive.ui.itens.AdapterPhotoListView;
import app.nefd.teenactive.ui.itens.PhotoListItem;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;

/**
 * Classe contém todos os atributos e métodos para criar dialogo seletor de atividades
 */
public class TaskDialog extends AlertDialog {

    //region Variáveis globais
    private Spinner spinnerTipoAtividade;
    private Context context;

    public ListView listViewAtividades; //Deve ter o OnItemClickListener implementado por quem chamar o dialog
    public ArrayList<Atividade> listaAtividades;
    //endregion

    //region Construtores

    /**
     * Construtor padrão do Dialog
     * @param context Tela para referência
     */
    public TaskDialog(Context context) {
        super(context);

        //region Iniciando os componentes
        LayoutInflater objLayoutInflater = LayoutInflater.from(context);
        View view = objLayoutInflater.inflate(R.layout.task_dialog, null);
        spinnerTipoAtividade = (Spinner) view.findViewById(R.id.spinnerTipoAtividade);
        listViewAtividades = (ListView) view.findViewById(R.id.listViewAtividades);
        //endregion

        //region Setando os valores dos componentes
        listViewAtividades.setAdapter(setListViewData());
        //endregion

        //region Definindo os eventos
        spinnerTipoAtividade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listViewAtividades.setAdapter(setListViewData());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Não faz nada
            }
        });
        //endregion

        this.setButton(Dialog.BUTTON_NEGATIVE, "Cancelar", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        this.context = context;
        this.setCancelable(true);
        this.setView(view);
    }
    //endregion

    /**
     * Carrega os dados da lista de acordo com o filtro selecionado no Spinner
     * @return
     */
    private AdapterPhotoListView setListViewData(){

        AdapterPhotoListView adapterPhotoListView = null;
        ArrayList<PhotoListItem> listaItens = new ArrayList<>();

        try{

            listaAtividades = new DAOAtividade(context).getListaAtividades((byte)spinnerTipoAtividade.getSelectedItemPosition());

            if(!listaAtividades.isEmpty()){

                for(Atividade item: listaAtividades){
                    listaItens.add(new PhotoListItem(
                            item.getResourceId(),
                            item.getDescricao(),
                            "Gasto energético: "+item.getMetsModerado()+" met's"
                    ));
                }

            }else{
                Toast.makeText(context, "Ainda não existe atividades nessa categoria.", Toast.LENGTH_SHORT).show();
            }

            adapterPhotoListView = new AdapterPhotoListView(context, listaItens);

        }catch(final Exception erro){

            MessageDialog.exibirDialogoErro(context, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            context.startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            this.dismiss();

        }finally {
            return adapterPhotoListView;
        }
    }
}
