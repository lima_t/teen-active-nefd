package app.nefd.teenactive.database.entities;

import java.util.Date;

/**
 * Classe que representa a entidade PERFIL do banco de dados
 */
public class Perfil {

    //region Atributos
    private int codigo = 0;
    private String fotoPerfil = null;
    private String nome = null;
    private Date dataNascimento = null;
    private short sexo = 0;
    private short altura = 0;
    private float peso = 0.000f;
    //endregion

    //region Getters e Setters
    /**
     * Retorna o código do objeto perfil
     * @return codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Seta o código no objeto perfil
     * @param codigo
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Retorna a imagem do perfil do objeto perfil
     * @return fotoPerfil
     */
    public String getFotoPerfil() {
        return fotoPerfil;
    }

    /**
     * Seta a foto do perfil no objeto perfil
     * @param fotoPerfil
     */
    public void setFotoPerfil(String fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    /**
     * Retorna o nome do objeto perfil
     * @return nome
     */
    public String getNome() { return nome; }

    /**
     * Seta o nome do objeto perfil
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Retorna a data de nascimento do objeto perfil
     * @return dataNascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Seta a data de nascimento no objeto perfil
     * @param dataNascimento
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * Retorna sexo do objeto perfil <b>0 = Masculino, 1 = Feminino</b>
     * @return sexo
     */
    public short getSexo() {
        return sexo;
    }

    /**
     * Seta o sexo no objeto perfil <b>0 = Masculino, 1 = Feminino</b>
     * @param sexo
     */
    public void setSexo(short sexo) {
        this.sexo = sexo;
    }

    /**
     * Retorna a altura do objeto perfil
     * @return altura em centímetros
     */
    public short getAltura() {
        return altura;
    }

    /**
     * Seta a altura no objeto perfil
     * @param altura
     */
    public void setAltura(short altura) {
        this.altura = altura;
    }

    /**
     * Retorna o peso do objeto perfil
     * @return peso em kg
     */
    public float getPeso() {
        return peso;
    }

    /**
     * Seta o peso no objeto perfil
     * @param peso
     */
    public void setPeso(float peso) {
        this.peso = peso;
    }
    //endregion
}
