package app.nefd.teenactive.ui.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.Calendar;

import app.nefd.teenactive.R;
import app.nefd.teenactive.database.dao.DAOMedidas;
import app.nefd.teenactive.database.dao.DAOPerfil;
import app.nefd.teenactive.database.entities.Medidas;
import app.nefd.teenactive.database.entities.Perfil;
import app.nefd.teenactive.database.vdo.VDOMedidas;
import app.nefd.teenactive.ui.dialogs.DecimalPickerDialog;
import app.nefd.teenactive.ui.dialogs.MessageDialog;
import app.nefd.teenactive.ui.dialogs.NumberPickerDialog;
import app.nefd.teenactive.controller.enums.EmailType;
import app.nefd.teenactive.controller.utils.AppUtils;
import app.nefd.teenactive.controller.utils.CientificUtils;

public class ActivityMedidas extends AppCompatActivity {

    //region Variáveis globais
    private EditText txtNovoPeso, txtNovaAltura;
    private VDOMedidas objVDOMedidas;
    private Medidas objMedidas;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medidas);

        try{

            //region Vinculando componentes ao XML
            Toolbar barraAcaoPeso = (Toolbar) findViewById(R.id.barraAcaoPeso);
            TextView labNomeUsuario = (TextView) findViewById(R.id.labNomeAtividade);
            TextView labSexoUsuario = (TextView) findViewById(R.id.labSexoUsuario);
            TextView labIdadeUsuario = (TextView) findViewById(R.id.labIdadeUsuario);
            TextView labAlturaUsuario = (TextView) findViewById(R.id.labAlturaUsuario);
            TextView labPesoUsuario = (TextView) findViewById(R.id.labPesoUsuario);
            CircularImageView imgFotoPerfil = (CircularImageView) findViewById(R.id.imgFotoPerfil);

            txtNovaAltura = (EditText) findViewById(R.id.txtNovaAltura);
            txtNovoPeso = (EditText) findViewById(R.id.txtNovoPeso);

            Perfil perfil = new DAOPerfil(ActivityMedidas.this).carregarPerfil();

            objVDOMedidas = new VDOMedidas(ActivityMedidas.this);//Passando contexto para poder inserir depois no banco
            objMedidas = new DAOMedidas(ActivityMedidas.this).getMedidasHoje();
            //endregion

            //region Setando os valores iniciais dos componentes
            setSupportActionBar(barraAcaoPeso);

            labNomeUsuario.setText("Dados Atuais de "+perfil.getNome());
            labSexoUsuario.setText(perfil.getSexo()==0?"Masculino":"Feminino");
            labIdadeUsuario.setText(CientificUtils.calcularIdadeAnos(perfil.getDataNascimento())+" anos");
            labAlturaUsuario.setText(perfil.getAltura()+" cm");
            labPesoUsuario.setText(perfil.getPeso()+" kg");

            if(perfil.getFotoPerfil() != null && !perfil.getFotoPerfil().isEmpty()) {

                if (new File(AppUtils.APP_PROFILE_DIRECTORY + "foto_perfil.jpg").exists()) {
                    imgFotoPerfil.setImageBitmap(BitmapFactory.decodeFile(perfil.getFotoPerfil()));
                } else {
                    imgFotoPerfil.setImageResource(R.mipmap.icone_imagem_perfil);
                    Toast.makeText(ActivityMedidas.this, "Não foi possível carregar a foto do perfil (arquivo inexistente).", Toast.LENGTH_LONG).show();
                }
            }

            if(objMedidas.getCodigo() > 0){
                txtNovaAltura.setText(String.valueOf(objMedidas.getAltura()));
                txtNovoPeso.setText(String.valueOf(objMedidas.getPeso()));
            }

            //endregion

        }catch(final Exception erro){
            MessageDialog.exibirDialogoErro(ActivityMedidas.this, "Erro",
                    "Ocorreu um erro enquanto as informações estavam sendo carregadas.\n[Erro]:\n\n"+erro.getMessage())
                    .setPositiveButton("Reportar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Abre o compositor de e-mails para enviar o relatório do erro
                            startActivity(Intent.createChooser(AppUtils.emailParaEnviar(
                                    EmailType.RELATORIO_BUG,
                                    new String[]{AppUtils.EMAIL_PARA_REPORTAR_BUG},
                                    AppUtils.comporEmailErro(erro)),
                                    "Enviar E-mail"));
                            finish();
                        }
                    })
                    .setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }

        //region Eventos dos componentes
        txtNovoPeso.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    chamarDecimalPickerDialog();
                }
            }
        });

        txtNovaAltura.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    chamarNumberPickerDialog();
                }
            }
        });

        txtNovaAltura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarNumberPickerDialog();
            }
        });
        txtNovoPeso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarDecimalPickerDialog();
            }
        });
        //endregion
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Cria o menu na barra de ação
        getMenuInflater().inflate(R.menu.menu_peso, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try{

            if(item.getItemId() == R.id.itemSalvarRegistroPeso){
                //Preenchendo objetos com os valores
                objMedidas.setAltura(Short.parseShort(txtNovaAltura.getText().toString()));
                objMedidas.setPeso(Float.parseFloat(txtNovoPeso.getText().toString()));//Passa o peso
                objMedidas.setDataRegistro(Calendar.getInstance().getTime());//Passa a data atual

                objVDOMedidas.inserirMedidas(objMedidas);

                MessageDialog.exibirDialogoInformacao(ActivityMedidas.this, "Sucesso", "As novas medidas foram registradas com sucesso!")
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }

        }catch(Exception erro){
            MessageDialog.exibirDialogoErro(ActivityMedidas.this, "Erro", erro.getMessage()).setNeutralButton("Ok", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Exibe um dialogo para seleção de números decimais para o peso
     */
    private void chamarDecimalPickerDialog(){
        final DecimalPickerDialog objDecimalPickerDialog = new DecimalPickerDialog(ActivityMedidas.this, "Medidas (Kg)", 300);
        objDecimalPickerDialog.seletorNumericoMajor.setValue(objMedidas.getPeso()>0?(int)objMedidas.getPeso():1);

        objDecimalPickerDialog.setButton(DecimalPickerDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDecimalPickerDialog.seletorNumericoMajor.clearFocus();
                objDecimalPickerDialog.seletorNumericoMinor.clearFocus();
                txtNovoPeso.setText(
                        objDecimalPickerDialog.seletorNumericoMajor.getValue()+"."+objDecimalPickerDialog.seletorNumericoMinor.getValue());
                objDecimalPickerDialog.dismiss();
            }
        });

        objDecimalPickerDialog.setButton(DecimalPickerDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objDecimalPickerDialog.cancel();
            }
        });

        objDecimalPickerDialog.show();
    }

    /**
     * Exibe um dialogo para seleção de números para a altura
     */
    private void chamarNumberPickerDialog(){
        final NumberPickerDialog objNumberPickerDialog = new NumberPickerDialog(ActivityMedidas.this, "Altura (Cm)", 1, 300);
        objNumberPickerDialog.seletorNumerico.setValue(objMedidas.getAltura() > 0?objMedidas.getAltura():100);//Setando valor inicial

        objNumberPickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.seletorNumerico.clearFocus();
                txtNovaAltura.setText(String.valueOf(objNumberPickerDialog.seletorNumerico.getValue()));
                objNumberPickerDialog.dismiss();
            }
        });
        objNumberPickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                objNumberPickerDialog.cancel();
            }
        });

        objNumberPickerDialog.show();
    }
}
