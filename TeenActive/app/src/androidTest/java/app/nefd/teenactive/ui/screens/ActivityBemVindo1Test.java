package app.nefd.teenactive.ui.screens;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import app.nefd.teenactive.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ActivityBemVindo1Test {

    @Rule
    public ActivityTestRule<ActivityBemVindo> mActivityTestRule = new ActivityTestRule<>(ActivityBemVindo.class);

    @Test
    public void activityBemVindo1Test() {
        ViewInteraction appCompatRadioButton = onView(
                allOf(withId(R.id.radDiscordoTermos), withText("Discordo dos termos de uso"), isDisplayed()));
        appCompatRadioButton.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btDarPermissoes), withText("Conceder Permissões"), isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.btDarPermissoes), withText("Conceder Permissões"), isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.btCriarPerfil), withText("Criar meu Perfil"), isDisplayed()));
        appCompatButton3.perform(click());

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(android.R.id.button3), withText("OK ENTENDI"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton4.perform(click());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.btEula), withText("Licença de Uso"), isDisplayed()));
        appCompatButton5.perform(click());

        ViewInteraction appCompatButton6 = onView(
                allOf(withId(android.R.id.button3), withText("OK ENTENDI!"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton6.perform(click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.btPoliticaPrivacidade), withText("Política de Privacidade"), isDisplayed()));
        appCompatButton7.perform(click());

        ViewInteraction appCompatButton8 = onView(
                allOf(withId(android.R.id.button3), withText("OK ENTENDI!"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton8.perform(click());

        ViewInteraction appCompatRadioButton2 = onView(
                allOf(withId(R.id.radConcordoTermos), withText("Estou ciente e aceito os termos de uso"), isDisplayed()));
        appCompatRadioButton2.perform(click());

        ViewInteraction appCompatButton9 = onView(
                allOf(withId(R.id.btCriarPerfil), withText("Criar meu Perfil"), isDisplayed()));
        appCompatButton9.perform(click());

        ViewInteraction appCompatEditText = onView(
                withId(R.id.txtNome));
        appCompatEditText.perform(scrollTo(), replaceText("Tarcisio Lima"), closeSoftKeyboard());

        ViewInteraction appCompatSpinner = onView(
                withId(R.id.spinnerSexo));
        appCompatSpinner.perform(scrollTo(), click());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(android.R.id.text1), withText("Masculino"), isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatButton10 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton10.perform(click());

        ViewInteraction numberPicker = onView(
                withId(R.id.seletorNumericoDialog));
        numberPicker.perform(scrollTo(), longClick());

        ViewInteraction customEditText = onView(
                allOf(withClassName(is("android.widget.NumberPicker$CustomEditText")), withText("38"),
                        withParent(withId(R.id.seletorNumericoDialog))));
        customEditText.perform(scrollTo(), replaceText("166"), closeSoftKeyboard());

        ViewInteraction appCompatButton11 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton11.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txtAltura), withText("38")));
        appCompatEditText2.perform(scrollTo(), click());

        ViewInteraction customEditText2 = onView(
                allOf(withClassName(is("android.widget.NumberPicker$CustomEditText")), withText("1"),
                        withParent(withId(R.id.seletorNumericoDialog))));
        customEditText2.perform(scrollTo(), replaceText("166"), closeSoftKeyboard());

        ViewInteraction appCompatButton12 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton12.perform(click());

        ViewInteraction customEditText3 = onView(
                allOf(withClassName(is("android.widget.NumberPicker$CustomEditText")), withText("1"),
                        withParent(withId(R.id.seletorNumericoMajor))));
        customEditText3.perform(scrollTo(), replaceText("58"), closeSoftKeyboard());

        ViewInteraction customEditText4 = onView(
                allOf(withClassName(is("android.widget.NumberPicker$CustomEditText")), withText("0"),
                        withParent(withId(R.id.seletorNumericoMinor))));
        customEditText4.perform(scrollTo(), replaceText("7"), closeSoftKeyboard());

        ViewInteraction appCompatButton13 = onView(
                allOf(withId(android.R.id.button1), withText("Ok"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton13.perform(click());

        ViewInteraction circularImageView = onView(
                allOf(withId(R.id.imgFotoPerfil), withContentDescription("Selecionar imagem do perfil.")));
        circularImageView.perform(scrollTo(), click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.itemSalvarPerfil), withContentDescription("Salvar as alterações do perfil"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction appCompatButton14 = onView(
                allOf(withId(android.R.id.button3), withText("OK"),
                        withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton14.perform(click());

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Abrir gaveta de menu"),
                        childAtPosition(
                                allOf(withId(R.id.barraAcaoPrincipal),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                0)),
                                0),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
