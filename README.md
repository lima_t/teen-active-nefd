### Leia-me ###

Este arquivo arquivo contém as informações necessárias para configurar o repositório do projeto Teen Active no seu computador.

### O que é este repositório para serve? ###

[Resumo]:
A construção desse trabalho cientifico visou desenvolver um aplicativo para Android que utilize a tecnologia oferecida pelo aparelho (GPS, acelerometria, aceleração linear, etc.) para a mensuração e quantificação da atividade física diária em adolescentes. Além disso, possibilitar a compreensão do comportamento para a atividade física de adolescentes, contribuindo para o melhor entendimento do construto. Possibilitar que cada aluno compreenda o conceito e a medida de atividade física utilizando seu próprio Smartphone.

[Versão]:
1.0.0.0 beta

[Equipe original do desenvolvimento]:

**Orientador**
**Coordenador:** Vinicius de Oliveira Damasceno
**Lattes:**      [http://lattes.cnpq.br/3253053787565027](http://lattes.cnpq.br/3253053787565027)
**Facebook:**    [https://www.facebook.com/damasceno.vo?fref=ts](https://www.facebook.com/damasceno.vo?fref=ts)
**E-mail:**      [vinicius.damasceno@ufpe.br](mailto:vinicius.damasceno@ufpe.br)

**Desenvolvedor Android**
**Aluno:**    Tarcísio de Lima Amorim
**Lattes:**   [http://lattes.cnpq.br/6288811552752754](http://lattes.cnpq.br/6288811552752754)
**Facebook:** [https://www.facebook.com.br/tarcisio.de.lima.amorim](https://www.facebook.com.br/tarcisio.de.lima.amorim)
**E-mail:**   [tarcisio.lima.amorim@outlook.com](mailto:tarcisio.lima.amorim@outlook.com)

** Como configurar o repositório no meu computador? **

*Pré-Requisitos:*
O projeto Teen Active foi construído para a plataforma Android, portanto a arvore de arquivos possui algumas dependências de softwares para que possam trabalhar corretamente, os softwares necessários para iniciar a configuração são:

*Essenciais:*
→ JDK 8 ou superior x86/x64, disponível para download em: [http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html](Link URL)]
→ Android Studio 2.1.3 ou superior, disponível para download em: [https://developer.android.com/studio/index.html?hl=pt-br](Link URL)];
→ Cliente Git, disponível para download em: [https://git-scm.com/downloads](https://git-scm.com/downloads) ou
→ Cliente SourceTree Disponível para download em: [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/);

*Opcionais:*
→ CorelDraw X8 ou superior, disponível para compra em: [https://www.corel.com/pt-br/](https://www.corel.com/pt-br/);
→ Microsoft Visio 2013 ou superior, dDisponível para compra em: [https://microsoft.office.com/visio](https://microsoft.office.com/visio);
→ SQLite Maestro v12.x ou superior, disponível para compra em: [https://www.sqlmaestro.com/products/sqlite/maestro/download/](https://www.sqlmaestro.com/products/sqlite/maestro/download/);

*Instalação e Configuração:*

→ Instale o JDK de acordo com a arquitetura do seu micro (x86 ou x64);
→ Acesse o ícone do "Meu Computador" com o botão direito do mouse em seguida selecione "Propriedades" → "Configurações Avançadas do Sistema". Deverá aparecer esse dialogo:

![image_thumb1.png](https://bitbucket.org/repo/oMreXn/images/4035391844-image_thumb1.png)

→ Acese o item "Variáveis de Ambiente" e adicione os seguintes valores para o java de acordo com sua arquitetura (x86 ou x64):
*Variavel JAVA_HOME deve possuir o caminho de referência de onde o jdk está instalado, podendo mudar o número da versão:*
![image_thumb2.png](https://bitbucket.org/repo/oMreXn/images/3128339797-image_thumb2.png)

*Variável CLASSPATH:*
![image_thumb3.png](https://bitbucket.org/repo/oMreXn/images/3171850616-image_thumb3.png)

*Variável PATH:*
![image_thumb4.png](https://bitbucket.org/repo/oMreXn/images/2171807899-image_thumb4.png)

→ Agora instale o Android Studio no computador, graças as configurações anteriores referentes as variáveis de ambiente, ele deverá ignorar a instalação do JAVA pois irá encontrar a referência para o mesmo no ambiente;

→ Abra o Android Studio e clique na opção das configurações em seguida vá até a opção "Appearance & Behavior" e expanda os itens até encontrar o item "Android SDK" como mostra na figura abaixo:

![thumb5.jpg](https://bitbucket.org/repo/oMreXn/images/1096518569-thumb5.jpg)

→ Você pode lançar uma instância do SDK Manager e baixar preferencialmente as ferramentas de desenvolvimento do Android 4.4 Kitkat (versão mínima utilizada no projeto) para que em seguida crie uma máquina virtual do mesmo;
→ Solicite ao administrador, um usuário para acesso do repositório através do E-mail: [tarcisio.lima.amorim@outlook.com](Link URL) caso não tenha recebido um convite espontâneo e em seguida efetue seu cadastro no site [https://bitbucket.org/](Link URL);
→ Agora instale o cliente GIT, seguindo o assistente em Next, Next, .... Finish;
→ Instale o cliente Source Tree para o bitbucket (git & mercurial) seguindo as respectivas instruções e adicionando os paramêtros solicitados (Usuário e Senha do bitbucket, Repositório, Pasta local se necessário);
→ Após instalado, configue o repositório a ser clonado através da opção "Clonar":
![thumb6.jpg](https://bitbucket.org/repo/oMreXn/images/4124537657-thumb6.jpg)

→ Insira a URL do repositório remoto no dialogo de configuração e defina a pasta local do computador para ser o repositório local do projeto:
URL: [https://lima_t@bitbucket.org/lima_t/teen-active-nefd.git](Link URL)

![thumb7.jpg](https://bitbucket.org/repo/oMreXn/images/2499536822-thumb7.jpg)

→ Agora tudo o que você tem que fazer é realizar um checkout do repositório para seu computador para que possa trabalhar, essa função pode ser feita pelo próprio Source Tree através da opção clonar durante a configuração ou pelo cliente Git instalado que por sua vez deve ser apontado no Android Studio seu executável que fica na pasta *C:\Program Files\Git\bin\git.exe*

*Recomendações Gerais:*
Ao trabalhar no projeto, efetuar commits e checkouts, verifique as versões existentes com clareza para que não ocorra inconsistências no código fonte. Vale ressaltar também que é essencial documentar e comentar o código para os outros integrantes da equipe.